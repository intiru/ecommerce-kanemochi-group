module.exports = function (grunt) {

    var jsFiles = [
        'assets/template_front/js/jquery-2.2.4.min.js',
        'assets/template_front/js/bootstrap.min.js',
        'assets/template_front/js/menuzord.js',
        'assets/template_front/js/main.js',
        'assets/template_front/revolution/js/jquery.themepunch.tools.min.js',
        'assets/template_front/revolution/js/jquery.themepunch.revolution.min.js',
        'assets/template_front/revolution/js/extensions/revolution.extension.actions.min.js',
        'assets/template_front/revolution/js/extensions/revolution.extension.carousel.min.js',
        'assets/template_front/revolution/js/extensions/revolution.extension.layeranimation.min.js',
        'assets/template_front/revolution/js/extensions/revolution.extension.migration.min.js',
        'assets/template_front/revolution/js/extensions/revolution.extension.navigation.min.js',
        'assets/template_front/revolution/js/extensions/revolution.extension.parallax.min.js',
        'assets/template_front/revolution/js/extensions/revolution.extension.slideanims.min.js',

        'assets/template_front/js/imagesloaded.pkgd.min.js',
        'assets/template_front/js/packery.js',
        'assets/template_front/js/fancybox.js',
        'assets/template_front/js/sweetalert.js',
        'assets/template_front/js/main.js',
    ];
    var cssFiles = [
        'assets/template_front/css/bootstrap.css',
        'assets/template_front/css/menuzord.css',
        'assets/template_front/fonts/font-awesome/css/font-awesome.css',
        'assets/template_front/css/style.css',
        'assets/template_front/css/responsive.css',
        'assets/template_front/css/animate.min.css',
        'assets/template_front/revolution/css/settings.css',
        'assets/template_front/revolution/css/layers.css',
        'assets/template_front/css/fancybox.css',
        'assets/template_front/css/main.css',
    ];
    var allFiles = jsFiles.concat(cssFiles); // merge js & css files directory

    grunt.initConfig({
        jsDistDir: 'js/',
        cssDistDir: 'assets/template_front/css/',
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: jsFiles,
                dest: '<%=jsDistDir%><%= pkg.name %>.js'
            },
            css: {
                src: cssFiles,
                dest: '<%=cssDistDir%><%= pkg.name %>.css'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '<%=jsDistDir%><%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    // banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
                },
                files: {
                    '<%=cssDistDir%><%= pkg.name %>.min.css': ['<%= concat.css.dest %>']
                }
            }
        },
        watch: {
            files: allFiles,
            tasks: ['concat', 'uglify', 'cssmin']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'concat',
        'uglify',
        'cssmin',
        'watch'
    ]);

};