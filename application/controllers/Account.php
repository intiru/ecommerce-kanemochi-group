<?php
defined('BASEPATH') or exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Account extends CI_Controller
{
    protected $server_key;
    protected $client_key;
    protected $midtrans_js_link;
    protected $midtrans_production;

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');

        $midtrans_key = $this->main->midtrans_key();
        $this->server_key = $midtrans_key['server_key'];
        $this->client_key = $midtrans_key['client_key'];
        $this->midtrans_js_link = $midtrans_key['midtrans_js_link'];
        $this->midtrans_production = $midtrans_key['midtrans_production'];

        Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
        Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
        Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method allowed
    }

    public function index()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'contact_us', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['captcha'] = $this->main->captcha();
        $data['side_menu_active'] = 'dashboard';
        $data['member'] = $this->db->where('id', $data['id_member'])->get('member')->row();
        $data['transaction_first'] = $this
            ->db
            ->where([
                'id_member' => $data['id_member'],
                'status' => 'payment_accepted'
            ])
            ->order_by('id', 'ASC')
            ->get('member_cart')
            ->row();
        $data['transaction_last'] = $this
            ->db
            ->where([
                'id_member' => $data['id_member'],
                'status' => 'payment_accepted'
            ])
            ->order_by('id', 'DESC')
            ->get('member_cart')
            ->row();
        $this->template->front('account', $data);
    }

    public function login()
    {
        error_reporting(E_ALL);
        $login_status = $this->session->userdata('login_status');
        if ($login_status) {
            redirect($this->session->userdata('url_redirect_login'));
        }

        $this->load->library('google');
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'login', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['captcha'] = $this->main->captcha();

        include_once APPPATH . "../vendor/autoload.php";
        $google_client = new Google_Client();
        $google_client->setClientId('407635300401-ftapk4kgm8gphvnkdpv0jrber6r426if.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('h9rJZNh3aL1acepQ5UHHR-Sz'); //Define your Client Secret Key
        $google_client->setRedirectUri('https://ecommerce.mtsc.com/account/signin_google_success'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');

        $data['login_google'] = $google_client->createAuthUrl();

        $url_uniqid = isset($_GET['p']) ? $_GET['p'] : '';
        $url_uniqid = $this->main->str_decrypt($url_uniqid);
        $url_session = $this->session->userdata('url_session');
        if ($url_uniqid) {
            $this->session->set_userdata(array('url_redirect_login' => $url_session[$url_uniqid]));
        }

        $this->template->front('account_signin', $data);
    }

    public function login_do()
    {
        header('Access-Control-Allow-Origin: *');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_login_check_form');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Mohon lengkapi Form dengan benar',
                'errors' => array(
                    'email' => form_error('email'),
                    'password' => form_error('password'),
                )
            ));
        } else {
            $member = $this->db->where(['email' => $this->input->post('email'), 'verifikasi_status' => 'yes'])->get('member')->row();
            $this->db->where('id', $member->id)->update('member', ['last_login' => date('Y-m-d H:i:s')]);
            $data = array(
                'login_status' => TRUE,
                'member' => $member
            );
            $this->session->set_userdata($data);

            $this->main->member_login_refill_cart();


            echo json_encode(array(
                'status' => 'success',
                'redirect' => $this->session->userdata('url_backward')
            ));
        }
    }

    public function signin_google_success()
    {

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        include_once APPPATH . "../vendor/autoload.php";
        $google_client = new Google_Client();
        $google_client->setClientId('407635300401-ftapk4kgm8gphvnkdpv0jrber6r426if.apps.googleusercontent.com'); //Define your ClientID
        $google_client->setClientSecret('h9rJZNh3aL1acepQ5UHHR-Sz'); //Define your Client Secret Key
        $google_client->setRedirectUri('https://ecommerce.mtsc.com/account/signin_google_success'); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');

        $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

        if (!isset($token["error"])) {
            echo '1';
            $url_redirect_login = $this->session->userdata('url_redirect_login');
            $google_client->setAccessToken($token['access_token']);
            $this->session->set_userdata('access_token', $token['access_token']);

            $google_service = new Google_Service_Oauth2($google_client);

            $data = $google_service->userinfo->get();

            $current_datetime = date('Y-m-d H:i:s');

            $data_member = array(
                'email' => $data['email'],
                'first_name' => $data['given_name'],
                'last_name' => $data['family_name'],
                'gender' => $data['family_name'],
                'hd' => $data['hd'],
                'id_google' => $data['id'],
                'link' => $data['link'],
                'locale' => $data['locale'],
                'name' => $data['name'],
                'picture' => $data['picture'],
                'signup_type' => 'google',
                'created_at' => $current_datetime,
                'last_login' => $current_datetime
            );

            $check = $this->db->where(array('id' => $data['id']))->get('member')->num_rows();
            if ($check == 0) {
                $this->db->insert('member', $data_member);
            } else {
                $this->db->where('id_google', $data['id'])->update('member', array('last_login' => $current_datetime));
            }

            $member = $this->db->where('id_google', $data['id'])->get('member')->row();

            $this->session->set_userdata('member', $member);
            $this->session->set_userdata(array('login_status' => TRUE));

            $this->main->member_login_refill_cart();


            redirect($url_redirect_login);
        } else {
            echo '2';
            redirect();
        }

    }

    function signin_facebook_success()
    {
        $this->load->library('facebook');
        if ($this->facebook->is_authenticated()) {
            // Get user info from facebook
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid'] = !empty($fbUser['id']) ? $fbUser['id'] : '';;
            $userData['first_name'] = !empty($fbUser['first_name']) ? $fbUser['first_name'] : '';
            $userData['last_name'] = !empty($fbUser['last_name']) ? $fbUser['last_name'] : '';
            $userData['email'] = !empty($fbUser['email']) ? $fbUser['email'] : '';
            $userData['gender'] = !empty($fbUser['gender']) ? $fbUser['gender'] : '';
            $userData['picture'] = !empty($fbUser['picture']['data']['url']) ? $fbUser['picture']['data']['url'] : '';
            $userData['link'] = !empty($fbUser['link']) ? $fbUser['link'] : 'https://www.facebook.com/';

            // Insert or update user data to the database
            $userID = $this->user->checkUser($userData);

            // Check user data insert or update status
            if (!empty($userID)) {
                $data['userData'] = $userData;

                // Store the user profile info into session
                $this->session->set_userdata('userData', $userData);
            } else {
                $data['userData'] = array();
            }

            // Facebook logout URL
            $data['logoutURL'] = $this->facebook->logout_url();
        } else {
            // Facebook authentication url
//            $data['authURL'] = $this->facebook->login_url();

            redirect('login');
        }
    }

    function facebook_logout()
    {
        // Remove local Facebook session
        $this->facebook->destroy_session();
        // Remove user data from session
        $this->session->unset_userdata('userData');
        // Redirect to login page
        redirect('account/facebook_logout');
    }

    function Is_already_register($id)
    {
        return TRUE;
    }

    public function signup()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'contact_us', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['captcha'] = $this->main->captcha();
        $data['province'] = $this->db->order_by('province_name', 'ASC')->get('province')->result();
        $this->template->front('account_signup', $data);
    }

    public function signup_do()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'Nama Depan', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Nama Belakang', 'trim|required');
        $this->form_validation->set_rules('gender', 'Jenis Kelamin', 'trim|required');
        $this->form_validation->set_rules('birthday', 'Tanggal Lahir', 'trim|required');
        $this->form_validation->set_rules('phone', 'Nomer Telepon', 'trim|required');
        $this->form_validation->set_rules('id_province', 'Nama Provinsi', 'trim|required');
        $this->form_validation->set_rules('id_city', 'Nomer Kota', 'trim|required');
        $this->form_validation->set_rules('id_subdistrict', 'Nama Kecamatan', 'trim|required');
        $this->form_validation->set_rules('address', 'Nomer Telepon', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_member_register_email_check');
        $this->form_validation->set_rules('password', 'Password', 'trim');
        $this->form_validation->set_rules('password_conf', 'Konfirmasi Password', 'trim|matches[password]');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Mohon lengkapi Form dengan benar',
                'errors' => array(
                    'first_name' => form_error('first_name'),
                    'last_name' => form_error('last_name'),
                    'gender' => form_error('gender'),
                    'birthday' => form_error('birthday'),
                    'phone' => form_error('phone'),
                    'id_province' => form_error('id_province'),
                    'id_city' => form_error('id_city'),
                    'id_subdistrict' => form_error('id_subdistrict'),
                    'address' => form_error('address'),
                    'email' => form_error('email'),
                    'password' => form_error('password'),
                    'password_conf' => form_error('password_conf'),
                )
            ));
        } else {
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $gender = $this->input->post('gender');
            $birthday = $this->input->post('birthday');
            $phone = $this->input->post('phone');
            $id_province = $this->input->post('id_province');
            $id_city = $this->input->post('id_city');
            $id_subdistrict = $this->input->post('id_subdistrict');
            $address = $this->input->post('address');
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $data_insert = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'gender' => $gender,
                'birthday' => $birthday,
                'phone' => $phone,
                'address' => $address,
                'email' => $email,
                'password' => md5($password),
                'verifikasi_status' => 'not_yet',
                'signup_type' => 'email',
                'created_at' => date('Y-m-d H:i:s')
            );

            $this->db->insert('member', $data_insert);
            $member_id = $this->db->insert_id();
            $member_id_encrypt = $this->main->str_encrypt($member_id);

            $member_address_insert = [
                'id_province' => $id_province,
                'id_city' => $id_city,
                'id_subdistrict' => $id_subdistrict,
                'id_member' => $member_id,
                'owner_name' => $first_name . ' ' . $last_name,
                'phone' => $phone,
                'address' => $address,
                'primary' => 'yes',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->db->insert('member_address', $member_address_insert);

            $subject = 'Verifikasi Member Kanemochi';
            $to_email = $email;
            $to_name = $first_name . ' ' . $last_name;
            $body = '
                Klik link berikut ini untuk melakukan verifikasi member kanemochi :<br />
                <a href="' . site_url('account/verifikasi/' . $member_id_encrypt) . '">Klik Link Ini</a>
            ';

            $this->main->mailer_auth($subject, $to_email, $to_name, $body);


            echo json_encode(array(
                'status' => 'success',
                'title' => 'Berhasil',
                'message' => 'Registrasi Member Berhasil, Silahkan check email untuk verifikasi',
                'redirect' => site_url('login')
            ));
        }
    }

    function verifikasi($member_id_encrypt)
    {
        $member_id_decrypt = $this->main->str_decrypt($member_id_encrypt);
        $verifikasi_status = $this->db->where('id', $member_id_decrypt)->get('member')->row()->verifikasi_status;
        $data_update = [
            'verifikasi_status' => 'yes'
        ];

        $this->db->where('id', $member_id_decrypt)->update('member', $data_update);

        if ($verifikasi_status == 'not_yet') {
            $data = $this->main->data_front();
            $data['page'] = $this->db->where(array('type' => 'contact_us', 'id_language' => $data['id_language']))->get('pages')->row();
            $this->template->front('account_verification_success', $data);
        } else {
            redirect('login');
        }


    }

    function email_test()
    {
        $subject = 'Morning';
        $to_email = 'mtsc@gmail.com';
        $to_name = 'MTSC';
        $body = 'Verifikasi Member <a href="">Klik Link Verifikasi</a>';
        $this->main->mailer_auth($subject, $to_email, $to_name, $body);
    }

    public function profile()
    {
        $this->main->login_check_page();
        $data = $this->main->data_front();
        $member_sess = $this->session->userdata('member');
        $data['page'] = $this->db->where(array('type' => 'member_profile', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['member'] = $this->db->where(array('id' => $member_sess->id))->get('member')->row();
        $data['side_menu_active'] = 'profile';

//        echo json_encode($data['page']);

        $member_gender = 'male';

        if ($data['member']->gender) {
            $member_gender = $data['member']->gender;
        }

        $data['member_gender'] = $member_gender;

        $this->template->front('account_profile', $data);
    }

    public function profile_update()
    {
        $this->main->login_check_page();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'Nama Depan', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Nama Belakang', 'trim|required');
        $this->form_validation->set_rules('gender', 'Jenis Kelamin', 'trim|required');
        $this->form_validation->set_rules('birthday', 'Tanggal Lahir', 'trim|required');
        $this->form_validation->set_rules('phone', 'Nomer Telepon', 'trim|required');
        $this->form_validation->set_rules('address', 'Nomer Telepon', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_member_email_check');
        $this->form_validation->set_rules('password', 'Password', 'trim');
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('password_conf', 'Konfirmasi Password', 'trim|matches[password]');
        }
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Mohon lengkapi Form dengan benar',
                'errors' => array(
                    'first_name' => form_error('first_name'),
                    'last_name' => form_error('last_name'),
                    'gender' => form_error('gender'),
                    'birthday' => form_error('birthday'),
                    'phone' => form_error('phone'),
                    'address' => form_error('address'),
                    'email' => form_error('email'),
                    'password' => form_error('password'),
                    'password_conf' => form_error('password_conf'),
                )
            ));
        } else {
            $member = $this->session->userdata('member');
            $id = $member->id;

            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $gender = $this->input->post('gender');
            $birthday = $this->input->post('birthday');
            $phone = $this->input->post('phone');
            $address = $this->input->post('address');
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $data_update = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'gender' => $gender,
                'birthday' => $birthday,
                'phone' => $phone,
                'address' => $address,
                'email' => $email,
                'updated_at' => date('Y-m-d H:i:s')
            );

            if ($password) {
                $data_update['password'] = md5($password);
            }

            $this->db->where('id', $id)->update('member', $data_update);

            $member_data_new = $this->db->where(array('id' => $id))->get('member')->row();
            $this->session->set_userdata(array('member' => $member_data_new));

            echo json_encode(array(
                'status' => 'success',
                'title' => 'Berhasil',
                'message' => 'Data Profile berhasil diperbarui'
            ));
        }
    }

    public function help()
    {
        $this->main->login_check_page();
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'member_help', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['side_menu_active'] = 'help';
        $this->template->front('account_help', $data);
    }

    public function track_order()
    {
        $this->main->login_check_page();
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'member_track_order', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['side_menu_active'] = 'order';
        $this->template->front('member_track_order', $data);
    }

    public function order_list()
    {
        $this->main->login_check_page();
        $data = $this->main->data_front();
        $member = $this->session->userdata('member');
        $data['page'] = $this->db->where(array('type' => 'member_order_list', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['member_order'] = $this
            ->db
            ->where('id_member', $member->id)
            ->order_by('id', 'DESC')
            ->get('member_cart')
            ->result();
        $data['side_menu_active'] = 'order';
        $this->template->front('member_order_list', $data);
    }

    public function order_detail($id_member_cart)
    {
        $this->main->login_check_page();
        $this->main->member_cart_id_check($id_member_cart);
        $data = $this->main->data_front();

        $page = $this->db->where(array('type' => 'member_order_list', 'id_language' => $data['id_language']))->get('pages')->row();
        $total_qty = 0;
        $member_cart = $this
            ->db
            ->select('member_cart.*, member_cart.id AS id_member_cart, member_cart_midtrans.*, member_cart_midtrans.id AS id_member_cart_midtrans')
            ->join('member_cart_midtrans', 'member_cart_midtrans.id_member_cart = member_cart.id')
            ->where('member_cart.id', $id_member_cart)
            ->order_by('member_cart.id', 'DESC')
            ->get('member_cart')
            ->row();

//        echo json_encode($member_cart);
//        exit;

        $member_cart_detail = $this
            ->db
            ->select('mcd.*, p.price AS product_price, mcd.price AS product_price_cart, p.title AS product_title, pg.thumbnail AS product_thumbnail, c.title AS product_category')
            ->join('product p', 'p.id = mcd.id_product')
            ->join('product_gallery pg', 'pg.id_product = p.id')
            ->join('category c', 'c.id = p.id_category')
            ->where('mcd.id_member_cart', $id_member_cart)
            ->where('pg.use_thumbnail', 'yes')
            ->where('mcd.checked_status', 'yes')
            ->order_by('mcd.id')
            ->get('member_cart_detail mcd')
            ->result();
        $member_cart_midtrans = $this
            ->db
            ->where('id_member_cart', $id_member_cart)
            ->get('member_cart_midtrans')
            ->row();
        $member_address = $this
            ->db
            ->join('province p', 'p.id_province = ma.id_province')
            ->join('city c', 'c.id_city = ma.id_city')
            ->join('subdistrict s', 's.id_subdistrict = ma.id_subdistrict')
            ->where(array(
                'id' => $member_cart->id_member_address,
            ))
            ->get('member_address ma')
            ->row();


        $data['page'] = $page;
        $data['member_address'] = $member_address;
        $data['member_cart'] = $member_cart;
        $data['member_cart_midtrans'] = $member_cart_midtrans;
        $data['member_cart_detail'] = $member_cart_detail;
        $data['memer_address'] = $member_address;
        $data['total_qty'] = $total_qty;
        $data['side_menu_active'] = 'order';
        $data['client_key'] = $this->client_key;
        $data['midtrans_js_link'] = $this->midtrans_js_link;
        $this->template->front('member_order_detail', $data);
    }

    public function order_tracking($id_member_cart)
    {
        $this->main->login_check_page();
        $member_cart = $this->db->where('id', $id_member_cart)->order_by('id', 'DESC')->get('member_cart')->row();

        $data_get = 'key=' . $this->main->rajaongkir_apikey() .
            '&waybill=' . $member_cart->resi .
            '&courier=' . $member_cart->courier_code;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data_get,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: your-api-key"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $data['track'] = json_decode($response, TRUE)['rajaongkir'];

        $data_return['modal_body'] = $this->load->view('front/member_order_detail_tracking', $data, TRUE);
        $data_return['track'] = $data['track'];

        echo json_encode($data_return);
        exit;

    }

    public function detail_pembayaran($id_member_cart)
    {
        $this->main->login_check_page();

        $member_cart_midtrans = $this->db->where('id_member_cart', $id_member_cart)->get('member_cart_midtrans')->row();

        $data = [
            'member_cart' => $member_cart_midtrans
        ];

        $data_return = [
            'modal_body' => $this->load->view('front/member_detail_pembayaran', $data, TRUE)
        ];

        echo json_encode($data_return);
        exit;
    }

    public function bayar_kembali($id_member_cart)
    {
        $invoice_duplicate_status = FALSE;

        $member = $this->session->userdata('member');

        $invoice_new_generate = $this->main->invoice_generate();
        $invoice_new = $invoice_new_generate['invoice'];
        $invoice_number = $invoice_new_generate['invoice_number'];

        /**
         * Update New Invoice karena dibayarkan kembali
         */

        $data_update = [
            'invoice' => $invoice_new,
            'invoice_number' => $invoice_number
        ];
        $this->db->where('id', $id_member_cart)->update('member_cart', $data_update);


        /**
         * Get date member cart, detail dan midtrans
         */
        $member_cart = $this->db->where('id', $id_member_cart)->get('member_cart')->row();
        $member_cart_detail = $this->db->where('id_member_cart', $id_member_cart)->get('member_cart_detail')->result();
        $member_cart_midtrans = $this->db->where('id', $id_member_cart)->get('member_cart_midtrans')->row();
        $member_address = $this
            ->db
            ->select('p.*, c.*, s.*, ma.*, ma.id AS id_member_address')
            ->join('province p', 'p.id_province = ma.id_province')
            ->join('city c', 'c.id_city = ma.id_city')
            ->join('subdistrict s', 's.id_subdistrict = ma.id_subdistrict')
            ->where(array(
                'id' => $member_cart->id_member_address,
            ))
            ->get('member_address ma')
            ->row();

        require_once dirname(__FILE__) . '/../libraries/Midtrans.php';
        \Midtrans\Config::$serverKey = $this->server_key;

        // Uncomment for production environment
        \Midtrans\Config::$isProduction = $this->midtrans_production;

        // Enable sanitization
        \Midtrans\Config::$isSanitized = true;

        // Enable 3D-Secure
        \Midtrans\Config::$is3ds = true;

        $item_details = array();

        foreach ($member_cart_detail as $row) {
            $product_name = $this->db->select('title')->where('id', $row->id_product)->get('product')->row()->title;
            $item_details[] = array(
                'id' => $row->id_product,
                'price' => $row->price,
                'quantity' => $row->qty,
                'name' => $product_name
            );
        }

        $item_details[] = array(
            'id' => rand(),
            'price' => $member_cart->courier_service_price,
            'quantity' => 1,
            'name' => 'Ongkos Kirim'
        );

        $billing_address = array(
            'first_name' => $member->first_name,
            'last_name' => $member->last_name,
            'address' => $member->address,
            'city' => $member_address->city_name,
            'postal_code' => $member_address->postal_code,
            'phone' => $member->phone,
            'country_code' => 'IDN'
        );

        $shipping_address = array(
            'first_name' => $member_address->owner_name,
            'last_name' => "",
            'address' => $member_address->address,
            'city' => $member_address->city_name,
            'postal_code' => $member_address->postal_code,
            'phone' => $member_address->phone,
            'country_code' => 'IDN'
        );

        $customer_details = array(
            'first_name' => $member->first_name,
            'last_name' => $member->last_name,
            'email' => $member->email,
            'phone' => $member->phone,
            'billing_address' => $billing_address,
            'shipping_address' => $shipping_address
        );

        $transaction_details = array(
            'order_id' => $member_cart->invoice,
        );
        $transaction = array(
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details,
            'item_details' => $item_details,
        );

        try {
            $snap_token = \Midtrans\Snap::getSnapToken($transaction);
        } catch (Exception $e) {
            $invoice_duplicate_status = TRUE;
            $return = $this->bayar_kembali($id_member_cart, TRUE);

            if ($return['invoice_duplicate_status']) {
                $this->bayar_kembali($id_member_cart, TRUE);
            }
        }

        $response = array(
            'snap_token' => $snap_token,
            'invoice_duplicate_status' => $invoice_duplicate_status
        );

        if ($return) {
            return $response;
        } else {
            echo json_encode($response);
        }
    }


    public function order_accepted()
    {
        $this->main->login_check_page();
        $id_member_cart = $this->input->post('id_member_cart');
        $data_update = [
            'status' => 'received'
        ];

        $this->db->where('id', $id_member_cart)->update('member_cart', $data_update);

    }

    public function address()
    {
        $this->main->login_check_page();
        $data = $this->main->data_front();
//        echo json_encode($this->session->userdata('member'));
//        echo $data['id_member'];
//        exit;
        $data['page'] = $this->db->where(array('type' => 'member_address', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['province'] = $this->db->order_by('province_name', 'ASC')->get('province')->result();
        $data['side_menu_active'] = 'address';
        $data['address_list'] = $this
            ->db
            ->join('province', 'province.id_province = member_address.id_province')
            ->join('city', 'city.id_city = member_address.id_city')
            ->join('subdistrict', 'subdistrict.id_subdistrict = member_address.id_subdistrict')
            ->where(array(
                'id_member' => $data['id_member']
            ))
            ->order_by('id', 'ASC')
            ->order_by('primary', 'DESC')
            ->get('member_address')
            ->result();

        $this->template->front('member_address', $data);
    }

    public function address_add()
    {
        $this->main->login_check_page();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('owner_name', 'Nama Pemilik/Toko', 'trim|required');
        $this->form_validation->set_rules('phone', 'Nomer Telepon', 'trim|required');
        $this->form_validation->set_rules('id_province', 'Provinsi', 'trim|required');
        $this->form_validation->set_rules('id_city', 'Kota', 'trim|required');
        $this->form_validation->set_rules('id_subdistrict', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('address', 'Alamat Detail', 'trim|required');
        $this->form_validation->set_rules('primary', 'Alamat Utama', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Mohon lengkapi Form dengan benar',
                'errors' => array(
                    'owner_name' => form_error('owner_name'),
                    'phone' => form_error('phone'),
                    'id_province' => form_error('id_province'),
                    'id_city' => form_error('id_city'),
                    'id_subdistrict' => form_error('id_subdistrict'),
                    'address' => form_error('address'),
                    'primary' => form_error('primary'),
                )
            ));
        } else {
            $member = $this->session->userdata('member');
            $id_member = $member->id;

            $owner_name = $this->input->post('owner_name');
            $phone = $this->input->post('phone');
            $id_province = $this->input->post('id_province');
            $id_city = $this->input->post('id_city');
            $id_subdistrict = $this->input->post('id_subdistrict');
            $address = $this->input->post('address');
            $primary = $this->input->post('primary');

            if ($primary == 'yes') {
                $this->db->where('id_member', $id_member)->update('member_address', array('primary' => 'no'));
            }

            $data_insert = array(
                'owner_name' => $owner_name,
                'phone' => $phone,
                'id_province' => $id_province,
                'id_city' => $id_city,
                'id_subdistrict' => $id_subdistrict,
                'id_member' => $id_member,
                'address' => $address,
                'primary' => $primary,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('member_address', $data_insert);

            echo json_encode(array(
                'status' => 'success',
                'title' => 'Berhasil',
                'message' => 'Alamat baru berhasil disimpan'
            ));
        }
    }

    public function member_address_primary()
    {
        $this->main->login_check_page();
        $member = $this->session->userdata('member');
        $id_member = $member->id;
        $id_member_address = $this->input->post('id_member_address');

        $this->db->where('id_member', $id_member)->update('member_address', array('primary' => 'no'));
        $this->db->where('id', $id_member_address)->update('member_address', array('primary' => 'yes', 'updated_at' => date('Y-m-d H:i:s')));
    }

    public function member_address_delete()
    {
        $this->main->login_check_page();
        $id_member_address = $this->input->post('id_member_address');

        $this->db->where('id', $id_member_address)->delete('member_address');
    }

    public function member_address_edit()
    {
        $this->main->login_check_page();
        $id_member_address = $this->input->post('id_member_address');
        $member_address = $this->db->where('id', $id_member_address)->get('member_address')->row();
        $province = $this
            ->db
            ->select('id_province AS id, province_name AS text')
            ->order_by('province_name', 'ASC')
            ->get('province')
            ->result();
        $city = $this
            ->db
            ->select('id_city AS id, city_name AS text')
            ->where('id_province', $member_address->id_province)
            ->order_by('city_name', 'ASC')
            ->get('city')
            ->result();
        $subdistrict = $this
            ->db
            ->select('id_subdistrict AS id, subdistrict_name AS text')
            ->where('id_city', $member_address->id_city)
            ->order_by('subdistrict_name', 'ASC')
            ->get('subdistrict')
            ->result();

        $data = array(
            'member_address' => $member_address,
            'province' => $province,
            'city' => $city,
            'subdistrict' => $subdistrict
        );

        echo json_encode($data);

    }


    public function address_update()
    {
        $this->main->login_check_page();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('owner_name', 'Nama Pemilik/Toko', 'trim|required');
        $this->form_validation->set_rules('phone', 'Nomer Telepon', 'trim|required');
        $this->form_validation->set_rules('id_province', 'Provinsi', 'trim|required');
        $this->form_validation->set_rules('id_city', 'Kota', 'trim|required');
        $this->form_validation->set_rules('id_subdistrict', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('address', 'Alamat Detail', 'trim|required');
        $this->form_validation->set_rules('primary', 'Alamat Utama', 'trim|required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Mohon lengkapi Form dengan benar',
                'errors' => array(
                    'owner_name' => form_error('owner_name'),
                    'phone' => form_error('phone'),
                    'id_province' => form_error('id_province'),
                    'id_city' => form_error('id_city'),
                    'id_subdistrict' => form_error('id_subdistrict'),
                    'address' => form_error('address'),
                    'primary' => form_error('primary'),
                )
            ));
        } else {
            $member = $this->session->userdata('member');
            $id_member = $member->id;
            $id = $this->input->post('id');
            $owner_name = $this->input->post('owner_name');
            $phone = $this->input->post('phone');
            $id_province = $this->input->post('id_province');
            $id_city = $this->input->post('id_city');
            $id_subdistrict = $this->input->post('id_subdistrict');
            $address = $this->input->post('address');
            $primary = $this->input->post('primary');

            if ($primary == 'yes') {
                $this->db->where('id_member', $id_member)->update('member_address', array('primary' => 'no'));
            }

            $data_update = array(
                'owner_name' => $owner_name,
                'phone' => $phone,
                'id_province' => $id_province,
                'id_city' => $id_city,
                'id_subdistrict' => $id_subdistrict,
                'id_member' => $id_member,
                'address' => $address,
                'primary' => $primary,
                'updated_at' => date('Y-m-d H:i:s'),
            );

            $this->db->where('id', $id)->update('member_address', $data_update);

            echo json_encode(array(
                'status' => 'success',
                'title' => 'Berhasil',
                'message' => 'Alamat Pengiriman berhasil diperbarui'
            ));
        }
    }

    public function city_get()
    {
        $id_province = $this->input->post('id_province');
        $city = $this
            ->db
            ->select('id_city AS id, city_name AS text')
            ->where(array(
                'id_province' => $id_province
            ))
            ->order_by('city_name', 'ASC')
            ->get('city')
            ->result();

        header('Content-Type: application/json');
        echo json_encode($city);
    }

    public function subdistrict_get()
    {
        $id_city = $this->input->post('id_city');
        $subdistrict = $this
            ->db
            ->select('id_subdistrict AS id, subdistrict_name AS text')
            ->where(array(
                'id_city' => $id_city
            ))
            ->order_by('subdistrict_name', 'ASC')
            ->get('subdistrict')
            ->result();

        header('Content-Type: application/json');
        echo json_encode($subdistrict);
    }

    public function subdistrict_postal_code_get()
    {
        $id_subdistrict = $this->input->post('id_subdistrict');
        $postal_code = $this
            ->db
            ->select('postal_code')
            ->where(array(
                'id_subdistrict' => $id_subdistrict
            ))
            ->get('subdistrict')
            ->row()
            ->postal_code;

        echo $postal_code;
    }

    public function get_province()
    {
        $client = new GuzzleHttp\Client();
        $city = $this->db->get('city')->result();
        $data_insert = array();
        foreach ($city as $row) {
            $res = $client->request('GET', 'https://pro.rajaongkir.com/api/subdistrict?key=2ecc568bbed69df540d3a5941b74b2ca&city=' . $row->city_id, array());
            $response = json_decode($res->getBody(), true);

            $data_insert = array();
            foreach ($response['rajaongkir']['results'] as $row) {
                $data_insert = array(
                    'subdistrict_id' => $row['subdistrict_id'],
                    'city_id' => $row['city_id'],
                    'province_id' => $row['province_id'],
                    'subdistrict_name' => $row['subdistrict_name'],
                );
                //$this->db->insert('subdistrict', $data_insert);
            }
        }


        echo json_encode($data_insert);
    }

    public function logout()
    {
        $this->session->sess_destroy();

        if ($_GET['p']) {
            $url_uniqid = $_GET['p'];
            $url_uniqid = $this->main->str_decrypt($url_uniqid);
            $url_session = $this->session->userdata('url_session');

//            echo json_encode($url_session[$url_uniqid]);
            redirect($url_session[$url_uniqid]);
        } else {
            redirect();
        }
    }

    public function login_check_form($str)
    {
        $email = $this->input->post('email');
        $password = md5($this->input->post('password'));

        $check = $this
            ->db
            ->where(array(
                'email' => $email,
                'password' => $password,
            ))
            ->get('member')
            ->num_rows();

        if ($check == 0) {
            $this->form_validation->set_message('login_check_form', 'Email atau Password salah');
            return FALSE;
        } else {
            $check_verifikasi_status = $this
                ->db
                ->where(array(
                    'email' => $email,
                    'password' => $password,
                    'verifikasi_status' => 'yes'
                ))
                ->get('member')
                ->num_rows();

            if ($check_verifikasi_status > 0) {
                return TRUE;
            } else {
                $this->form_validation->set_message('login_check_form', 'User belum melakukan verifikasi');
                return FALSE;
            }


        }
    }

    public function member_email_check($str)
    {
        $member = $this->session->userdata('member');
        $email = $member->email;
        $check = $this
            ->db
            ->where(array(
                'email' => $str
            ))
            ->where_not_in('email', array($email))
            ->get('member')
            ->num_rows();

        if ($check > 0) {
            $this->form_validation->set_message('member_email_check', 'Email tidak tersedia');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function member_register_email_check($str)
    {
        $check = $this
            ->db
            ->where(array(
                'email' => $str,
                'verifikasi_status' => 'yes'
            ))
            ->get('member')
            ->num_rows();

        if ($check > 0) {
            $this->form_validation->set_message('member_register_email_check', 'Email tidak tersedia');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function forgot_password()
    {
        error_reporting(E_ALL);
        $login_status = $this->session->userdata('login_status');
        if ($login_status) {
            redirect($this->session->userdata('url_redirect_login'));
        }

        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'login', 'id_language' => $data['id_language']))->get('pages')->row();

        $this->template->front('account_forgot_password', $data);
    }

    public function forgot_password_send()
    {
        header('Access-Control-Allow-Origin: *');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|callback_forgot_password_check_email');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Mohon lengkapi Form dengan benar',
                'errors' => array(
                    'email' => form_error('email'),
                )
            ));
        } else {
            $email = $this->input->post('email');
            $member = $this->db->where('email', $email)->get('member')->row();

            $token_reset_password = bin2hex(random_bytes(16));
            $subject = 'Reset Password Member Kanemochi';
            $to_email = $email;
            $to_name = $member->first_name . ' ' . $member->last_name;
            $body = '
                Klik link berikut ini untuk melakukan reset password member kanemochi :<br />
                <a href="' . site_url('account/reset_password/' . $token_reset_password) . '">Klik Link Ini</a>
            ';

            $this->db->where('id', $member->id)->update('member', ['token_reset_password' => $token_reset_password]);

            $this->main->mailer_auth($subject, $to_email, $to_name, $body);


            echo json_encode(array(
                'status' => 'success',
                'title' => 'Permintaan Reset Password Berhasil',
                'message' => 'Silahkan check email Anda untuk Link Reset Password',
                'member' => $member,
                'token' => $token_reset_password
            ));
        }
    }

    function reset_password($token_reset_password)
    {
        $check_token_reset_password_member = $this
            ->db
            ->where('token_reset_password', $token_reset_password)
            ->get('member')
            ->num_rows();

        if($check_token_reset_password_member == 0) {
            redirect();
        } else {
            $member_id = $this->db->where('token_reset_password', $token_reset_password)->get('member')->row()->id;
            $this->session->set_userdata(['reset_password_member_id' => $member_id]);

            $data = $this->main->data_front();
            $data['page'] = $this->db->where(array('type' => 'login', 'id_language' => $data['id_language']))->get('pages')->row();
            $data['member'] = $this->db->where('id', $member_id)->get('member')->row();

            $this->template->front('account_reset_password', $data);
        }
    }

    function reset_password_send()
    {
        header('Access-Control-Allow-Origin: *');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('password_conf', 'Konfirmasi Password', 'trim|required|matches[password_conf]');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Mohon lengkapi Form dengan benar',
                'errors' => array(
                    'password' => form_error('password'),
                    'password_conf' => form_error('password_conf'),
                )
            ));
        } else {
            $email = $this->input->post('email');
            $member_id = $this->session->userdata('reset_password_member_id');

            $password = $this->input->post('password');
            $password_conf = $this->input->post('password_conf');
            $token_reset_password = bin2hex(random_bytes(16));

            $data_update = [
                'password' => md5($password),
                'token_reset_password' => $token_reset_password
            ];

            $this->db->where('id', $member_id)->update('member', $data_update);

            echo json_encode(array(
                'status' => 'success',
                'title' => 'Reset Password Berhasil',
                'message' => 'Silahkan login dengan Password Baru',
                'redirect' => site_url('login')
            ));
        }
    }

    function forgot_password_check_email($str)
    {
        $email_member = $str;
        $check = $this->db->where('email', $email_member)->get('member')->num_rows();

        if ($check == 0) {
            $this->form_validation->set_message('forgot_password_check_email', 'Email tidak tersedia');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
