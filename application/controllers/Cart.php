<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cart extends CI_Controller
{
    protected $server_key;
    protected $client_key;
    protected $midtrans_js_link;
    protected $midtrans_production;

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');

        $midtrans_key = $this->main->midtrans_key();
        $this->server_key = $midtrans_key['server_key'];
        $this->client_key = $midtrans_key['client_key'];
        $this->midtrans_js_link = $midtrans_key['midtrans_js_link'];
        $this->midtrans_production = $midtrans_key['midtrans_production'];
    }

    public function list_page()
    {
//        echo json_encode($this->session->all_userdata());
//        exit;
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'cart_list', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['cart_list'] = $this->cart->contents();
//        $data['cart_total_item'] = $this->cart->total_items();

//        echo json_encode($data['cart_list']);
//        exit;

        $price_total = 0;
        $cart_total_item = 0;
        $product_checked_status = FALSE;
        foreach ($data['cart_list'] as $row) {
            if ($row['options']['checked_status'] == "yes") {
                $price_total += $row['subtotal'];
                $cart_total_item += $row['qty'];
                $product_checked_status = TRUE;
            }
        }

//        $member = $this->session->userdata('member');
//        $id_member = $member->id;
//        $member_cart_detail = $this
//            ->db
//            ->select('p.*, p.id AS id_product, ct.*, ct.id AS id_member_cart_detail, c.title AS category_title, pg.thumbnail as product_thumbnail')
//            ->join('member_cart mc', 'mc.id = ct.id_member_cart')
//            ->join('product p', 'p.id = ct.id_product')
//            ->join('category c', 'c.id = p.id_category')
//            ->join('product_gallery pg', 'pg.id_product = p.id', 'left')
//            ->where(array(
//                'mc.id_member' => $id_member,
//                'pg.use_thumbnail' => 'yes',
//                'pg.use_view' => 'yes',
//                'ct.product_status' => 'product_in_cart'
//            ))
//            ->get('member_cart_detail ct')
//            ->result();
//
//        foreach($member_cart_detail as $row) {
//            unset($row->description);
//        }
//
//        echo json_encode($member_cart_detail);
//        exit;

        $data['cart_total_price'] = $price_total;
        $data['cart_total_item'] = $cart_total_item;
        $data['product_checked_status'] = $product_checked_status;

        $this->template->front('cart_list_page', $data);
    }

    public function checkout_page()
    {
        $this->main->login_check_page();
        $this->main->checkout_product_check();

        $this->main->invoice_number_generate();

        $data = $this->main->data_front();
        $member_cart = $this->db->where(array('id_member' => $data['id_member'], 'status' => 'start'))->get('member_cart')->row();

        $invoice = $member_cart->invoice;
        $data['page'] = $this->db->where(array('type' => 'cart_checkout', 'id_language' => $data['id_language']))->get('pages')->row();

        //echo $invoice.'--';

        /**
         * Proses Kondisi Cart
         */


        $data['cart_content'] = $this->cart->contents();
        $data['cart_price_total'] = 0;
        foreach ($data['cart_content'] as $row) {
            if ($row['options']['checked_status'] == 'yes') {
                $data['cart_price_total'] += $row['subtotal'];
            }
        }

        /**
         * Update cart total atau total pembelian produk
         */

        $data_update = [
            'cart_total' => $data['cart_price_total']
        ];
        $this
            ->db
            ->where('id', $member_cart->id)
            ->update('member_cart', $data_update);

        /**
         * Proses Kondisi Courirer
         */

        $origin_address = $this->db->where('id', 1)->get('shop_address_origin')->row();
        $data['member_address'] = $this
            ->db
            ->join('province p', 'p.id_province = ma.id_province')
            ->join('city c', 'c.id_city = ma.id_city')
            ->join('subdistrict s', 's.id_subdistrict = ma.id_subdistrict')
            ->where(array(
                'id_member' => $data['id_member'],
                'primary' => 'yes'
            ))
            ->get('member_address ma')
            ->row();
        $courier_data = $this->db->where(array('use' => 'yes'))->order_by('title', 'ASC')->get('courier')->result();
        $courier_list = '';
        $courier_count = count($courier_data) - 1;

        foreach ($courier_data as $key => $row) {
            $separator = '';
            if ($key != $courier_count) {
                $separator = ':';
            }
            $courier_list .= $row->code . $separator;
        }

        $weight = 0;
        $length = 0;
        $width = 0;
        $height = 0;
        $diameter = 0;
        foreach ($data['cart_content'] as $row) {
            if ($row['options']['checked_status'] == 'yes') {
                $product = $this->db->where('id', $row['id'])->get('product')->row();
                $weight += $row['qty'] * $product->weight;
                $length += $row['qty'] * $product->length;
                $width += $row['qty'] * $product->width;
                $height += $row['qty'] * $product->height;
                $diameter += $row['qty'] * $product->diameter;
            }
        }

        $data_get = 'key=' . $this->main->rajaongkir_apikey() .
            '&origin=' . $origin_address->id_subdistrict .
            '&originType=subdistrict' .
            '&destination=' . $data['member_address']->id_subdistrict .
            '&destinationType=subdistrict' .
            '&weight=' . $weight .
            '&length=' . $length .
            '&width=' . $width .
            '&height=' . $height .
            '&diameter=' . $diameter .
            '&courier=' . $courier_list;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data_get,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: your-api-key"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

//        echo $response;
//        exit;

        $data['courier'] = json_decode($response, TRUE)['rajaongkir']['results'];

        $data['ship_cost'] = 0;
        foreach ($data['courier'] as $key => $row) {
            foreach ($row['costs'] as $key2 => $row2) {
                if ($key == 0 && $key2 == 0) {
                    $data['ship_cost'] = $row2['cost'][0]['value'];
                    break;
                }
            }
        }

        $data['checkout_total'] = $data['cart_price_total'] + $data['ship_cost'];

        /**
         * Midtrans
         */

        $data['client_key'] = $this->client_key;
        $data['midtrans_js_link'] = $this->midtrans_js_link;

        $data['invoice'] = $invoice;

        $this->template->front('cart_checkout', $data);
    }

    /**
     * Digunakan untuk change snap token saat change courier type and price
     */
    function courier_change_price($return = FALSE)
    {
        $ship_cost = $this->input->post('ship_cost');
        $courier_code = $this->input->post('courier_code');
        $courier_code_name = $this->input->post('courier_code_name');
        $courier_service_code = $this->input->post('courier_service_code');
        $courier_service_code_name = $this->input->post('courier_service_code_name');
        $courier_service_price = $this->input->post('courier_service_price');
        $courier_service_day_estimate = $this->input->post('courier_service_day_estimate');
        $invoice_duplicate_status = FALSE;

        $member = $this->session->userdata('member');
        /**
         * Update courier at member cart
         */

        $data_where = array(
            'id_member' => $member->id,
            'status' => 'start'
        );

        $data_update = array(
            'courier_code' => $courier_code,
            'courier_code_name' => $courier_code_name,
            'courier_service_code' => $courier_service_code,
            'courier_service_code_name' => $courier_service_code_name,
            'courier_service_price' => $courier_service_price,
            'courier_service_day_estimate' => $courier_service_day_estimate
        );

        $this->db->where($data_where)->update('member_cart', $data_update);


        $invoice = $this->db->select('invoice')->where(array('id_member' => $member->id, 'status' => 'start'))->get('member_cart')->row()->invoice;

        $member_address = $this
            ->db
            ->select('p.*, c.*, s.*, ma.*, ma.id AS id_member_address')
            ->join('province p', 'p.id_province = ma.id_province')
            ->join('city c', 'c.id_city = ma.id_city')
            ->join('subdistrict s', 's.id_subdistrict = ma.id_subdistrict')
            ->where(array(
                'id_member' => $member->id,
                'primary' => 'yes'
            ))
            ->get('member_address ma')
            ->row();

        /**
         * Update Member Address pada Member Cart
         */
        $data_update = [
            'id_member_address' => $member_address->id_member_address
        ];

        $this->db->where($data_where)->update('member_cart', $data_update);

        require_once dirname(__FILE__) . '/../libraries/Midtrans.php';
        \Midtrans\Config::$serverKey = $this->server_key;

        // Uncomment for production environment
        \Midtrans\Config::$isProduction = $this->midtrans_production;

        // Enable sanitization
        \Midtrans\Config::$isSanitized = true;

        // Enable 3D-Secure
        \Midtrans\Config::$is3ds = true;

        $item_details = array();

        foreach ($this->cart->contents() as $row) {
            if ($row['options']['checked_status'] == 'yes') {
                $item_details[] = array(
                    'id' => $row['id'],
                    'price' => $row['price'],
                    'quantity' => $row['qty'],
                    'name' => $row['name']
                );
            }
        }

        $item_details[] = array(
            'id' => rand(),
            'price' => $ship_cost,
            'quantity' => 1,
            'name' => 'Ongkos Kirim'
        );

        $billing_address = array(
            'first_name' => $member->first_name,
            'last_name' => $member->last_name,
            'address' => $member->address,
            'city' => $member_address->city_name,
            'postal_code' => $member_address->postal_code,
            'phone' => $member->phone,
            'country_code' => 'IDN'
        );

        $shipping_address = array(
            'first_name' => $member_address->owner_name,
            'last_name' => "",
            'address' => $member_address->address,
            'city' => $member_address->city_name,
            'postal_code' => $member_address->postal_code,
            'phone' => $member_address->phone,
            'country_code' => 'IDN'
        );

        $customer_details = array(
            'first_name' => $member->first_name,
            'last_name' => $member->last_name,
            'email' => $member->email,
            'phone' => $member->phone,
            'billing_address' => $billing_address,
            'shipping_address' => $shipping_address
        );

        $transaction_details = array(
            'order_id' => $invoice,
            'gross_amount' => $this->cart->total(), // no decimal allowed for creditcard
        );
        $transaction = array(
            'transaction_details' => $transaction_details,
            'customer_details' => $customer_details,
            'item_details' => $item_details,
        );

        try {
            $snap_token = \Midtrans\Snap::getSnapToken($transaction);
        } catch (Exception $e) {
            $invoice_duplicate_status = TRUE;
            $this->main->invoice_number_generate_when_exist();
            $return = $this->courier_change_price(TRUE);

            if ($return['invoice_duplicate_status']) {
                $this->courier_change_price(TRUE);
            }
        }

        $response = array(
            'snap_token' => $snap_token,
            'invoice_duplicate_status' => $invoice_duplicate_status
        );

        if ($return) {
            return $response;
        } else {
            echo json_encode($response);
        }

    }

    public function add()
    {
        $member = $this->session->userdata('member');
        $id_member = $member->id;

        $id = $this->input->post('id');
        $title = $this->input->post('title');
        $qty = $this->input->post('qty');
        $price = $this->input->post('price');
        $thumbnail = $this->input->post('thumbnail');
        $category_title = $this->input->post('category_title');
        $id_product_option_arr = $this->input->post('id_product_option_arr');
        $id_product_option_json = json_encode($id_product_option_arr);
        $id_member_cart = NULL;
        $id_member_cart_detail = NULL;
        $product_option = [];

        $check_product_in_stock = $this->db->where('id', $id)->get('product')->row()->out_of_stock;
        if ($check_product_in_stock == 'yes') {
            echo json_encode([
                'status' => 'sold_out'
            ]);
            exit;
        }


        /**
         * Insert Into Table Cart Temporary
         */

        $check_order_exist = $this
            ->db
            ->where(array('id_member' => $id_member, 'status' => 'start'))
            ->get('member_cart')
            ->num_rows();

        /**
         * Proses ini adalah proses check apakah member sudah melakukan cart atau belum
         * jika belum, akan insert data baru ke member_cart dan juga member_cart_detail
         */
        if ($check_order_exist == 0) {
            $data_member_cart_insert = array(
                'id_member' => $id_member,
                'cart_date' => date('Y-m-d H:i:s'),
                'cart_total' => 0,
                'status' => 'start',
            );

            $this->db->insert('member_cart', $data_member_cart_insert);
            $id_member_cart = $this->db->insert_id();

            $data_insert = array(
                'id_product' => $id,
                'id_member_cart' => $id_member_cart,
                'id_product_option_json' => $id_product_option_json,
                'qty' => $qty,
                'price' => $price,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('member_cart_detail', $data_insert);
            $id_member_cart_detail = $this->db->insert_id();


            /**
             * Jika sudah akan melakukan insert atau update ke member_cart_detail
             * tergantung apakah product yang dimasukkan member, sudah ada di cart atau belum
             */
        } else {

            $id_member_cart = $this
                ->db
                ->select('id')
                ->where(array(
                    'id_member' => $id_member,
                    'status' => 'start'
                ))
                ->get('member_cart')
                ->row()
                ->id;

            $check_product_exist = $this
                ->db
                ->where(array(
                    'id_product' => $id,
                    'id_member_cart' => $id_member_cart
                ));
            $key = 0;
            foreach ($id_product_option_arr as $id_product_option => $id_product_option_value) {
                if ($key == 0) {
                    $check_product_exist = $check_product_exist
                        ->like('id_product_option_json', '"' . $id_product_option . '":"' . $id_product_option_value . '"');
                } else {
                    $check_product_exist = $check_product_exist
                        ->or_like('id_product_option_json', '"' . $id_product_option . '":"' . $id_product_option_value . '"');
                }
                $key++;

            }


            $check_product_exist = $check_product_exist
                ->get('member_cart_detail');

            if ($check_product_exist->num_rows() == 0) {
                $data_insert = array(
                    'id_product' => $id,
                    'id_member_cart' => $id_member_cart,
                    'id_product_option_json' => $id_product_option_json,
                    'qty' => $qty,
                    'price' => $price,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $this->db->insert('member_cart_detail', $data_insert);
                $id_member_cart_detail = $this->db->insert_id();
            } else {

                $id_member_cart_detail = $check_product_exist->row()->id;

                $qty_latest = $this
                    ->db
                    ->select('qty')
                    ->where(array(
                        'id' => $id_member_cart_detail,
                    ))
                    ->get('member_cart_detail')
                    ->row()
                    ->qty;

                $qty_now = $qty_latest + $qty;

                $data_update = array(
                    'qty' => $qty_now,
                    'id_product_option_json' => $id_product_option_json,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $this
                    ->db
                    ->where(array(
                        'id' => $id_member_cart_detail,
                    ))
                    ->update('member_cart_detail', $data_update);
            }

        }

        /**
         * Mendapatkan Label dari Product Option untuk nanti digunakan saat view kernajang belanja
         */
        foreach ($id_product_option_arr as $id_product_option => $id_product_option_value) {
            $product_option[$id_product_option]['option'] = $this->db->where('id', $id_product_option)->get('product_option')->row()->title;
            $product_option[$id_product_option]['value'] = $this->db->where('id', $id_product_option_value)->get('product_option_value')->row()->title;
        }


        /**
         * Check apakah saat add sudah ada atau belum
         */
        if ($this->cart->total_items() > 0) {
            $cart_session = $this->cart->contents();
            $product_search_status = FALSE;
            $product_search_qty = 0;
            $product_search_rowid = '';

            if (count($id_product_option_arr) == 0) {
                foreach ($cart_session as $rowid => $row) {
                    if ($row['id'] == $id) {
                        $product_search_status = TRUE;
                        $product_search_qty = $row['qty'];
                        $product_search_rowid = $rowid;
                        break;
                    }
                }

                if ($product_search_status) {
                    $qty_now = $product_search_qty + $qty;
                    $cart_update = [
                        'rowid' => $product_search_rowid,
                        'qty' => $qty_now
                    ];
                    $this->cart->update($cart_update);
                } else {
                    $cart_data = array(
                        'id' => $id,
                        'qty' => $qty,
                        'price' => $price,
                        'name' => $title,
                        'options' => array(
                            'thumbnail' => $thumbnail,
                            'category_title' => $category_title,
                            'id_member_cart' => $id_member_cart,
                            'id_member_cart_detail' => $id_member_cart_detail,
                            'id_product_option_json' => $id_product_option_json,
                            'product_option' => $product_option,
                            'checked_status' => 'no',
                            'product_status' => 'product_in_cart'
                        )
                    );
                    $this->cart->insert($cart_data);
                }
            } else {
                $id_product_option_json_arr = [];
                foreach ($cart_session as $rowid => $row) {
                    $id_product_option_json_arr[$rowid] = [
                        'qty' => $row['qty'],
                        'product_option' => json_decode($row['options']['id_product_option_json'], TRUE)
                    ];
                }

                /**
                 * ini dari session
                 */
                foreach ($id_product_option_json_arr as $rowid => $data_arr) {

                    /**
                     * ini product option dari session
                     */
                    $product_option_match_count = 0;
                    foreach ($data_arr['product_option'] as $id_product_option => $id_product_option_value) {
                        /**
                         * ini dari add form
                         */

                        foreach ($id_product_option_arr as $id_product_option_form => $id_product_option_value_form) {
                            if ($id_product_option == $id_product_option_form && $id_product_option_value == $id_product_option_value_form) {
                                $product_option_match_count++;
                            }
                        }
                    }

                    if ($product_option_match_count == count($id_product_option_arr)) {
                        $product_search_status = TRUE;
                        $product_search_qty = $data_arr['qty'];
                        $product_search_rowid = $rowid;
                        break;
                    }

                }

                if($product_search_status) {
                    $qty_now = $product_search_qty + $qty;
                    $cart_update = [
                        'rowid' => $product_search_rowid,
                        'qty' => $qty_now
                    ];
                    $this->cart->update($cart_update);
                } else {
                    $cart_data = array(
                        'id' => $id,
                        'qty' => $qty,
                        'price' => $price,
                        'name' => $title,
                        'options' => array(
                            'thumbnail' => $thumbnail,
                            'category_title' => $category_title,
                            'id_member_cart' => $id_member_cart,
                            'id_member_cart_detail' => $id_member_cart_detail,
                            'id_product_option_json' => $id_product_option_json,
                            'product_option' => $product_option,
                            'checked_status' => 'no',
                            'product_status' => 'product_in_cart'
                        )
                    );
                    $this->cart->insert($cart_data);
                }

            }
        } else {

            /**
             * Insert cart session
             */
            $cart_data = array(
                'id' => $id,
                'qty' => $qty,
                'price' => $price,
                'name' => $title,
                'options' => array(
                    'thumbnail' => $thumbnail,
                    'category_title' => $category_title,
                    'id_member_cart' => $id_member_cart,
                    'id_member_cart_detail' => $id_member_cart_detail,
                    'id_product_option_json' => $id_product_option_json,
                    'product_option' => $product_option,
                    'checked_status' => 'no',
                    'product_status' => 'product_in_cart'
                )
            );
            $this->cart->insert($cart_data);
        }


        /**
         * Total Member Cart
         */

        $cart_content = $this->cart->contents();
        $cart_total = 0;
        foreach ($cart_content as $row) {
            if ($row['options']['checked_status'] == 'yes') {
                $cart_total += $row['subtotal'];
            }
        }

        $this
            ->db
            ->where(array(
                'id_member' => $id_member,
                'status' => 'start'
            ))
            ->update('member_cart', array(
                'cart_total' => $cart_total
            ));

        $return = array(
            'status' => 'exist',
            'cart_list_review' => $this->main->cart_list_review(),
            'cart_data' => $cart_data,
//            'id_member_cart' => $id_member_cart,
//            'id_member_cart_detail' => $id_member_cart_detail
        );

        echo json_encode($return);
    }

    function member_cart_detail_add_price()
    {
        $member_cart_detail = $this->db->get('member_cart_detail')->result();
        foreach ($member_cart_detail as $row) {
            $price = $this->db->where('id', $row->id_product)->get('product')->row()->price;

//            $this->db->where('id', $row->id)->update('member_cart_detail', ['price' => $price]);
        }
    }

    public function remove()
    {
        $member = $this->session->userdata('member');
        $id_member = $member->id;
        $rowid = $this->input->post('rowid');
        $id_product = $this->input->post('id');
        $cart_price_total = 0;
        $cart_content = $this->cart->contents();
        $id_member_cart = $this->db->select('id')->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->row()->id;

        foreach ($cart_content as $row) {
            $cart_price_total += $row['subtotal'];
        }

        $this->cart->remove($rowid);

        $this->db->where(array('id_product' => $id_product, 'id_member_cart' => $id_member_cart))->delete('member_cart_detail');

        $return = array(
            'cart_list_review' => $this->main->cart_list_review(),
            'cart_item_total' => $this->cart->total_items(),
            'cart_price_total' => $cart_price_total,
            'id_product' => $id_product,
            'rowid' => $rowid
        );

        echo json_encode($return);
    }

    public function destroy()
    {
        $this->cart->destroy();
    }

    public function update()
    {
        $member = $this->session->userdata('member');
        $id_member = $member->id;
        $qty_arr = $this->input->post('qty');
        $id_product = $this->input->post('id_product');
        $data_update = array();
        $id_member_cart = $this->db->select('id')->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->row()->id;

        foreach ($qty_arr as $rowid => $qty) {
            $data_update[] = array(
                'rowid' => $rowid,
                'qty' => $qty
            );

            $data_update_temp = array(
                'qty' => $qty,
                'updated_at' => date('Y-m-d H:i:s'),
            );

            $this
                ->db
                ->where(array(
                    'id_product' => $id_product[$rowid],
                    'id_member_cart' => $id_member_cart
                ))
                ->update('member_cart_detail', $data_update_temp);

        }

        $this->cart->update($data_update);

        redirect(site_url('cart/list'));
    }

    public function update_item_qty()
    {

        $this->check_login_cart();
        $member = $this->session->userdata('member');
        $rowid = $this->input->post('rowid');
        $qty = $this->input->post('qty');
        $id_product = $this->cart->contents()[$rowid]['id'];
        $id_member = $member->id;
        $id_member_cart = $this->db->select('id')->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->row()->id;

        $data_update = array(
            'rowid' => $rowid,
            'qty' => $qty
        );

        $data_update_temp = array(
            'qty' => $qty,
            'updated_at' => date('Y-m-d H:i:s'),
        );

        $this
            ->db
            ->where(array(
                'id_product' => $id_product,
                'id_member_cart' => $id_member_cart
            ))
            ->update('member_cart_detail', $data_update_temp);


        $this->cart->update($data_update);

        echo json_encode([
            'status' => TRUE,
        ]);
        exit;
    }

    public function update_item_checked()
    {
        $this->check_login_cart();
        $rowid = $this->input->post('rowid');
        $member = $this->session->userdata('member');
        $id_member = $member->id;

        $this->main->generate_member_cart_start();

        $id_member_cart = $this->db->select('id')->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->row()->id;
        $checked_status = $this->input->post('checked_status');
        $id_product = $this->cart->contents()[$rowid]['id'];
        $options = $this->cart->contents()[$rowid]['options'];
        $options = array_merge($options, ['checked_status' => $checked_status]);

        $this
            ->db
            ->where([
                'id_member_cart' => $id_member_cart,
                'id_product' => $id_product
            ])
            ->update('member_cart_detail', ['checked_status' => $checked_status]);


        $data = [
            'rowid' => $rowid,
            'options' => $options
        ];

        $this->cart->update($data);


        echo json_encode([
            'status' => TRUE,
        ]);
        exit;

        echo json_encode(['id_member_cart' => $id_member_cart,
            'id_product' => $id_product]);
    }

    function check_login_cart()
    {
        $login_status = $this->session->userdata('login_status');
        if (!$login_status) {
            echo json_encode([
                'status' => FALSE,
                'title' => 'Mohon Perhatian',
                'message' => 'Silahkan login member terlebih dahulu'
            ]);
            exit;
        }
    }

    public function checkout_process()
    {
        $member = $this->session->userdata('member');
        $id_member = $member->id;
        $check_product_checked_status = $this
            ->db
            ->join('member_cart', 'member_cart.id = member_cart_detail.id_member_cart')
            ->where([
                'id_member' => $id_member,
                'checked_status' => 'yes',
                'product_status' => 'product_in_cart',
                'member_cart.status' => 'start'
            ])
            ->get('member_cart_detail')
            ->num_rows();
        $check_product_cart_status = $this
            ->db
            ->join('member_cart', 'member_cart.id = member_cart_detail.id_member_cart')
            ->where([
                'id_member' => $id_member,
                'product_status' => 'product_in_cart'
            ])
            ->get('member_cart_detail')->num_rows();
        $login_status = $this->session->userdata('login_status');

        if ($check_product_checked_status > 0 && $login_status) {
            $data = array(
                'status' => TRUE,
                'type' => NULL,
                'title' => NULL,
                'message' => NULL,
                'button_confirm' => NULL
            );
        } elseif ($check_product_cart_status == 0) {
            $data = array(
                'status' => FALSE,
                'type' => 'cart_empty',
                'title' => 'Perhatian',
                'message' => 'Keranjang Belanja masih Kosong, silahkan berbelanja terlebih dahulu',
                'button_confirm' => 'Lanjut Belanja'
            );
        } elseif ($check_product_checked_status == 0) {
            $data = array(
                'status' => FALSE,
                'type' => 'cart_unchecked',
                'title' => 'Perhatian',
                'message' => 'Produk belum Dicentang, silahkan pilih/centang dahulu',
                'button_confirm' => 'Baik'
            );
        } elseif (!$login_status) {
            $data = array(
                'status' => FALSE,
                'type' => 'login',
                'title' => 'Login / Daftar',
                'message' => 'Mohon Login terlebih dahulu untuk melakukan pembayaran',
                'button_confirm' => 'Login / Daftar'
            );
        } else {
            $data = array(
                'status' => FALSE,
                'type' => NULL,
                'title' => NULL,
                'message' => NULL,
                'button_confirm' => NULL
            );
        }

        echo json_encode($data);
    }

    public function checkout_success()
    {
        $member = $this->session->userdata('member');
        $result_midtrans = $this->input->post('result_midtrans');
        $id_member = $member->id;
        $id_member_cart = $this->db->select('id')->where(array('invoice' => $result_midtrans['order_id']))->get('member_cart')->row()->id;


        /**
         * Update cart status menjadi 0 agar tidak terlihat lagi
         * dan di database juga sudah dianggap di beli agar tidak di proses kembali
         */
        $cart_content = $this->cart->contents();
        foreach ($cart_content as $row) {
            if ($row['options']['checked_status'] == 'yes') {
                $this->cart->update([
                    'rowid' => $row['rowid'],
                    'qty' => 0
                ]);
                $this
                    ->db
                    ->where([
                        'id_product' => $row['id'],
                        'id_member_cart' => $id_member_cart
                    ])
                    ->update('member_cart_detail',
                        ['product_status' => 'purchased']
                    );

            }
        }

        if ($id_member_cart) {
            $id_member_address = $this
                ->db
                ->where(array(
                    'id_member' => $id_member,
                    'primary' => 'yes'
                ))
                ->get('member_address')
                ->row()
                ->id;

            $data_update = array(
                'id_member_address' => $id_member_address,
                'status' => 'payment_accepted',
                'date_payment_accepted' => date('Y-m-d H:i:s')
            );

            $this
                ->db
                ->where(array('id' => $id_member_cart))
                ->update('member_cart', $data_update);

            /**
             * Insert data midtrans
             */


            $datetime = new DateTime($result_midtrans['transaction_time']);
            $datetime->modify('+1 day');
            $payment_deadline = $datetime->format('Y-m-d H:i:s');
            $data_insert_midtrans = array(
                'id_member_cart' => $id_member_cart,
                'status_code' => $result_midtrans['status_code'],
                'status_message' => $result_midtrans['status_message'],
                'transaction_id' => $result_midtrans['transaction_id'],
                'order_id' => $result_midtrans['order_id'],
                'gross_amount' => $result_midtrans['gross_amount'],
                'payment_type' => $result_midtrans['payment_type'],
                'transaction_time' => $result_midtrans['transaction_time'],
                'payment_deadline' => $payment_deadline,
                'transaction_status' => $result_midtrans['transaction_status'],
                'va_bank' => isset($result_midtrans['va_numbers'][0]['bank']) ? $result_midtrans['va_numbers'][0]['bank'] : '',
                'va_number' => isset($result_midtrans['va_numbers'][0]['va_number']) ? $result_midtrans['va_numbers'][0]['va_number'] : '',
                'fraud_status' => $result_midtrans['fraud_status'],
                'pdf_url' => $result_midtrans['pdf_url'],
                'finish_redirect_url' => $result_midtrans['finish_redirect_url'],
                'masked_card' => isset($result_midtrans['masked_card']) ? $result_midtrans['masked_card'] : '',
                'card_type' => isset($result_midtrans['card_type']) ? $result_midtrans['card_type'] : '',
                'raw' => json_encode($result_midtrans)
            );


            $member_cart_midtrans_check = $this->db->where('id_member_cart', $id_member_cart)->get('member_cart_midtrans')->num_rows();
            if ($member_cart_midtrans_check == 0) {
                $this->db->insert('member_cart_midtrans', $data_insert_midtrans);
            } else {
                $this->db->where('id_member_cart', $id_member_cart)->update('member_cart_midtrans', $data_insert_midtrans);
            }


            $this->main->checkout_mail_member($id_member_cart);

        }

    }

    public function checkout_pending()
    {
        $member = $this->session->userdata('member');
        $result_midtrans = $this->input->post('result_midtrans');
        $id_member = $member->id;
        $id_member_cart = $this->db->select('id')->where(array('invoice' => $result_midtrans['order_id']))->get('member_cart')->row()->id;

        /**
         * Update cart status menjadi 0 agar tidak terlihat lagi
         * dan di database juga sudah dianggap di beli agar tidak di proses kembali
         */
        $cart_content = $this->cart->contents();
        foreach ($cart_content as $row) {
            if ($row['options']['checked_status'] == 'yes') {
                $this->cart->update([
                    'rowid' => $row['rowid'],
                    'qty' => 0
                ]);
                $this
                    ->db
                    ->where([
                        'id_product' => $row['id'],
                        'id_member_cart' => $id_member_cart
                    ])
                    ->update('member_cart_detail',
                        ['product_status' => 'purchased']
                    );

            }
        }

        if ($id_member_cart) {
            $id_member_address = $this
                ->db
                ->where(array(
                    'id_member' => $id_member,
                    'primary' => 'yes'
                ))
                ->get('member_address')
                ->row()
                ->id;

            $data_update = array(
                'status' => 'pending',
                'date_payment_process' => date('Y-m-d H:i:s'),
                'id_member_address' => $id_member_address
            );

            $this
                ->db
                ->where(array('id' => $id_member_cart))
                ->update('member_cart', $data_update);

            /**
             * Insert data midtrans
             */

            $datetime = new DateTime($result_midtrans['transaction_time']);
            $datetime->modify('+1 day');
            $payment_deadline = $datetime->format('Y-m-d H:i:s');

            $data_insert_midtrans = array(
                'id_member_cart' => $id_member_cart,
                'status_code' => $result_midtrans['status_code'],
                'status_message' => $result_midtrans['status_message'],
                'transaction_id' => $result_midtrans['transaction_id'],
                'order_id' => $result_midtrans['order_id'],
                'gross_amount' => $result_midtrans['gross_amount'],
                'payment_type' => $result_midtrans['payment_type'],
                'transaction_time' => $result_midtrans['transaction_time'],
                'payment_deadline' => $payment_deadline,
                'transaction_status' => $result_midtrans['transaction_status'],
                'va_bank' => isset($result_midtrans['va_numbers'][0]['bank']) ? $result_midtrans['va_numbers'][0]['bank'] : '',
                'va_number' => isset($result_midtrans['va_numbers'][0]['va_number']) ? $result_midtrans['va_numbers'][0]['va_number'] : '',
                'fraud_status' => $result_midtrans['fraud_status'],
                'pdf_url' => $result_midtrans['pdf_url'],
                'finish_redirect_url' => $result_midtrans['finish_redirect_url'],
                'masked_card' => isset($result_midtrans['masked_card']) ? $result_midtrans['masked_card'] : '',
                'card_type' => isset($result_midtrans['card_type']) ? $result_midtrans['card_type'] : '',
                'raw' => json_encode($result_midtrans)
            );

            $member_cart_midtrans_check = $this->db->where('id_member_cart', $id_member_cart)->get('member_cart_midtrans')->num_rows();
            if ($member_cart_midtrans_check == 0) {
                $this->db->insert('member_cart_midtrans', $data_insert_midtrans);
            } else {
                $this->db->where('id_member_cart', $id_member_cart)->update('member_cart_midtrans', $data_insert_midtrans);
            }


            $this->main->checkout_mail_member($id_member_cart);
        }
    }

    public function checkout_error()
    {
        $member = $this->session->userdata('member');
        $result_midtrans = $this->input->post('result_midtrans');
        $id_member = $member->id;
        $id_member_cart = $this->db->select('id')->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->row()->id;

        /**
         * Update cart status menjadi 0 agar tidak terlihat lagi
         * dan di database juga sudah dianggap di beli agar tidak di proses kembali
         */
        $cart_content = $this->cart->contents();
        foreach ($cart_content as $row) {
            if ($row['options']['checked_status'] == 'yes') {
                $this->cart->update([
                    'rowid' => $row['rowid'],
                    'qty' => 0
                ]);
                $this
                    ->db
                    ->where([
                        'id_product' => $row['id'],
                        'id_member_cart' => $id_member_cart
                    ])
                    ->update('member_cart_detail',
                        ['product_status' => 'purchased']
                    );

            }
        }

        if ($id_member_cart) {
            $id_member_address = $this
                ->db
                ->where(array(
                    'id_member' => $id_member,
                    'primary' => 'yes'
                ))
                ->get('member_address')
                ->row()
                ->id;

            $data_update = array(
                'status' => 'error',
                'date_payment_process' => date('Y-m-d H:i:s'),
                'id_member_address' => $id_member_address
            );

            /**
             * Insert data midtrans
             */

            $datetime = new DateTime($result_midtrans['transaction_time']);
            $datetime->modify('+1 day');
            $payment_deadline = $datetime->format('Y-m-d H:i:s');

            $data_insert_midtrans = array(
                'id_member_cart' => $id_member_cart,
                'status_code' => $result_midtrans['status_code'],
                'status_message' => $result_midtrans['status_message'],
                'transaction_id' => $result_midtrans['transaction_id'],
                'order_id' => $result_midtrans['order_id'],
                'gross_amount' => $result_midtrans['gross_amount'],
                'payment_type' => $result_midtrans['payment_type'],
                'transaction_time' => $result_midtrans['transaction_time'],
                'payment_deadline' => $payment_deadline,
                'transaction_status' => $result_midtrans['transaction_status'],
                'va_bank' => isset($result_midtrans['va_numbers'][0]['bank']) ? $result_midtrans['va_numbers'][0]['bank'] : '',
                'va_number' => isset($result_midtrans['va_numbers'][0]['va_number']) ? $result_midtrans['va_numbers'][0]['va_number'] : '',
                'fraud_status' => $result_midtrans['fraud_status'],
                'pdf_url' => $result_midtrans['pdf_url'],
                'finish_redirect_url' => $result_midtrans['finish_redirect_url'],
                'masked_card' => isset($result_midtrans['masked_card']) ? $result_midtrans['masked_card'] : '',
                'card_type' => isset($result_midtrans['card_type']) ? $result_midtrans['card_type'] : '',
                'raw' => json_encode($result_midtrans)
            );


            $member_cart_midtrans_check = $this->db->where('id_member_cart', $id_member_cart)->get('member_cart_midtrans')->num_rows();
            if ($member_cart_midtrans_check == 0) {
                $this->db->insert('member_cart_midtrans', $data_insert_midtrans);
            } else {
                $this->db->where('id_member_cart', $id_member_cart)->update('member_cart_midtrans', $data_insert_midtrans);
            }


            $this
                ->db
                ->where(array('id' => $id_member_cart))
                ->update('member_cart', $data_update);

        }
    }

    public function view()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'contact_us', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['captcha'] = $this->main->captcha();
        $this->template->front('cart_view', $data);
    }

    public function captcha_check($str)
    {
        if ($str == $this->session->userdata('captcha_mwz')) {
            return TRUE;
        } else {
            $this->form_validation->set_message('captcha_check', 'security code was wrong, please fill again truly');
            return FALSE;
        }
    }

    public function send()
    {
        $this->main->login_check_page();

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nama', 'required');
        $this->form_validation->set_rules('phone', 'No Telepon', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('message', 'Pesan', 'required');
        $this->form_validation->set_rules('captcha', 'Security Code', 'required|callback_captcha_check');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'title' => 'Perhatian',
                'message' => 'Fill form completly',
                'errors' => array(
                    'name' => form_error('name'),
                    'phone' => form_error('phone'),
                    'email' => form_error('email'),
                    'message' => form_error('message'),
                    'captcha' => form_error('captcha'),
                )
            ));
        } else {
            $email_admin = $this->db->where('use', 'yes')->get('email')->result();
            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $message = $this->input->post('message');


            $message_admin = '

            Dear Admin,<br /><br />
            You have contact us message from web form<br />
            form details as follows:<br /><br />
            
            Name : ' . $name . '<br>
            Email : ' . $email . '<br>
            Telephone : ' . $phone . '<br>
            Message : ' . $message . '<br /><br />
            
            
            Regarding,<br />
            Contact Us System ' . $this->main->web_name() . '<br /><br />' . $this->main->credit();

            $message_user = '
			    Dear ' . $name . ',<br />
			    <br />
			    Thank you for contact us, We will follow up your message as soon as possible ^_^<br />
			    <br /><br />
			    Regarding,<br />
			    ' . $this->main->web_name() . '<br /><Br />' . $this->main->credit();

            $this->main->mailer_auth('Kontak Kami - ' . $this->main->web_name(), $email, $this->main->web_name(), $message_user);

            foreach ($email_admin as $r) {
                $this->main->mailer_auth('Kontak Kami - Website ' . $this->main->web_url(), $r->email, $this->main->web_name() . ' Administrator ', $message_admin);
            }

            echo json_encode(array(
                'status' => 'success',
                'title' => 'Sukses',
                'message' => 'Thank you for contact us, we will follow up you as soon as possible ^_^'
            ));

        }
    }


    function test()
    {
        echo json_encode($this->cart->contents());
    }

    function email_view($id_member_cart = 84) {
        $this->main->checkout_mail_member($id_member_cart);
    }

}
