<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cron extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function check_payment_deadline()
    {
        $date_now = date('Y-m-d');
        $member_cart = $this
            ->db
            ->join('member_cart_midtrans', 'member_cart_midtrans.id_member_cart = member_cart.id')
            ->where('member_cart.status', 'pending')
            ->where('DATE(member_cart_midtrans.payment_deadline)', $date_now)
            ->get('member_cart')
            ->result();

        foreach ($member_cart as $row) {

            if ($row->payment_deadline) {
                $payment_deadline = new \DateTime($row->payment_deadline);
                $datetime_now = new \DateTime();
                if ($payment_deadline > $datetime_now) {
                    $this->db->where('id', $row->id_member_cart)->update('member_cart', ['status' => 'expired']);
                }
            }

        }
    }

    function check_delivered()
    {
        $member_cart = $this
            ->db
            ->where([
                'status' => 'send',
                'resi !=' => NULL
            ])
            ->get('member_cart')
            ->result();

        foreach ($member_cart as $row) {
            $data_get = 'key=' . $this->main->rajaongkir_apikey() .
                '&waybill=' . $row->resi .
                '&courier=' . $row->courier_code;

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $data_get,
                CURLOPT_HTTPHEADER => array(
                    "content-type: application/x-www-form-urlencoded",
                    "key: your-api-key"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $track = json_decode($response, TRUE)['rajaongkir'];
            $delivered_status = $track['result']['delivered'];

            if($delivered_status) {
                $this->db->where('id', $row->id)->update('member_cart', ['status' => 'delivered']);
            }

        }
    }
}
