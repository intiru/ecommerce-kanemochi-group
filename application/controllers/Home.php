<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {

        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'home', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['slider'] = $this->db->where(array('use' => 'yes', 'id_language' => $data['id_language']))->order_by('id', 'ASC')->get('slider')->result();
        $data['product_list'] = $this
            ->db
            ->select('product.*, category.title AS category_title, product_gallery.thumbnail as product_thumbnail')
            ->join('category', 'category.id = product.id_category', 'left')
            ->join('product_gallery', 'product_gallery.id_product = product.id', 'left')
            ->where(array(
                'product.use' => 'yes',
                'product.best_seller' => 'yes',
                'product_gallery.use_thumbnail' => 'yes',
                'product_gallery.use_view' => 'yes'
            ))
            ->order_by('title', 'ASC')
            ->get('product', 4, 0)
            ->result();

        $data['home_product_row'] = $this
            ->db
            ->select('product.*, pages.thumbnail AS pages_thumbnail, category.title AS category_title')
            ->join('product', 'product.id = pages.id_product', 'left')
            ->join('category', 'category.id = product.id_category', 'left')
            ->where(array(
                'pages.type' => 'home_product_row',
                'product.id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();

        $data['blog_home'] = $this
            ->db
            ->select('blog.title,blog.thumbnail,blog.thumbnail_alt,blog.description,blog.created_at, blog_category.title AS category_title')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->where('blog.use', 'yes')
            ->where('blog.id_language', $data['id_language'])
            ->order_by('blog.id', 'DESC')
            ->get('blog', 4, 0)
            ->result();

        $this->template->front('home', $data);

    }
}
