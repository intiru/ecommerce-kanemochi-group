<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function success()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'blog', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['page_type'] = 'home';

        $this->template->front('payment_success', $data);
    }

    public function pending()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'blog', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['page_type'] = 'home';

        $this->template->front('payment_pending', $data);
    }

    public function error()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'blog', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['page_type'] = 'home';

        $this->template->front('payment_error', $data);
    }

    public function notif()
    {
        error_reporting(E_ALL);
        error_reporting(0);
        $post = file_get_contents("php://input");

        $post_2 = json_encode($this->input->post(NULL, TRUE));


        $text = $post ? $post : $post_2;

        $data_midtrans = json_decode($text, TRUE);

        $invoice = $data_midtrans['order_id'];
        $transaction_status = $data_midtrans['transaction_status'];
        $payment_type = $data_midtrans['payment_type'];
        $merchant_id = $data_midtrans['merchant_id'];
        $transaction_time = $data_midtrans['transaction_time'];

        $status = $this->main->midtrans_payment_status($payment_type, $transaction_status, $invoice);


        $data_update = array(
            'status' => $status,
            'payment_type' => $payment_type,
            'merchant_id' => $merchant_id,
            'midtrans_callback_date' => $transaction_time,
            'date_payment_accepted' => date('Y-m-d H:i:s')
        );

        $data_update_member_cart_midtrans = [
            'status_code' => $data_midtrans['status_code'],
            'status_message' => $data_midtrans['status_message'],
            'transaction_status' => $data_midtrans['transaction_status']
        ];

        $this->db->where('invoice', $invoice)->update('member_cart', $data_update);
        $this->db->where('order_id', $invoice)->update('member_cart_midtrans', $data_update_member_cart_midtrans);
        $this->db->insert('midtrans', array('invoice' => $invoice, 'text' => $text, 'created_at' => date('Y-m-d H:i:s')));


        $member_cart = $this->db->where('invoice', $invoice)->get('member_cart')->row();
        $this->main->checkout_mail_member($member_cart->id);


        $response = array('status' => 'OK', 'member_cart' => $member_cart);
//
        $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
            ->_display();
        exit;

    }

    function payment_test_email($invoice = "202304040006") {

        $member_cart = $this->db->where('invoice', $invoice)->get('member_cart')->row();
        $this->main->checkout_mail_member($member_cart->id);
    }


}
