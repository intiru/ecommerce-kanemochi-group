<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function list_product()
    {
        $data = $this->main->data_front();
        $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';

        $data['page'] = $this
            ->db
            ->where(array(
                'type' => 'category',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();
        $data['total_product'] = 0;

        foreach ($data['category_menu'] as $row) {
            $row->product_total = $this
                ->db
                ->where(array(
                    'id_category' => $row->id,
                    'use' => 'yes'
                ))
                ->get('product')
                ->num_rows();

            $data['product_total'] += $row->product_total;
        }

        $perpage = 9;
        $offset = $this->uri->segment(2);
        $base_url_pagination = site_url('produk');
        $jumlah_data = $this
            ->db
            ->where(array(
                'product.id_language' => $data['id_language'],
                'product.use' => 'yes'
            ));

        if ($keyword) {
            $jumlah_data = $jumlah_data
                ->join('category', 'category.id = product.id_category', 'left')
                ->like('product.title', $keyword)
                ->or_like('product.title_sub', $keyword)
                ->or_like('product.description', $keyword)
                ->or_like('category.title', $keyword)
                ->or_like('category.description', $keyword);
        }

        $jumlah_data = $jumlah_data
            ->get('product')
            ->num_rows();


        $this->load->library('pagination');
        $config['base_url'] = $base_url_pagination;
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = $perpage;
        $config['reuse_query_string'] = true;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);
        $data['keyword'] = $keyword;
        $data['product_list'] = $this
            ->db
            ->select('p.*, c.title AS category_title')
            ->join('category c', 'c.id = p.id_category', 'left')
            ->where(array(
                'p.id_language' => $data['id_language'],
                'p.use' => 'yes'
            ));

        if ($keyword) {
            $data['product_list'] = $data['product_list']
                ->join('category', 'category.id = p.id_category', 'left')
                ->like('p.title', $keyword)
                ->or_like('p.title_sub', $keyword)
                ->or_like('p.description', $keyword)
                ->or_like('c.title', $keyword)
                ->or_like('c.description', $keyword);
        }

        $data['product_list'] = $data['product_list']
            ->order_by('category_title', 'ASC')
            ->order_by('p.title', 'ASC')
            ->get('product p', $perpage, $offset)
            ->result();

        $this->template->front('product_list', $data);
    }

    public function list_category($slug, $step)
    {
        $data = $this->main->data_front();
        $data['page'] = $this
            ->db
            ->where(array(
                'type' => 'category',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();
        $data['total_product'] = 0;

        $category = $_GET['category'];

        foreach ($data['category_menu'] as $row) {
            $row->product_total = $this
                ->db
                ->where(array(
                    'id_category' => $row->id,
                    'use' => 'yes'
                ))
                ->get('product')
                ->num_rows();

            $data['product_total'] += $row->product_total;
        }

        $perpage = 9;
        $offset = $this->uri->segment(3);
        $base_url_pagination = site_url('produk/' . $slug);
        $id_category = $this->main->str_decrypt($category);

        $id_category_hirarcy = $this->main->user_id_category_hirarcy($id_category);

//        echo json_encode($id_category_hirarcy);
//        exit;

        $id_category_all_in = $this->main->user_id_category_all_in($id_category);

        $jumlah_data = $this
            ->db
            ->where_in('id_category', $id_category_all_in)
            ->get('product')
            ->num_rows();


        $this->load->library('pagination');
        $config['base_url'] = $base_url_pagination;
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = $perpage;
        $config['reuse_query_string'] = true;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);

        $data['id_category_hirarcy'] = $id_category_hirarcy;
        $data['id_category_active'] = $id_category;
        $data['product_list'] = $this
            ->db
            ->select('p.*, pg.thumbnail AS product_thumbnail, c.title AS category_title')
            ->join('category c', 'c.id = p.id_category', 'left')
            ->join('product_gallery AS pg', 'pg.id_product = p.id')
            ->where(array(
                'pg.use_thumbnail' => 'yes'
            ))
            ->where_in('p.id_category', $id_category_all_in)
            ->order_by('category_title', 'ASC')
            ->order_by('p.title', 'ASC')
            ->get('product p', $perpage, $offset)
            ->result();

        $this->template->front('product_list', $data);
    }

    public function list_row()
    {
        $data = $this->main->data_front();
        $data['page'] = $this
            ->db
            ->where(array(
                'type' => 'category',
                'id_language' => $data['id_language']
            ))
            ->get('pages')
            ->row();
        $data['category'] = $this
            ->db
            ->select('title,id')
            ->where(array(
                'use' => 'yes',
                'id_language' => $data['id_language']
            ))
            ->order_by('title', 'ASC')
            ->get('category')
            ->result();

        $jumlah_data = $this
            ->db
            ->where(array(
                'id_language' => $data['id_language']
            ))
            ->get('product')
            ->num_rows();
        $perpage = 8;
        $offset = $this->uri->segment(2);

        $this->load->library('pagination');
        $config['base_url'] = site_url('produk');
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = $perpage;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);

        $data['product_list'] = $this
            ->db
            ->select('p.id,p.id_category,p.title,p.thumbnail,p.thumbnail_alt,p.price,p.price_old,p.description,c.title AS category_title')
            ->join('category c', 'c.id = p.id_category', 'left')
            ->where(array(
                'p.id_language' => $data['id_language']
            ))
            ->order_by('category_title', 'ASC')
            ->order_by('p.title', 'ASC')
            ->get('product p', $perpage, $offset)
            ->result();

        $this->template->front('product_list_row', $data);
    }

    public function detail($slug_category, $slug_product)
    {
        $data = $this->main->data_front();
        $bought_limit = 3;
        $data['page'] = $this
            ->db
            ->select('product.*, category.title AS category_title, pg.thumbnail AS product_thumbnail, pg.thumbnail_alt AS product_thumbnail_alt')
            ->join('product_gallery AS pg', 'pg.id_product = product.id')
            ->join('category', 'category.id = product.id_category')
            ->where('product.slug', $slug_product)
            ->where('pg.use_thumbnail', 'yes')
            ->get('product')
            ->row();
        $data['product_gallery'] = $this->db->where(array('id_product' => $data['page']->id, 'use_view' => 'yes', 'use_thumbnail' => 'no'))->order_by('order_index', 'ASC')->get('product_gallery')->result();
        $data['related'] = $this
            ->db
            ->select('p.*, pg.thumbnail AS product_thumbnail, pg.thumbnail_alt AS product_thumbnail_alt, c.title AS category_title')
            ->join('product_gallery AS pg', 'pg.id_product = p.id')
            ->join('category AS c', 'c.id = p.id_category')
            ->where(array(
                'p.id_language' => $data['id_language'],
                'p.use' => 'yes',
                'pg.use_thumbnail' => 'yes',
                'p.id_category' => $data['page']->id_category
            ))
            ->where_not_in('p.id', [$data['page']->id])
            ->order_by('p.title', 'ASC')
            ->get('product p', 6, 0)
            ->result();

        $product_option = $this->db->where('id_product', $data['page']->id)->get('product_option')->result();
        foreach ($product_option as $row) {
            $row->product_option_value = $this->db->where('id_product_option', $row->id)->get('product_option_value')->result();
        }

        $data['product_option'] = $product_option;

//        echo json_encode($product_option);
//        exit;

        $also_bought = array();
        foreach ($data['related'] as $key => $row) {
            if ($key < $bought_limit) {
                $also_bought[] = $row;
            }
        }

        $data['page']->type = 'category';
        $data['also_bought'] = $this->load->view('front/cart_add_item_bought_list', array('list' => $also_bought), TRUE);
        $data['captcha'] = $this->main->captcha();
        $data['related_count'] = count($data['related']);
        $this->template->front('product_detail', $data);
    }
}
