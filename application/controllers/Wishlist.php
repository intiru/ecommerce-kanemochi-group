<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $this->main->login_check_page();
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'member_wishlist', 'id_language' => $data['id_language']))->get('pages')->row();
        $data['wishlist'] = $this
            ->db
            ->select('product.*, category.title AS category_title, product_gallery.thumbnail as product_thumbnail')
            ->join('category', 'category.id = product.id_category', 'left')
            ->join('product_gallery', 'product_gallery.id_product = product.id', 'left')
            ->join('wishlist', 'wishlist.id_product = product.id')
            ->where(array(
                'product.use' => 'yes',
                'product.best_seller' => 'yes',
                'product_gallery.use_thumbnail' => 'yes',
                'product_gallery.use_view' => 'yes',
                'id_member' => $data['id_member']
            ))
            ->order_by('title', 'ASC')
            ->get('product')
            ->result();
        $this->template->front('member_wishlist', $data);
    }

    public function process()
    {
        $this->main->login_check_page();
        $id_product = $this->input->post('id_product');
        $process_type = $this->input->post('process_type');
        $member = $this->session->userdata('member');
        $id_member = $member->id;
        $product_row = $this->db->where('id', $id_product)->get('product')->row();

        /**
         * Update data  product wishlist
         */
        if ($process_type == 'remove') {
            $wishlist_member_data = json_decode($product_row->wishlist_member_json, TRUE);
            unset($wishlist_member_data[$id_member]);

            if (($key = array_search($id_member, $wishlist_member_data)) !== false) {
                unset($wishlist_member_data[$key]);
            }

        } else {
            if ($product_row->wishlist_json == '') {
                $wishlist_member_data = array($id_member);
            } else {
                $wishlist_member_data = json_decode($product_row->wishlist_member_json, TRUE);
                array_push($wishlist_member_data, $id_member);
            }
        }

        $product_update = array(
            'wishlist_member_json' => json_encode($wishlist_member_data)
        );

        $this->db->where('id', $id_product)->update('product', $product_update);

        /**
         * Update data wishlist history
         */

        if ($process_type == 'remove') {
            $this->db->where(array('id_product' => $id_product, 'id_member' => $id_member))->delete('wishlist');
        } else {
            $this->db->where(array('id_product' => $id_product, 'id_member' => $id_member))->delete('wishlist');
            $data_insert = array(
                'id_product' => $id_product,
                'id_member' => $id_member,
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->db->insert('wishlist', $data_insert);
        }

        echo json_encode(array(
            'status' => 'success',
            'wishlist_member_data' => $wishlist_member_data
        ));
    }
}
