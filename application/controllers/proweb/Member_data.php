<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_data extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index()
    {
        $data = $this->main->data_main();

        $data['member'] = $this
            ->db
            ->order_by('first_name', 'ASC')
            ->order_by('last_name', 'ASC')
            ->get('member')
            ->result();


        $this->template->set('member_data', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Data Member');
        $this->template->load_admin('member_data/index', $data);
    }

    public function member_detail($id_member)
    {
        $member = $this->db->where('id', $id_member)->get('member')->row();

        $data = array(
            'member' => $member
        );

        echo $this->load->view('admins/member_data/member_detail', $data, TRUE);
    }

    public function member_address($id_member)
    {
        $data['address_list'] = $this
            ->db
            ->join('province', 'province.id_province = member_address.id_province')
            ->join('city', 'city.id_city = member_address.id_city')
            ->join('subdistrict', 'subdistrict.id_subdistrict = member_address.id_subdistrict')
            ->where(array(
                'id_member' => $id_member
            ))
            ->order_by('id', 'ASC')
            ->order_by('primary', 'DESC')
            ->get('member_address')
            ->result();

        echo $this->load->view('admins/member_data/member_address', $data, TRUE);
    }

    public function member_wishlist($id_member)
    {
        $data['wishlist'] = $this
            ->db
            ->select('product.*, category.title AS category_title, product_gallery.thumbnail as product_thumbnail')
            ->join('category', 'category.id = product.id_category', 'left')
            ->join('product_gallery', 'product_gallery.id_product = product.id', 'left')
            ->join('wishlist', 'wishlist.id_product = product.id')
            ->where(array(
                'product.use' => 'yes',
                'product.best_seller' => 'yes',
                'product_gallery.use_thumbnail' => 'yes',
                'product_gallery.use_view' => 'yes',
                'id_member' => $id_member
            ))
            ->order_by('title', 'ASC')
            ->get('product')
            ->result();

        echo $this->load->view('admins/member_data/member_wishlist', $data, TRUE);
    }
}
