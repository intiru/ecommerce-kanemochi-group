<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member_data_order extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index()
    {
        $data = $this->main->data_main();

        $data['member_cart'] = $this
            ->db
            ->select('mc.*, m.*, mc.id AS id_member_cart')
            ->join('member m', 'm.id = mc.id_member')
            ->order_by('mc.id', 'DESC')
            ->get('member_cart mc')
            ->result();


        $this->template->set('member_data_order', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Data Pemesanan Member');
        $this->template->load_admin('member_data_order/index', $data);
    }

    public function detail($id_member_cart)
    {

        $data = $this->main->data_main();

        $total_qty = 0;
        $member_cart = $this
            ->db
            ->select('mc.*, m.*, mc.id AS id_member_cart')
            ->join('member m', 'm.id = mc.id_member')
            ->where('mc.id', $id_member_cart)
            ->order_by('mc.id', 'DESC')
            ->get('member_cart mc')
            ->row();
        $member_cart_detail = $this
            ->db
            ->select('mcd.*, p.price AS product_price, mcd.price AS product_price_cart, p.title AS product_title, pg.thumbnail AS product_thumbnail, c.title AS product_category')
            ->join('product p', 'p.id = mcd.id_product', 'left')
            ->join('product_gallery pg', 'pg.id_product = p.id', 'left')
            ->join('category c', 'c.id = p.id_category', 'left')
            ->where('mcd.id_member_cart', $id_member_cart)
            ->where('pg.use_thumbnail', 'yes')
            ->order_by('mcd.id', 'ASC')
            ->get('member_cart_detail mcd')
            ->result();

//        echo json_encode($member_cart_detail);
//        exit;

        $member_address = $this
            ->db
            ->join('province p', 'p.id_province = ma.id_province')
            ->join('city c', 'c.id_city = ma.id_city')
            ->join('subdistrict s', 's.id_subdistrict = ma.id_subdistrict')
            ->where(array(
                'id' => $member_cart->id_member_address,
            ))
            ->get('member_address ma')
            ->row();

        $data = array_merge($data, array(
            'member_cart' => $member_cart,
            'member_cart_detail' => $member_cart_detail,
            'member_address' => $member_address
        ));

        $this->template->set('member_data_order', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Data Pemesanan Member');
        $this->template->load_admin('member_data_order/detail', $data);
    }

    public function tracking($id_member_cart)
    {
        $member_cart = $this->db->where('id', $id_member_cart)->order_by('id', 'DESC')->get('member_cart')->row();

        $data_get = 'key=' . $this->main->rajaongkir_apikey() .
            '&waybill=' . $member_cart->resi .
            '&courier=' . $member_cart->courier_code;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data_get,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: your-api-key"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $data['track'] = json_decode($response, TRUE)['rajaongkir'];

        $data_return['modal_body'] = $this->load->view('front/member_order_detail_tracking', $data, TRUE);

        echo json_encode($data_return);
        exit;
    }

    public function status_change()
    {
        $id_member_cart = $this->input->post('id_member_cart');
        $status = $this->input->post('member_cart_status');
        $resi = $this->input->post('resi');

        if ($status == 'send') {
            $data_update = array(
                'resi' => $resi,
                'status' => $status
            );
        } else {
            $data_update = array(
                'resi' => '',
                'status' => $status
            );
        }


        if ($status == 'send') {

        }


        $this->db->where('id', $id_member_cart)->update('member_cart', $data_update);
        redirect('proweb/member_data_order');

    }
}
