<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_gallery extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('m_product_gallery');
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function list($id_product = '')
    {
        $data = $this->main->data_main();
        $data['gallery'] = $this->db->where('id_product', $id_product)->order_by('order_index', 'ASC')->get('product_gallery')->result();
        $data['id_product'] = $id_product;

        $product_option = $this->db->where('id_product', $id_product)->get('product_option')->result();
        foreach ($product_option as $row) {
            $row->product_option_value = $this->db->where('id_product_option', $row->id)->get('product_option_value')->result();
        }

        $data['product_option'] = $product_option;
        $data['product_option_count'] = count($product_option);

        $this->template->set('gallery', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'management Gallery Photo');
        $this->template->load_admin('product_gallery/index', $data);
    }

    public function order_update() {
        $order_index_arr = $this->input->post('order_index');

        foreach($order_index_arr as $id_product_gallery => $order_index) {
            $this->db->where('id', $id_product_gallery)->update('product_gallery', ['order_index' => $order_index]);
        }
    }

    public function createprocess($id_product = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('thumbnail_alt', 'Thumbnail Alt', 'required');
        $this->form_validation->set_error_delimiters('', '');

        $title = $this->input->post('title');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                    'description' => form_error('description'),
                    'thumbnail_alt' => form_error('thumbnail_alt'),
                )
            ));
        } else {

            $data = $this->input->post(NULL, TRUE);
            $data['id_product'] = $id_product;

            if ($_FILES['thumbnail']['name']) {
                $response = $this->main->upload_file_thumbnail('thumbnail', $title);
                if (!$response['status']) {
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Isi form belum benar',
                        'errors' => array(
                            'image' => $response['message']
                        )
                    ));
                    exit;
                } else {
                    $data['thumbnail'] = $response['filename'];
                }
            }

            if ($data['use_thumbnail'] == 'yes') {
                $where = array(
                    'id_product' => $id_product
                );
                $update = array(
                    'use_thumbnail' => 'no'
                );
                $this->m_product_gallery->update_data($where, $update);
            }

            $check = $this->db->where('id_product', $id_product)->get('product_gallery')->num_rows();
            if ($check > 0) {
                $order_index = $this->db->where('id_product', $id_product)->order_by('order_index', 'DESC')->get('product_gallery')->row()->order_index;
                $order_index += 1;
            } else {
                $order_index = 1;
            }

            asort($data['id_product_option_value']);

            $data['order_index'] = $order_index;
            $data['id_product_option_value_json'] = json_encode($data['id_product_option_value']);
            unset($data['id_product_option_value']);

            $this->m_product_gallery->input_data($data);

            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinput',
                'id' => json_encode($data['id_product_option_value'])
            ));
        }
    }

    function gallery_order()
    {
        $product = $this->db->get('product')->result();
        foreach ($product as $row) {
            $gallery = $this->db->where('id_product', $row->id)->get('product_gallery')->result();
            $order_index = 1;
            foreach ($gallery as $row_gallery) {
                $this->db->where('id', $row_gallery->id)->update('product_gallery', ['order_index' => $order_index]);

                $order_index++;
            }
        }
    }

    public function delete($id)
    {

        $where = array('id' => $id);

        $product_gallery = $this->db->where('id', $id)->get('product_gallery')->row();
        $id_product = $product_gallery->id_product;

        $this->m_product_gallery->delete_data($where);

        $gallery = $this->db->where('id_product', $id_product)->get('product_gallery')->result();
        $order_index = 1;
        foreach ($gallery as $row_gallery) {
            $this->db->where('id', $row_gallery->id)->update('product_gallery', ['order_index' => $order_index]);
            $order_index++;
        }
    }

    public function update($id_product = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('thumbnail_alt', 'Thumbnail Alt', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                    'description' => form_error('description'),
                    'thumbnail_alt' => form_error('thumbnail_alt'),
                )
            ));
        } else {

            $this->load->model('m_slider');
            if ($this->form_validation->run() === FALSE) {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'Isi form belum benar2',
                    'errors' => array(
                        'title' => form_error('title'),
                        'description' => form_error('description'),
                    )
                ));
            } else {

                $id = $this->input->post('id');
                $data = $this->input->post(NULL, TRUE);
                unset($data['id']);
                $where = array(
                    'id' => $id
                );

                if ($_FILES['thumbnail']['name']) {
                    $response = $this->main->upload_file_thumbnail('thumbnail', $data['title']);
                    if (!$response['status']) {
                        echo json_encode(array(
                            'status' => 'error',
                            'message' => 'Isi form belum benar',
                            'errors' => array(
                                'image' => $response['message']
                            )
                        ));
                        exit;
                    } else {
//						$row_data = $this->m_gallery->row_data($where);
//						$this->main->delete_file($row_data->thumbnail);
                        $data['thumbnail'] = $response['filename'];
                    }
                }

                if ($data['use_thumbnail'] == 'yes') {
                    $where = array(
                        'id_product' => $id_product
                    );
                    $update = array(
                        'use_thumbnail' => 'no'
                    );
                    $this->m_product_gallery->update_data($where, $update);
                }

                asort($data['id_product_option_value']);

                $data['id_product_option_value_json'] = json_encode($data['id_product_option_value']);
                unset($data['id_product_option_value']);

                $this->m_product_gallery->update_data(array('id' => $id), $data);
                echo json_encode(array(
                    'status' => 'success',
                    'message' => 'data berhasil diinputkan'
                ));
            }
        }
    }
}
