<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_option extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('m_product_option');
        $this->load->model('m_product');
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function list($id_product = '')
    {
        $data = $this->main->data_main();
        $data['data'] = $this->m_product_option->get_where(array('id_product' => $id_product))->result();
        $data['id_product'] = $id_product;
        $data['product'] = $this->m_product->get_where(['id' => $id_product])->row();

        $this->template->set('gallery', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'management Gallery Photo');
        $this->template->load_admin('product_option/index', $data);
    }

    public function createprocess($id_product = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_error_delimiters('', '');

        $title = $this->input->post('title');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                )
            ));
        } else {

            $data = $this->input->post(NULL, TRUE);
            $data['id_product'] = $id_product;

            $this->m_product_option->input_data($data);

            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinput',
            ));
        }
    }

    public function delete($id)
    {

        $where = array('id' => $id);
        $this->m_product_option->delete_data($where);
    }

    public function update($id_product = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                )
            ));
        } else {

            $this->load->model('m_slider');
            if ($this->form_validation->run() === FALSE) {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'Isi form belum benar2',
                    'errors' => array(
                        'title' => form_error('title'),
                    )
                ));
            } else {

                $id = $this->input->post('id');
                $data = $this->input->post(NULL, TRUE);
                unset($data['id']);
                $where = array(
                    'id' => $id
                );

                $this->m_product_option->update_data(array('id' => $id), $data);
                echo json_encode(array(
                    'status' => 'success',
                    'message' => 'data berhasil diinputkan'
                ));
            }
        }
    }
}
