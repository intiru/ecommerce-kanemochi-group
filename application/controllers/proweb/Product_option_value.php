<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_option_value extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('m_product_option');
        $this->load->model('m_product');
        $this->load->model('m_product_option_value');
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function list($id_product_option = '')
    {
        $data = $this->main->data_main();
        $product_option = $this->m_product_option->get_where(['id' => $id_product_option])->row();
        $product = $this->m_product->get_where(['id' => $product_option->id_product])->row();

        $data['data'] = $this->m_product_option_value->get_where(array('id_product_option' => $id_product_option))->result();
        $data['id_product_option'] = $id_product_option;
        $data['id_product'] = $product->id;
        $data['product'] = $product;
        $data['product_option'] = $product_option;

        $this->template->set('gallery', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'management Gallery Photo');
        $this->template->load_admin('product_option_value/index', $data);
    }

    public function createprocess($id_product_option = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_error_delimiters('', '');

        $title = $this->input->post('title');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                )
            ));
        } else {

            $data = $this->input->post(NULL, TRUE);
            $data['id_product_option'] = $id_product_option;

            $this->m_product_option_value->input_data($data);

            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinput',
            ));
        }
    }

    public function delete($id)
    {
        $where = array('id' => $id);
        $this->m_product_option_value->delete_data($where);
    }

    public function update($id_product_option = '')
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                )
            ));
        } else {
            if ($this->form_validation->run() === FALSE) {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'Isi form belum benar2',
                    'errors' => array(
                        'title' => form_error('title'),
                    )
                ));
            } else {

                $id = $this->input->post('id');
                $data = $this->input->post(NULL, TRUE);
                unset($data['id']);
                $where = array(
                    'id' => $id
                );

                $this->m_product_option_value->update_data(array('id' => $id), $data);
                echo json_encode(array(
                    'status' => 'success',
                    'message' => 'data berhasil diinputkan'
                ));
            }
        }
    }
}
