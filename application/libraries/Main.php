<?php

class Main
{

    private $ci;
    private $web_name = 'MTSC Digital e-Commerce';
    private $web_url = 'mtsc.com';
    private $file_info = 'Images with resolution 800px x 600px and size 100KB';
    private $file_info_slider = 'Images with resolution 1920px x 900px and size 250KB';
    private $path_images = 'upload/images/';
    private $image_size_preview = 200;
    private $help_thumbnail_alt = 'Penting untuk SEO Gambar';
    private $help_meta = 'Penting untuk SEO Halaman Website';
    private $short_desc_char = 180;
    private $currency = 'Rp.';
    private $rajaongkir_apikey = '2ecc568bbed69df540d3a5941b74b2ca';

    // Development
    private $server_key = 'SB-Mid-server-oZgaTfhQoCqueWEHD-Ls-WC2';
    private $client_key = 'SB-Mid-client-sg5xSlUUup9yxPwj';
    private $midtrans_js_link = 'https://app.sandbox.midtrans.com/snap/snap.js';
    private $midtrans_production = FALSE;

    // Production
//    protected $server_key = 'Mid-server-hYq6m8dNZj5XuD6KpSlEK8yg';
//    protected $client_key = 'Mid-client-a3RGHUv2LRPBiNRA';
//    protected $midtrans_js_link = 'https://app.midtrans.com/snap/snap.js';
//    protected $midtrans_production = TRUE;

    public function __construct()
    {
        error_reporting(0);
        $this->ci =& get_instance();
    }

    function rajaongkir_apikey()
    {
        return $this->rajaongkir_apikey;
    }

    public function midtrans_key()
    {
        return [
            'server_key' => $this->server_key,
            'client_key' => $this->client_key,
            'midtrans_js_link' => $this->midtrans_js_link,
            'midtrans_production' => $this->midtrans_production
        ];
    }

    function short_desc($string)
    {
        return substr(strip_tags($string), 0, $this->short_desc_char) . ' ...';
    }

    function web_name()
    {
        return $this->web_name;
    }

    function web_url()
    {
        return $this->web_url;
    }

    function credit()
    {
        return 'development by Mario Teguh Super Club';
    }

    function date_view($date)
    {
        return date('d F Y', strtotime($date));
    }

    function help_thumbnail_alt()
    {
        return $this->help_thumbnail_alt;
    }

    function help_meta()
    {
        return $this->help_meta;
    }

    function file_info()
    {
        return $this->file_info;
    }

    function file_info_slider()
    {
        return $this->file_info_slider;
    }

    function path_images()
    {
        return $this->path_images;
    }

    function image_size_preview()
    {
        return $this->image_size_preview;
    }

    function image_preview_url($filename)
    {
        return base_url($this->path_images . $filename);
    }

    function currency($nominal)
    {
        return 'Rp. ' . number_format($nominal);
    }

    function courier_day($day)
    {
        $find = array('HARI', 'hari', 'Hari');
        $replace = array('');

        $day = str_replace($find, $replace, $day) . ' hari';

        return $day;
    }

    function delete_file($filename)
    {
        if ($filename) {
            if (file_exists(FCPATH . $this->path_images . $filename)) {
//				/unlink($this->path_images . $filename);
            }
        }
    }

    function data_main()
    {
        $id_language = $this->ci->session->userdata('id_language') ?
            $this->ci->session->userdata('id_language') :
            $this->ci->db->select('id')->where('use', 'yes')->order_by('id', 'ASC')->get('language')->row()->id;

        $data = array(
            'web_name' => $this->web_name,
            'menu_list' => $this->menu_list(),
            'name' => $this->ci->session->userdata('name'),
            'language' => $this->ci->db->where('use', 'yes')->get('language')->result(),
            'id_language' => $id_language,
        );

        $tab_language = $this->ci->load->view('admins/components/tab_language', $data, TRUE);
        $data['tab_language'] = $tab_language;

        return $data;
    }

    function data_front()
    {

        $lang_code = 'id';
        $lang_active = $this->ci->db->where('code', $lang_code)->get('language')->row();
        $member_sess = $this->ci->session->userdata('member') == NULL ? (object)array('id' => NULL) : $this->ci->session->userdata('member');

        $show_hide = TRUE;
        $category_menu = $this
            ->ci
            ->db
            ->select('title, id, thumbnail, thumbnail_alt')
            ->where(array(
                'use' => 'yes',
                'id_category_parent' => 0,
                'view_top_menu' => 'yes',
                'id_language' => $lang_active->id
            ))
            ->order_by('title', 'ASC')
            ->get('category')
            ->result();


        $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';

        $uniqid = uniqid();
        $url_session = $this->ci->session->userdata('url_session');
        $url_session = empty($url_session) ? [] : $url_session;
        $url_session = array_merge($url_session, [
            $uniqid => current_url()
        ]);

        $this->ci->session->set_userdata(['url_session' => $url_session]);

        $data = array(
            'address' => 'Sentraland Residence 1 Blok A No.18, Wadas, Telukjambe Timur, Karawang, Jawa Barat 41361',
            'address_link' => 'https://goo.gl/maps/7jUzbz7oBq1RGS7aA',
            'telephone' => '0895-6184-60505',
            'owner_name' => 'Mario Teguh Super Club',
            'phone' => '0895-6184-60505',
            'phone_link' => 'tel:0895618460505',
            'whatsapp' => '0895-6184-60505',
            'whatsapp_link' => 'https://api.whatsapp.com/send?phone=62895618460505&text=Hello,%20i%20want%20to%20ask%20you%20about%20your%20services.',
            'wechat_id' => '',
            'wechat_link' => '',
            'email_link' => 'mailto:kjapanomiyage@gmail.com',
            'email' => 'kjapanomiyage@gmail.com',
            'facebook_link' => 'https://www.facebook.com/kanemochi.jpn.omiyage.',
            'twitter_link' => 'https://twitter.com/',
            'linkedin_link' => '',
            'instagram_link' => 'https://www.instagram.com/kanemochijapanomiyage/',
            'view_secret' => FALSE,
            'author' => 'www.google.com',
            'lang_code' => $lang_code,
            'lang_active' => $lang_active,
            'id_language' => $lang_active->id,
            'show_hide' => $show_hide,
            'category_menu' => $category_menu,
            'web_name' => $this->web_name,
            'cart_list_review' => $this->cart_list_review(),
            'currency' => $this->currency,
            'login_status' => $this->ci->session->userdata('login_status'),
            'id_member' => $member_sess->id,
            'cart_price_total' => 0,
            'url_uniqid' => $this->str_encrypt($uniqid),
            'keyword' => $keyword
        );

        return $data;
    }

    function str_encrypt($string)
    {
        $ciphering = "AES-128-CTR";
        $encryption_iv = '1234567891011121';
        $encryption_key = "MTSCEcommerce";
        $options = 0;
        $encryption = openssl_encrypt($string, $ciphering,
            $encryption_key, $options, $encryption_iv);

        return $encryption;
    }

    function str_decrypt($encryption)
    {
        $ciphering = "AES-128-CTR";
        $options = 0;

        $decryption_iv = '1234567891011121';
        $decryption_key = "MTSCEcommerce";

        $decryption = openssl_decrypt($encryption, $ciphering,
            $decryption_key, $options, $decryption_iv);

        return $decryption;
    }

    function cart_list_review()
    {
        $cart_view_limit = 3;
        $cart_total = $this->ci->cart->total_items();
        $cart_content = $this->ci->cart->contents();

        $data = array(
            'cart_view_limit' => $cart_view_limit,
            'cart_total' => $cart_total,
            'cart_content' => $cart_content
        );

        return $this->ci->load->view('front/cart_list_review', $data, TRUE);
    }

    function translate_number($number)
    {
        switch ($number) {
            case 1:
                return "first";
                break;
            case 2:
                return "second";
                break;
            case 3:
                return "third";
                break;
            case 4:
                return "four";
                break;
        }
    }

    function check_admin()
    {
        if ($this->ci->session->userdata('status') !== 'login') {
            redirect('proweb/login');
        }
    }

    function check_login()
    {
        if ($this->ci->session->userdata('status') == 'login') {
            redirect('proweb/dashboard');
        }
    }

    function permalink($data = array())
    {

        $slug = '';
        $count = count($data);
        foreach ($data as $key => $r) {
            $check_get = substr($r, 0, 1);
            if ($check_get == '?') {
                $slug .= $r;
            } else {
                if ($key == $count) {
                    $space = '';
                } else {
                    $space = '/';
                }

                $slug .= $this->slug($r) . $space;
            }

        }

        return site_url($slug);
    }

    function breadcrumb($data)
    {
        $breadcrumb = '<ul class="breadcrumb">';
        $count = count($data);
        $no = 1;
        foreach ($data as $url => $label) {
            $current = '';
            if ($no == $count) {
                $current = ' class="current"';
            }

            $breadcrumb .= '<li' . $current . '><a href="' . $url . '">' . $label . '</a></li>';
        }

        $breadcrumb .= '</ul>';


        return $breadcrumb;
    }

    function slug($text)
    {

        $find = array(' ', '/', '&', '\\', '\'', ',', '(', ')', '?', '!', ':', '%');
        $replace = array('-', '-', 'and', '-', '-', '-', '', '', '', '', '', 'percent');

        $slug = str_replace($find, $replace, strtolower($text));

        return $slug;
    }

    function kategori_judul($str)
    {
        return ucwords(strtolower($str));
    }

    function date_format_view($date)
    {
        return date('d M Y', strtotime($date));
    }

    function format_currency($price)
    {
        return $this->currency . ' ' . number_format($price);
    }

    function format_star($star)
    {
        $string = '';
        for ($i = 1; $i <= $star; $i++) {
            $string .= '<i class="icon-star voted"></i>';
        }
        for ($i = 1; $i <= (5 - $star); $i++) {
            $string .= '<i class="icon-star"></i>';
        }

        return $string;
    }

    function format_datetime($datetime)
    {
//        return $datetime;
        return date('d F Y, H:i:s');
    }

    function format_datetime_view($datetime)
    {
//        return $datetime;
        return date('d F Y, H:i:s', strtotime($datetime));
    }

    function format_date($datetime)
    {
        return date('d F Y');
    }

    function format_number($number)
    {
        return number_format($number);
    }

    function slug_back($slug)
    {
        $slug = trim($slug);
        if (empty($slug)) return '';
        $slug = str_replace('-', ' ', $slug);
        $slug = ucwords($slug);
        return $slug;
    }

    function upload_file_thumbnail($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;
        $config['max_width'] = 80000;
        $config['max_height'] = 60000;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function upload_file_slider($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 5000;
        $config['max_width'] = 19200;
        $config['max_height'] = 14000;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function captcha()
    {
        $this->ci->load->helper(array('captcha', 'string'));
        $this->ci->load->library('session');

        $vals = array(
            'img_path' => './upload/images/captcha/',
            'img_url' => base_url() . 'upload/images/captcha',
            'img_width' => '200',
            'img_height' => 35,
            'border' => 0,
            'expiration' => 7200,
            'word' => random_string('numeric', 5)
        );

        // create captcha image
        $cap = create_captcha($vals);

        // store image html code in a variable
        $captcha = $cap['image'];

        // store the captcha word in a session,
        $this->ci->session->set_userdata('captcha_mwz', $cap['word']);

        return $captcha;
    }

    function share_link($socmed_type, $title, $link)
    {
        switch ($socmed_type) {
            case "facebook":
                return "https://www.facebook.com/sharer/sharer.php?u=" . $link;
                break;
            case "twitter":
                return "https://twitter.com/home?status=" . $link;
                break;
            case "googleplus":
                return "https://plus.google.com/share?url=" . $link;
                break;
            case "linkedin":
                return "https://www.linkedin.com/shareArticle?mini=true&url=" . $link . "&title=" . $title . "&summary=&source=";
                break;
            case "pinterest":
                return "https://pinterest.com/pin/create/button/?url=" . $title . "&media=" . $link . "&description=";
                break;
            case "email":
                return "mailto:" . $link . "?&subject=" . $title;
            default:
                return $link;
                break;
        }
    }

    function mailer_auth($subject, $to_email, $to_name, $body, $file = '')
    {
        $this->ci->load->library('my_phpmailer');
        $mail = new PHPMailer;

        try {
            $mail->IsSMTP();
            $mail->SMTPSecure = "ssl";
            $mail->Host = "tls://smtp.gmail.com"; //hostname masing-masing provider email
//            $mail->SMTPDebug = 2;
            $mail->SMTPDebug = false;
            $mail->do_debug = 0;
            $mail->Port = 587;
            $mail->SMTPAuth = true;
            $mail->Username = "kanemochigroup.payment@gmail.com"; //user email
            $mail->Password = "suqwnzwhpxojgzqb"; //password email
            $mail->SetFrom("kanemochigroup.payment@gmail.com", 'No Reply - Kanemochi Email'); //set email pengirim
            $mail->Subject = $subject; //subyek email
            $mail->AddAddress($to_email, $to_name); //tujuan email
            $mail->MsgHTML($body);
            if ($file) {
                $mail->addAttachment("upload/images/" . $file);
            }
            $mail->Send();
            //echo "Message has been sent";
        } catch (phpmailerException $e) {
            echo 'ERROR MANIS';
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo 'ERROR TIDAK TAHU';
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    function menu_list()
    {
        $menu = array(
            'MAIN' => array(
                'dashboard' => array(
                    'label' => 'Dashboard',
                    'route' => base_url('proweb/dashboard'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'view_front' => array(
                    'label' => 'View Website',
                    'route' => base_url(''),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                )
            ),
            'PAGES' => array(
                'home' => array(
                    'label' => 'Beranda',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'home_page' => array(
                            'label' => 'Halama Umum Beranda',
                            'route' => base_url('proweb/pages/type/home'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'home_slider' => array(
                            'label' => 'Home Slider',
                            'route' => base_url('proweb/home_slider'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'home_baris_produk' => array(
                            'label' => 'Sesi Baris Produk',
                            'route' => base_url('proweb/home_product_row'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'category' => array(
                    'label' => 'Data Produk',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'ebook_page' => array(
                            'label' => 'Halaman Kategori Produk',
                            'route' => base_url('proweb/pages/type/category'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'category' => array(
                            'label' => 'Data Kategori Produk',
                            'route' => base_url('proweb/category/list/0'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'product' => array(
                            'label' => 'Data Produk',
                            'route' => base_url('proweb/product'),
                            'icon' => 'fab fa-asymmetrik'
                        )
                    )
                ),
                'how_shop' => array(
                    'label' => 'Cara Belanja',
                    'route' => base_url('proweb/pages/type/how_shop'),
                    'icon' => 'fab fa-asymmetrik'
                ),
                'gallery_photo' => array(
                    'label' => 'Galeri Foto',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'gallery_photo_page' => array(
                            'label' => 'Halaman Galeri Foto',
                            'route' => base_url('proweb/pages/type/gallery_photo'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'gallery_photo_list' => array(
                            'label' => 'Daftar Galeri Foto',
                            'route' => base_url('proweb/gallery_photo'),
                            'icon' => 'fab fa-asymmetrik'

                        ),
                    )
                ),
                'blog' => array(
                    'label' => 'Blog',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'blog_page' => array(
                            'label' => 'Halaman Blog',
                            'route' => base_url('proweb/pages/type/blog'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'blog_category' => array(
                            'label' => 'Kategori Blog',
                            'route' => base_url('proweb/blog_category'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'blog_list' => array(
                            'label' => 'Daftar Blog',
                            'route' => base_url('proweb/blog_content'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'about_us' => array(
                    'label' => 'Tentang Kami',
                    'route' => base_url('proweb/pages/type/about_us'),
                    'icon' => 'fab fa-asymmetrik'
                ),
                'contact_us' => array(
                    'label' => 'Kontak Kami',
                    'route' => base_url('proweb/pages/type/contact_us'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'member_data' => array(
                    'label' => 'Data Member',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'data_member' => array(
                            'label' => 'Data Member',
                            'route' => base_url('proweb/member_data'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                        'data_pemesanan_member' => array(
                            'label' => 'Data Pemesanan Member',
                            'route' => base_url('proweb/member_data_order'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                    ),
                ),
                'member_pages' => array(
                    'label' => 'Menu Member',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'wishlist' => array(
                            'label' => 'Halaman Wishlist',
                            'route' => base_url('proweb/pages/type/member_wishlist'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                        'help' => array(
                            'label' => 'Halaman Bantuan',
                            'route' => base_url('proweb/pages/type/member_help'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                        'profile' => array(
                            'label' => 'Halaman Profil Member',
                            'route' => base_url('proweb/pages/type/member_profile'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                        'track' => array(
                            'label' => 'Halaman Track Order',
                            'route' => base_url('proweb/pages/type/member_track_order'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                        'order' => array(
                            'label' => 'Halaman Pemesanan Member',
                            'route' => base_url('proweb/pages/type/member_order_list'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                        'address' => array(
                            'label' => 'Halaman Alamat Pengiriman',
                            'route' => base_url('proweb/pages/type/member_address'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                    ),
                ),
                'pages' => array(
                    'label' => 'Manajemen Halaman',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'faq' => array(
                            'label' => 'Halaman FAQ',
                            'route' => base_url('proweb/pages/type/faq'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                        'privacy' => array(
                            'label' => 'Halaman Kebijakan & Privasi',
                            'route' => base_url('proweb/pages/type/privacy'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                        'order_data' => array(
                            'label' => 'Halaman Daftar Pemesanan',
                            'route' => base_url('proweb/pages/type/order_data'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                        'cart' => array(
                            'label' => 'Halaman Keranjang Belanja',
                            'route' => base_url('proweb/pages/type/cart'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                        'checkout' => array(
                            'label' => 'Halaman Checkout',
                            'route' => base_url('proweb/pages/type/checkout'),
                            'icon' => 'fab fa-asymmetrik',
                            'sub_menu' => array()
                        ),
                    )
                ),
            ),
            'OTHERS MENU' => array(
                'file_manager' => array(
                    'label' => 'File Manager',
                    'route' => base_url('proweb/file_manager'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'email' => array(
                    'label' => 'Email',
                    'route' => base_url('proweb/email'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),

                'admin' => array(
                    'label' => 'Manage Admin',
                    'route' => base_url('proweb/admin'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
            ),
        );

        return $menu;
    }

    public function login_check_page()
    {
        if (!$this->ci->session->userdata('login_status')) {
            $url_redirect_login = $this->ci->session->userdata('url_redirect_login');
            if ($url_redirect_login) {
                redirect($url_redirect_login);
            } else {
                redirect();
            }
        }
    }

    public function checkout_product_check()
    {
        if ($this->ci->cart->total_items() == 0) {
            redirect('produk');
        }
    }

    public function product_member_wishlist_status($row)
    {
        $member_sess = $this->ci->session->userdata('member');
        $login_status = $this->ci->session->userdata('login_status');
        $id_member = $member_sess->id;
        $wishlist_member_status = FALSE;
        if ($login_status) {
            $wishlist_member = json_decode($row->wishlist_member_json, TRUE);
            if (in_array($id_member, $wishlist_member)) {
                $wishlist_member_status = TRUE;
            }
        }

        return $wishlist_member_status;
    }

    public function product_item_view($row)
    {
        $link = $this->permalink(array('produk', $row->category_title, $row->title));
        $wishlist_member_status = $this->product_member_wishlist_status($row);
        $thumbnail = $this
            ->ci
            ->db
            ->select('thumbnail')
            ->where('id_product', $row->id)
            ->where('use_view', 'yes')
            ->where('use_thumbnail', 'yes')
            ->get('product_gallery')
            ->row()
            ->thumbnail;

        $data = array(
            'link' => $link,
            'wishlist_member_status' => $wishlist_member_status,
            'row' => $row,
            'thumbnail' => $thumbnail
        );

        return $this->ci->load->view('front/components/product_item', $data, TRUE);
    }

    public function member_login_refill_cart()
    {
        $member = $this->ci->session->userdata('member');
        $id_member = $member->id;

        $member_cart = $this
        ->ci
            ->db
            ->where([
                'id_member' => $id_member,
                'status' => 'start'
            ])
            ->get('member_cart')
            ->row();


        $member_cart_detail = $this
            ->ci
            ->db
            ->select('p.*, p.price AS product_price, p.title AS product_title, p.id AS id_product, ct.price AS ct_price, ct.*, ct.id AS id_member_cart_detail, c.title AS category_title, pg.thumbnail as product_thumbnail')
            ->join('member_cart mc', 'mc.id = ct.id_member_cart')
            ->join('product p', 'p.id = ct.id_product')
            ->join('category c', 'c.id = p.id_category')
            ->join('product_gallery pg', 'pg.id_product = p.id', 'left')
            ->where(array(
                'mc.id_member' => $id_member,
                'ct.id_member_cart' => $member_cart->id,
                'pg.use_thumbnail' => 'yes',
                'pg.use_view' => 'yes',
                'ct.product_status' => 'product_in_cart'
            ))
            ->get('member_cart_detail ct')
            ->result();

        $cart_insert = array();
        foreach ($member_cart_detail as $row) {
            $product_option = [];
            $id_product_option_json = $row->id_product_option_json;
            $id_product_option_arr = json_decode($id_product_option_json, TRUE);
            foreach ($id_product_option_arr as $id_product_option => $id_product_option_value) {
                $product_option[$id_product_option]['option'] = $this->ci->db->where('id', $id_product_option)->get('product_option')->row()->title;
                $product_option[$id_product_option]['value'] = $this->ci->db->where('id', $id_product_option_value)->get('product_option_value')->row()->title;
            }

            $cart_insert[] = array(
                'id' => $row->id_product,
                'qty' => $row->qty,
                'price' => $row->product_price,
                'name' => $this->trim_special_char($row->product_title),
                'options' => array(
                    'thumbnail' => $this->ci->main->image_preview_url($row->product_thumbnail),
                    'checked_status' => $row->checked_status,
                    'category_title' => $row->category_title,
                    'id_member_cart' => $row->id_member_cart,
                    'id_member_cart_detail' => $row->id_member_cart_detail,
                    'id_product_option_json' => $row->id_product_option_json,
                    'product_option' => $product_option,
                )
            );

        }

        $this->ci->cart->insert($cart_insert);
    }

    /**
     * Proses ini dilakukan untuk mengupdate produk yang ada di member cart detail jika sebelumnya salah satu nya sudah di checkout
     * selanjutnya sisa product di member cart detail akan masuk ke member cart start yang baru
     */
    function generate_member_cart_start()
    {
        $member = $this->ci->session->userdata('member');
        $id_member = $member->id;

        /**
         * Check apakah member mempunyai member cart start atau belum, jika belum buat terlebih dahulu, baru bisa generate invoice
         */
        $check_member_cart_start = $this
            ->ci
            ->db->where(['id_member' => $id_member, 'status' => 'start'])
            ->get('member_cart')
            ->num_rows();

        if ($check_member_cart_start == 0) {

            /**
             * Jika kosong, akan membuat member cart start baru
             */
            $data_member_cart_insert = array(
                'id_member' => $id_member,
                'cart_date' => date('Y-m-d H:i:s'),
                'cart_total' => 0,
                'status' => 'start',
            );
            $this->ci->db->insert('member_cart', $data_member_cart_insert);
            $id_member_cart = $this->ci->db->insert_id();

            /**
             * Jika kosong, maka product keranjang belanja yg tidak dipilih/dicentang sebelumnya akan masuk ke member cart start yang baru
             */
            $member_cart_detail = $this
                ->ci
                ->db
                ->select('member_cart_detail.id')
                ->join('member_cart', 'member_cart.id = member_cart_detail.id_member_cart')
                ->where([
                    'member_cart.id_member' => $id_member,
                    'product_status' => 'product_in_cart'
                ])
                ->get('member_cart_detail')
                ->result();

            foreach ($member_cart_detail as $row) {
                $this->ci->db->where('id', $row->id)->update('member_cart_detail', ['id_member_cart' => $id_member_cart]);
            }

        }
    }

    function invoice_number_generate()
    {
        $member = $this->ci->session->userdata('member');
        $id_member = $member->id;

        $this->generate_member_cart_start();

        $invoice = $this->ci->db->select('invoice')->where(array('id_member' => $id_member, 'status' => 'start'))->get('member_cart')->row()->invoice;

        if (!$invoice) {
            $generate_invoice = $this->invoice_generate();
            $invoice = $generate_invoice['invoice'];
            $invoice_number = $generate_invoice['invoice_number'];

            $this
                ->ci
                ->db
                ->where(array(
                    'id_member' => $id_member,
                    'status' => 'start'
                ))
                ->update('member_cart', array(
                    'invoice' => $invoice,
                    'invoice_number' => $invoice_number
                ));
        }
    }

    /**
     * @return string[]
     *
     * Generate hanya invoice saja
     */
    function invoice_generate()
    {
        $date_now = date('Y-m-d');
        $invoice_latest = $this
            ->ci
            ->db
            ->where(array('DATE(cart_date)' => $date_now))
            ->order_by('invoice_number', 'DESC')
            ->get('member_cart')
            ->row()
            ->invoice;
        $invoice_latest_number = substr($invoice_latest, 8);
        $invoice_number = $invoice_latest_number + 1;

        if (strlen($invoice_number) <= 3) {
            $invoice = date('Ymd') . str_pad($invoice_number, 4, "0", STR_PAD_LEFT);
        } else {
            $invoice = date('Ymd') . $invoice_number;
        }

        return [
            'invoice' => $invoice,
            'invoice_number' => $invoice_number
        ];
    }

    function invoice_number_generate_when_exist()
    {
        $member = $this->ci->session->userdata('member');
        $id_member = $member->id;

        $invoice_generate = $this->invoice_generate();
        $invoice = $invoice_generate['invoice'];
        $invoice_number = $invoice_generate['invoice_number'];

        $this
            ->ci
            ->db
            ->where(array('id_member' => $id_member, 'status' => 'start'))
            ->update('member_cart', array('invoice' => $invoice, 'invoice_number' => $invoice_number));

        return [
            'invoice' => $invoice,
            'invoice_number' => $invoice_number
        ];
    }

    function member_cart_status($status)
    {
        $status_label = '';
        switch ($status) {
            case "expired":
                $status_label = 'Pembayaran Kadarluarsa';
                break;
            case "delivered":
                $status_label = 'Pesanan Terkirim';
                break;
            case "received":
                $status_label = 'Pesanan sudah Diterima';
                break;
            case "pending":
                $status_label = 'Menunggu Pembayaran';
                break;
            case "payment_accepted":
                $status_label = 'Pembayaran Selesai';
                break;
            case "error":
                $status_label = 'Terjadi Kesalahan';
                break;
            case "reject" :
                $status_label = 'Dibatalkan';
                break;
            case "packed":
                $status_label = 'Sedang Dikemas';
                break;
            case "send":
                $status_label = 'Sedang Pengiriman';
                break;
            case "success":
                $status_label = 'Pembelian Selesai';
                break;
            case "rated":
                $status_label = 'Sudah Direview';
                break;
            case "start":
                $status_label = 'Dalam Keranjang Belanja';
                break;
            default:
                $status_label = $status;
        }

        return $status_label;
    }

    function member_cart_id_check($id_member_cart)
    {
        $member = $this->ci->session->userdata('member');
        $id_member = $member->id;
        $check = $this->ci->db->where(array('id_member' => $id_member, 'id' => $id_member_cart))->get('member_cart')->num_rows();
        if ($check == 0) {
            redirect();
        }
    }

    function encrypt($simple_string)
    {
        $ciphering = "RSGo";
        $iv_length = openssl_cipher_iv_length($ciphering);
        $options = 0;
        $encryption_iv = random_bytes($iv_length);
        $encryption_key = openssl_digest(php_uname(), 'MD5', TRUE);

        $encryption = openssl_encrypt($simple_string, $ciphering,
            $encryption_key, $options, $encryption_iv);

        return $encryption;
    }

    function decrypt($encryption)
    {
        $ciphering = "RSGo";
        $options = 0;
        $iv_length = openssl_cipher_iv_length($ciphering);
        $decryption_iv = random_bytes($iv_length);
        $encryption_iv = random_bytes($iv_length);
        $decryption_key = openssl_digest(php_uname(), 'MD5', TRUE);
        $decryption = openssl_decrypt($encryption, $ciphering,
            $decryption_key, $options, $encryption_iv);

        return $decryption;
    }

    function midtrans_payment_status($payment_type, $transaction_status, $invoice = NULL)
    {

        $status = 'pending';

        switch ($payment_type) {
            case "credit_card":
                switch ($transaction_status) {
                    case "capture":

                        if ($invoice) {
                            $status = $this
                                ->db
                                ->select('status')
                                ->where('invoice', $invoice)
                                ->get('member_cart')
                                ->row()
                                ->status;
                        } else {
                            $status = 'pending';
                        }
                        break;
                    case "deny":
                        $status = 'error';
                        break;
                    case "settlement":
                        $status = 'payment_accepted';
                }
                break;
            case "bank_transfer"
                || "echannel"
                || "bca_klikpay"
                || "bca_klikbca"
                || "mandiri_clickpay"
                || "bri_epay"
                || "cimb_clicks"
                || "danamon_online"
                || "gopay"
                || "cstore"
                || "akulaku":
                switch ($transaction_status) {
                    case "pending":
                        $status = 'pending';
                        break;
                    case "settlement":
                        $status = 'payment_accepted';
                        break;
                    case "expire"
                        || "deny":
                        $status = 'error';
                        break;
                }
                break;
        }

        return $status;

    }

    function midtrans_payment_status_label($payment_type, $transaction_status, $invoice = NULL)
    {

        $status = 'pending';

        switch ($payment_type) {
            case "credit_card":
                switch ($transaction_status) {
                    case "capture":

                        if ($invoice) {
                            $status = $this
                                ->db
                                ->select('status')
                                ->where('invoice', $invoice)
                                ->get('member_cart')
                                ->row()
                                ->status;
                        } else {
                            $status = 'pending';
                        }
                        break;
                    case "deny":
                        $status = 'error';
                        break;
                    case "settlement":
                        $status = 'payment_accepted';
                }
                break;
            case "bank_transfer"
                || "echannel"
                || "bca_klikpay"
                || "bca_klikbca"
                || "mandiri_clickpay"
                || "bri_epay"
                || "cimb_clicks"
                || "danamon_online"
                || "gopay"
                || "cstore"
                || "akulaku":
                switch ($transaction_status) {
                    case "pending":
                        $status = 'pending';
                        break;
                    case "settlement":
                        $status = 'payment_accepted';
                        break;
                    case "expire"
                        || "deny":
                        $status = 'error';
                        break;
                }
                break;
        }

        switch ($status) {
            case "pending":
                $status_label = 'Menunggu Pembayaran';
                break;
            case "error":
                $status_label = 'Terjadi Kesalahann';
                break;
            case "payment_accepted":
                $status_label = 'Lunas';
                break;
            default:
                $status_label = 'Menunggu Pembayaran';
                break;
        }

        return $status_label;

    }

    function midtrans_payment_type_label($payment_type)
    {
        switch ($payment_type) {
            case "credit_card":
                $label = 'Kredit Card';
                break;
            case "bank_transfer":
                $label = 'Transfer Bank';
                break;
            case "gopay":
                $label = 'Gopay';
                break;
            case "qris":
                $label = 'ShoopePay';
                break;
            case "cstore":
                $label = 'Store';
                break;
            default:
                $label = $payment_type;
        }

        return $label;
    }

    function admin_category_breadcrumb_hirarcy($id_category = 0, $category_order = [], $i = 0)
    {
        $category = $this->ci->db->where('id', $id_category)->get('category')->row();
        $id_category_parent = $category->id_category_parent;

        if (count($category_order) == 0) {
            $category_order[$i]['title'] = $category->title;
            $category_order[$i]['id'] = $category->id;
        }

        $check = $this
            ->ci
            ->db
            ->where('id', $id_category_parent)
            ->get('category')
            ->num_rows();
        $i++;
        if ($check > 0) {
            $category = $this
                ->ci
                ->db
                ->where('id', $id_category_parent)
                ->get('category')
                ->row();

            $category_order[$i]['title'] = $category->title;
            $category_order[$i]['id'] = $category->id;


            return $this->admin_category_breadcrumb_hirarcy($category->id, $category_order, $i);
        } else {

            $category_order[$i]['title'] = 'Kategori';
            $category_order[$i]['id'] = 0;
            krsort($category_order);
            $breadcrumb_category = '';
            foreach ($category_order as $key => $row) {
                $breadcrumb_category .= '<a href="' . base_url('proweb/category/list/' . $row['id']) . '">' . $row['title'] . '</a> / ';
            }

            return $breadcrumb_category;
        }
    }

    function admin_category_option_hirarcy($id_category = 0, $category_option = [], $i = 0)
    {

        $category = $this->ci->db->where('id_category_parent', $id_category)->get('category')->result();
        $i++;
        foreach ($category as $key => $row) {
            $category_option[$i]['title'] = $row->title;
            $category_option[$i]['id'] = $row->id;

            $check = $this->ci->db->where('id_category_parent', $row->id)->get('category')->num_rows();

            if ($check == 0) {

            }

            return $this->admin_category_option_hirarcy($row->id, $category_option, $i);

        }
    }


    function user_id_category_hirarcy($id_category = 0, $category_order = [], $i = 0)
    {
        $category = $this->ci->db->where('id', $id_category)->get('category')->row();
        $id_category_parent = $category->id_category_parent;

        if (count($category_order) == 0) {
            $category_order[] = $category->id;
        }

        $check = $this
            ->ci
            ->db
            ->where('id', $id_category_parent)
            ->get('category')
            ->num_rows();
        $i++;
        if ($check > 0) {
            $category = $this
                ->ci
                ->db
                ->where('id', $id_category_parent)
                ->get('category')
                ->row();

            $category_order[] = $category->id;


            return $this->user_id_category_hirarcy($category->id, $category_order, $i);
        } else {
            return $this->id_category_all_get_hirary($category_order);
        }
    }

    function id_category_all_get_hirary($category_order = [])
    {
        $id_category_open = [];
        $key = 0;
        foreach ($category_order as $id_category) {
            $id_category_open[$key] = $id_category;
            $key++;
            $category_1 = $this->ci->db->where('id_category_parent', $id_category)->get('category')->result();
            foreach ($category_1 as $row_1) {
                $id_category_open[$key] = $row_1->id;
                $key++;
//                $category_2 = $this->ci->db->where('id_category_parent', $row_1->id)->get('category')->result();
//                foreach($category_2 as $row_2) {
//                    $id_category_open[$key] = $row_2->id.' - '.$row_2->title;
//                    $key++;
//                    $category_3 = $this->ci->db->where('id_category_parent', $row_2->id)->get('category')->result();
//                    foreach($category_3 as $row_3) {
//                        $id_category_open[$key] = $row_3->id;
//                        $key++;
//                        $category_4 = $this->ci->db->where('id_category_parent', $row_3->id)->get('category')->result();
//                        foreach($category_4 as $row_4) {
//                            $id_category_open[$key] = $row_4->id;
//                            $key++;
//                            $category_5 = $this->ci->db->where('id_category_parent', $row_4->id)->get('category')->result();
//                            foreach($category_5 as $row_5) {
//                                $id_category_open[$key] = $row_5->id;
//                                $key++;
//                                $category_6 = $this->ci->db->where('id_category_parent', $row_5->id)->get('category')->result();
//                                foreach($category_6 as $row_6) {
//                                    $id_category_open[$key] = $row_6->id;
//                                    $key++;
////                                    $category_6 = $this->ci->db->where('id_category_parent', $row_6->id)->get('category')->result();
//                                }
//                            }
//                        }
//                    }
//                }
            }
        }

        return $id_category_open;
    }

    function user_category_sub($id_category)
    {
        return $this
            ->ci
            ->db
            ->where([
                'id_category_parent' => $id_category,
                'use' => 'yes'
            ])
            ->order_by('title', 'ASC')
            ->get('category')
            ->result();
    }


    function user_category_breadcrumb_hirarcy($id_category = 0, $category_order = [], $i = 0)
    {
        $category = $this->ci->db->where('id', $id_category)->get('category')->row();
        $id_category_parent = $category->id_category_parent;

        if (count($category_order) == 0) {
            $category_order[$i]['title'] = $category->title;
            $category_order[$i]['id'] = $category->id;
        }

        $check = $this
            ->ci
            ->db
            ->where('id', $id_category_parent)
            ->get('category')
            ->num_rows();
        $i++;
        if ($check > 0) {
            $category = $this
                ->ci
                ->db
                ->where('id', $id_category_parent)
                ->get('category')
                ->row();

            $category_order[$i]['title'] = $category->title;
            $category_order[$i]['id'] = $category->id;


            return $this->user_category_breadcrumb_hirarcy($category->id, $category_order, $i);
        } else {
            krsort($category_order);
            $breadcrumb_category = '';
            foreach ($category_order as $key => $row) {
                $breadcrumb_category .= '
                    <li>
                        <a href="' . $this->permalink(array('produk', $row['title'], '?category=' . $this->str_encrypt($row['id']))) . '">' . ucwords(strtolower($row['title'])) . '</a>
                    </li>
                ';
            }

            return $breadcrumb_category;
        }
    }

    function user_id_category_all_in($id_category)
    {
        $id_category_all = [$id_category];
        $category_0 = $this->ci->db->where('id_category_parent', $id_category)->get('category')->result();
        foreach ($category_0 as $row_0) {
            $id_category_all[] = $row_0->id;
            $category_1 = $this->ci->db->where('id_category_parent', $row_0->id)->get('category')->result();
            foreach ($category_1 as $row_1) {
                $id_category_all[] = $row_1->id;
                $category_2 = $this->ci->db->where('id_category_parent', $row_1->id)->get('category')->result();
                foreach ($category_2 as $row_2) {
                    $id_category_all[] = $row_2->id;
                    $category_3 = $this->ci->db->where('id_category_parent', $row_2->id)->get('category')->result();
                    foreach ($category_3 as $row_3) {
                        $id_category_all[] = $row_3->id;
                        $category_4 = $this->ci->db->where('id_category_parent', $row_3->id)->get('category')->result();
                        foreach ($category_4 as $row_4) {
                            $id_category_all[] = $row_4->id;
                            $category_5 = $this->ci->db->where('id_category_parent', $row_4->id)->get('category')->result();
                            foreach ($category_5 as $row_5) {
                                $id_category_all[] = $row_5->id;
                                $category_6 = $this->ci->db->where('id_category_parent', $row_5->id)->get('category')->result();
                                foreach ($category_6 as $row_6) {
                                    $id_category_all[] = $row_6->id;
//                                    $category_3 = $this->ci->db->where('id_category_parent', $row_6->id)->get('category')->result();
                                }
                            }
                        }
                    }
                }
            }
        }

        return $id_category_all;
    }

    public function checkout_mail_member($id_member_cart = 29)
    {

        $member_cart = $this->ci->db->where('id', $id_member_cart)->get('member_cart')->row();
        $member_cart_midtrans = $this->ci->db->where('id_member_cart', $id_member_cart)->get('member_cart_midtrans')->row();
        $member = $this->ci->db->where('id', $member_cart->id_member)->get('member')->row();
        $member_address = $this
            ->ci
            ->db
            ->join('province p', 'p.id_province = ma.id_province')
            ->join('city c', 'c.id_city = ma.id_city')
            ->join('subdistrict s', 's.id_subdistrict = ma.id_subdistrict')
            ->where(array(
                'id_member' => $member_cart->id_member,
                'primary' => 'yes'
            ))
            ->get('member_address ma')
            ->row();

        $member_cart_detail = $this
            ->ci
            ->db
            ->select('mcd.*, p.price AS product_price, mcd.price AS product_price_cart, p.title AS product_title, pg.thumbnail AS product_thumbnail, c.title AS product_category')
            ->join('product p', 'p.id = mcd.id_product')
            ->join('product_gallery pg', 'pg.id_product = p.id')
            ->join('category c', 'c.id = p.id_category')
            ->where('mcd.id_member_cart', $id_member_cart)
            ->where('pg.use_thumbnail', 'yes')
            ->where('mcd.checked_status', 'yes')
            ->order_by('mcd.id')
            ->get('member_cart_detail mcd')
            ->result();

        $member_cart_product_count = 0;
        foreach($member_cart_detail as $row) {
            $member_cart_product_count += $row->qty;
        }


        $data = array(
            'member_cart' => $member_cart,
            'member_address' => $member_address,
            'member_cart_detail' => $member_cart_detail,
            'member_cart_product_count' => $member_cart_product_count,
            'cart_price_total' => 0,
            'member_cart_midtrans' => $member_cart_midtrans
        );

        $body = $this->ci->load->view('front/mail/member_checkout', $data, TRUE);
//        exit;
        $subject = 'Checkout #' . $member_cart->invoice;
        if ($member_cart->status == 'payment_accepted') {
            $subject = 'Pembayaran Berhasil #' . $member_cart->invoice;
        }
        $to_email = $member->email;
        $to_name = $member->first_name . ' ' . $member->last_name;

        $this->mailer_auth($subject, $to_email, $to_name, $body, $file = '');
        $this->mailer_auth($subject, 'kanemochigroup.payment@gmail.com', 'Admin Kanemochi', $body, $file = '');
    }


    // Trim special character from text
    public function trim_special_char($text)
    {
        $str = str_replace("(", '_:', $text);
        $str = str_replace(")", ':_', $str);
        $str = str_replace("/", '_slash', $str);
        $str = str_replace("+", '_plus', $str);
        $str = str_replace("&", '_and', $str);
        $str = str_replace("'", '_ss', $str);
        $str = str_replace("x", '_X', $str);
        $str = str_replace('"', '_cot', $str);

        return $str;
    }

    // Set special character from previous text
    public function set_special_char($text)
    {
        $str = str_replace('_:', "(", $text);
        $str = str_replace(':_', ")", $str);
        $str = str_replace('_slash', "/", $str);
        $str = str_replace('_plus', "+", $str);
        $str = str_replace('_and', "&", $str);
        $str = str_replace('_ss', "'", $str);
        $str = str_replace('_X', "x", $str);
        $str = str_replace('_cot', '"', $str);

        return $str;
    }
}
