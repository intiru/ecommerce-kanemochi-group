<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="flaticon-avatar"></i>
			</span>
            <h3 class="kt-portlet__head-title">
                Data Member
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <table class="table table-striped- table-bordered table-hover table-checkable datatable">
            <thead>
            <tr>
                <th class="d-none"></th>
                <th width="20">No</th>
                <th width="100">Avatar</th>
                <th>Name</th>
                <th>Email</th>
                <th>Telepon</th>
                <th width="130">Option</th>
            </tr>
            </thead>
            <?php $no = 1 ?>
            <tbody>
            <?php foreach ($member as $datas) : ?>
                <tr>
                    <td><?php echo $no ?></td>
                    <td><img src="<?php echo $datas->picture ?>" width="100"></td>
                    <td><?php echo $datas->name ?></td>
                    <td><?php echo $datas->email ?></td>
                    <td><?php echo $datas->phone ?></td>
                    <td width="50">
                        <div class="btn-group-vertical btn-group-sm">
                            <a href="javascript:;"
                               data-action="<?php echo base_url() ?>proweb/member_data/member_detail/<?php echo $datas->id ?>"
                               data-title="Detail Member"
                               class="btn btn-primary btn-elevate-air btn-member-detail">
                                Detail Member
                            </a>
                            <a href="javascript:;"
                               data-action="<?php echo base_url() ?>proweb/member_data/member_address/<?php echo $datas->id ?>"
                               data-title="Daftar Alamat Member"
                               class="btn btn-success btn-elevate-air btn-member-detail">
                                Daftar Alamat
                            </a>
                            <a href="javascript:;"
                               data-action="<?php echo base_url() ?>proweb/member_data/member_wishlist/<?php echo $datas->id ?>"
                               data-title="Daftar Wishlist Member"
                               class="btn btn-info btn-elevate-air btn-member-detail">
                                Daftar Wishlist
                            </a>
                        </div>
                    </td>
                </tr>
                <?php $no++ ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal" id="modal-member-detail" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

