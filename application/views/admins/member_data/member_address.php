<?php foreach($address_list as $row) { ?>
                            <div class="row">
                                <div class="col-12 col-lg-4">Nama Pemilik/Toko</div>
                                <div class="col-12 col-lg-8">
                                    <strong><?php echo $row->owner_name ?></strong>
                                    <?php if($row->primary == 'yes') { ?>
                                        <button type="button" class="btn btn-sm btn-success">Utama</button>
                                    <?php } ?>
                                </div>
                                <div class="col-12 col-lg-4">Telepon</div>
                                <div class="col-12 col-lg-8"><?php echo $row->phone ?></div>
                                <div class="col-12 col-lg-4">Alamat</div>
                                <div class="col-12 col-lg-8">
                                    <?php echo $row->address ?>
                                    <?php echo $row->city_name ?> - <?php echo $row->subdistrict_name ?>
                                    <?php echo $row->province_name ?>
                                    <?php echo $row->postal_code ?>
                                </div>
                            </div>
                            <br/>
                            <hr/>
                            <br />
                        <?php } ?>