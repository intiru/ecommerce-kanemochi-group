<table class="table table-striped">
    <tbody>
        <tr>
            <td>Picture</td>
            <td><img src="<?php echo $member->picture ?>" width="150"></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><?php echo $member->email ?></td>
        </tr>
        <tr>
            <td>First Name</td>
            <td><?php echo $member->first_name ?></td>
        </tr>
        <tr>
            <td>Last Name</td>
            <td><?php echo $member->last_name ?></td>
        </tr>
        <tr>
            <td>Jenis Kelamin</td>
            <td><?php echo $member->gender ?></td>
        </tr>
        <tr>
            <td>HD</td>
            <td><?php echo $member->hd ?></td>
        </tr>
        <tr>
            <td>ID Google</td>
            <td><?php echo $member->id_google ?></td>
        </tr>
        <tr>
            <td>Link</td>
            <td><?php echo $member->link ?></td>
        </tr>
        <tr>
            <td>Locale</td>
            <td><?php echo $member->locale ?></td>
        </tr>
        <tr>
            <td>Name</td>
            <td><?php echo $member->name ?></td>
        </tr>
        <tr>
            <td>Verified Email</td>
            <td><?php echo $member->verified_email ?></td>
        </tr>
        <tr>
            <td>Phone</td>
            <td><?php echo $member->phone ?></td>
        </tr>
        <tr>
            <td>Address</td>
            <td><?php echo $member->address ?></td>
        </tr>
        <tr>
            <td>Birthday</td>
            <td><?php echo $member->birthday ?></td>
        </tr>
        <tr>
            <td>Signup Type</td>
            <td><?php echo $member->signup_type ?></td>
        </tr>
        <tr>
            <td>Created At</td>
            <td><?php echo $member->created_at ?></td>
        </tr>
        <tr>
            <td>Updated At</td>
            <td><?php echo $member->updated_at ?></td>
        </tr>
        <tr>
            <td>Last Login</td>
            <td><?php echo $member->last_login ?></td>
        </tr>
    </tbody>
</table>