<table class="table table-striped- table-bordered table-hover table-checkable datatable">
            <thead>
            <tr>
                <th class="d-none"></th>
                <th width="20">No</th>
                <th>Category</th>
                <th>Product Title</th>
                <!--				<th>Thumbnail</th>-->
                <th>Available</th>
                <th>Best Seller</th>
                <th>New Status</th>
                <th>Product Price</th>
                <th width="50">Option</th>
            </tr>
            </thead>
            <?php $no = 1 ?>
            <tbody>
            <?php foreach ($wishlist as $datas) :
                    $link = $this->main->permalink(array('produk', $datas->category_title, $datas->title))
                     ?>
                <tr>
                    <td class="d-none data-row">
                        <textarea><?php echo json_encode($datas) ?></textarea>
                    </td>
                    <td><?php echo $no ?></td>
                    <td><?php echo $datas->category_title ?></td>
                    <td><?php echo $datas->title ?></td>
                    <!--					<td>-->
                    <!--						<img src="-->
                    <?php //echo $this->main->image_preview_url($datas->thumbnail) ?><!--" alt="Icon" width="-->
                    <?php //echo $this->main->image_size_preview() ?><!--">-->
                    <!--					</td>-->
                    <td><?php echo $datas->use ?></td>
                    <td><?php echo $datas->best_seller ?></td>
                    <td><?php echo $datas->new_status ?></td>
                    <td>
                        <?php
                        if ($datas->price_old) {
                            echo '<span style="text-decoration: line-through">' . $this->main->currency($datas->price_old) . '</span><br />';
                        }
                        echo $this->main->currency($datas->price);
                        ?>
                    </td>
                    <td>
                        <a href="<?php echo $link ?>"
                            target="_blank"
                           class="btn btn-primary btn-elevate btn-elevate-air">Detail</a>
                    </td>
                </tr>
                <?php $no++ ?>
            <?php endforeach; ?>
            </tbody>
        </table>