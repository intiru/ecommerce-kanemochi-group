<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="flaticon-avatar"></i>
			</span>
            <h3 class="kt-portlet__head-title">
                Data Pemesanan Member
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-sm table-striped">
                        <tbody>
                        <tr>
                            <td><strong>No Invoice</strong></td>
                            <td><?php echo $member_cart->invoice ?></td>
                        </tr>
                        <tr>
                            <td><strong>Nama Member</strong></td>
                            <td><?php echo $member_cart->first_name.' '.$member_cart->last_name ?></td>
                        </tr>
                        <tr>
                            <td><strong>Tanggal Pembelian</strong></td>
                            <td><?php echo $this->main->date_view($member_cart->cart_date) ?></td>
                        </tr>
                        <tr>
                            <td><strong>Nama Kurir</strong></td>
                            <td><?php echo $member_cart->courier_code_name ?></td>
                        </tr>
                        <tr>
                            <td><strong>Nama Paket Pengiriman</strong></td>
                            <td><?php echo $member_cart->courier_service_code_name ?></td>
                        </tr>
                        <tr>
                            <td><strong>Harga Pengiriman</strong></td>
                            <td><?php echo $this->main->currency($member_cart->courier_service_price) ?></td>
                        </tr>
                        <tr>
                            <td><strong>Estimasi Waktu Pengiriman</strong></td>
                            <td><?php echo $member_cart->courier_service_day_estimate ?> hari</td>
                        </tr>
                        <tr>
                            <td><strong>Alamat Tujuan Pengiriman</strong></td>
                            <td>
                                <?php echo $member_address->owner_name ?><br/>
                                <?php echo $member_address->phone ?><br/>
                                <?php echo $member_address->address ?>
                                <?php echo $member_address->city_name ?>
                                - <?php echo $member_address->subdistrict_name ?>
                                <?php echo $member_address->province_name ?>
                                <?php echo $member_address->postal_code ?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Status Pemesanan</strong></td>
                            <td><?php echo $this->main->member_cart_status($member_cart->status) ?></td>
                        </tr>
                        <?php if($member_cart->resi) { ?>
                            <tr>
                                <td><strong>Lacak Pemesanan</strong></td>
                                <td>
                                    <button type="button"
                                            class="btn btn-primary btn-elevate btn-elevate-air btn-order-tracking"
                                            data-id-member-cart="<?php echo $member_cart->id_member_cart ?>"
                                            data-resi="<?php echo $member_cart->resi ?>"
                                            data-courier-code="<?php echo $member_cart->courier_code ?>"
                                            data-action="<?php echo base_url('proweb/member_data_order/tracking/'.$member_cart->id_member_cart) ?>">
                                        <i class="ti-location-pin"></i> Lacak Sekarang
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <br/>

        <table class="table table-striped cart-list">
            <thead>
            <tr>
                <th>
                    Gambar Produk
                </th>
                <th>
                    Nama Produk
                </th>
                <th>
                    Harga
                </th>
                <th>
                    Qty
                </th>
                <th>
                    Subtotal
                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($member_cart_detail as $row) {
                $product_option = [];
                $id_product_option_json = $row->id_product_option_json;
                $id_product_option_arr = json_decode($id_product_option_json, TRUE);
                foreach($id_product_option_arr as $id_product_option => $id_product_option_value) {
                    $product_option[$id_product_option]['option'] = $this->db->where('id', $id_product_option)->get('product_option')->row()->title;
                    $product_option[$id_product_option]['value'] = $this->db->where('id', $id_product_option_value)->get('product_option_value')->row()->title;
                }
                $total_qty += $row->qty;
                $link_product = $this->main->permalink(array('produk', $row->product_category, $row->product_title)); ?>
                <tr>
                    <td>
                        <a href="<?php echo $link_product ?>"
                           target="_blank">
                            <div class="thumb_cart">
                                <img src="<?php echo $this->main->image_preview_url($row->product_thumbnail) ?>"
                                     data-src="<?php echo $this->main->image_preview_url($row->product_thumbnail) ?>"
                                     class="lazy" alt="Image" width="120">
                            </div>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo $link_product ?>"
                           target="_blank">
                            <?php echo $row->product_title ?><br />
                            <hr />
                            <?php foreach($product_option as $row_option) { ?>
                                <span class="item_cart_option"><?php echo $row_option['option'].' : '.$row_option['value'] ?></span><br />
                            <?php } ?>
                        </a>
                    </td>
                    <td class="font-weight-bolder"><?php echo $this->main->format_currency($row->product_price_cart) ?></td>
                    <td class="font-weight-bolder"><?php echo $row->qty ?></td>
                    <td class="cart-list-item-subtotal font-weight-bolder">
                        <?php echo $this->main->format_currency($row->product_price_cart * $row->qty) ?>
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="4" align="right">SUB TOTAL</td>
                <td class="cart-price-total font-weight-bolder"><?php echo $this->main->format_currency($member_cart->cart_total) ?></td>
            </tr>
            <tr>
                <td colspan="4" align="right">Ongkos Kirim</td>
                <td class="cart-price-total font-weight-bolder"><?php echo $this->main->format_currency($member_cart->courier_service_price) ?></td>
            </tr>
            <tr>
                <td colspan="4" align="right" class="font-weight-bolder">TOTAL</td>
                <td class="cart-price-total font-weight-bolder"><?php echo $this->main->format_currency($member_cart->cart_total + $member_cart->courier_service_price) ?></td>
            </tr>
            </tbody>
        </table>

    </div>
</div>

<div class="modal fade" id="modal-order-tracking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row" style="width: 100%">
                    <div class="col-10">
                        <h4 class="modal-title" id="myModalLabel"><i class="ti-location-pin"></i>  Order Tracking</h4>
                    </div>
                    <div class="col-2">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                </div>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>


