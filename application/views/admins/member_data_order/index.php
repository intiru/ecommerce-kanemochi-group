<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="flaticon-avatar"></i>
			</span>
            <h3 class="kt-portlet__head-title">
                Data Pemesanan Member
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <table class="table table-striped- table-bordered table-hover table-checkable datatable">
            <thead>
            <tr>
                <th class="d-none"></th>
                <th width="20">No</th>
                <th>No Invoice</th>
                <th>Member</th>
                <th>Tanggal</th>
                <th>Total Barang</th>
                <th>Total Harga</th>
                <th>Status</th>
                <th width="130">Option</th>
            </tr>
            </thead>
            <?php $no = 1 ?>
            <tbody>
            <?php foreach ($member_cart as $key => $row) {
                $total_barang = $this->db->select_sum('qty')->where('id_member_cart', $row->id_member_cart)->get('member_cart_detail')->row()->qty; ?>
                <tr>
                    <td><?php echo $key + 1 ?>.</td>
                    <td>
                        <?php echo $row->invoice ?>
                    </td>
                    <td>
                        <?php echo $row->first_name.' '.$row->last_name ?>
                    </td>
                    <td>
                        <?php echo $row->cart_date ?>
                    </td>
                    <td>
                        <?php echo $total_barang ?>
                    </td>
                    <td>
                        <strong><?php echo $this->main->currency($row->cart_total + $row->courier_service_price) ?></strong>
                    </td>
                    <td>
                        <strong><?php echo $this->main->member_cart_status($row->status) ?></strong>
                    </td>
                    <td>
                        <div class="btn-group-vertical">
                            <a href="<?php echo base_url('proweb/member_data_order/detail/' . $row->id_member_cart) ?>"
                               class="btn btn-success">Detail</a>
                            <?php if (in_array($row->status, array('pending', 'payment_accepted', 'error', 'reject', 'packed', 'send'))) ?>
                            <button class="btn btn-primary btn-member-cart-status-change"
                                    data-id-member-cart="<?php echo $row->id_member_cart ?>"
                                    data-status="<?php echo $row->status ?>"
                                    data-resi="<?php echo $row->resi ?>"
                                    data-action="<?php echo base_url('proweb/member_data_order/status_change/' . $row->id_member_cart); ?>">
                                Rubah Status
                            </button>
                        </div>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<form action="<?php echo base_url() ?>proweb/member_data_order/status_change" method="post">
    <input type="hidden" name="id_member_cart">
    <div class="modal" id="modal-member-cart-status" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ganti Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Status</label>
                        <select name="member_cart_status" class="form-control" required>
                            <option value="">Pilih Status</option>
                            <option value="payment_accepted">Payment Accepted</option>
                            <option value="reject">Reject</option>
                            <option value="packed">Packed</option>
                            <option value="send">Send</option>
                            <option value="success">Success</option>
                            <option value="rated">Rated</option>
                        </select>
                    </div>
                    <div class="form-group form-resi hide">
                        <label>No Resi</label>
                        <input type="text" name="resi" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>
</form>