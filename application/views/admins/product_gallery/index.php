<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="flaticon2-image-file"></i>
			</span>
            <h3 class="kt-portlet__head-title">
                Management Image Slider
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a href="<?php echo site_url('proweb/product') ?>" class="btn btn-warning btn-elevate btn-icon-sm">
                        <i class="la la-share"></i>
                        Kembali
                    </a>
                    <a href="#" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal"
                       data-target="#modal-create">
                        <i class="la la-plus"></i>
                        Add Slider
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <!--begin: Datatable -->

        <div class="alert alert-success" role="alert">
            <div class="alert-icon"><i class="flaticon-warning"></i></div>
            <div class="alert-text">
                <h3>Perhatian</h3>
                <p>Untuk dapat merubah urutan posisi galeri foto, dengan cara, drag and drop setiap baris galeri sesuai
                    dengan urutan yang diperlukan</p>
            </div>
        </div>

        <table class="table table-striped- table-bordered table-hover table-checkable datatable table-sortable">
            <thead>
            <tr>
                <th width="20">No</th>
                <th>Title</th>
                <th>Image</th>
                <th>View Status</th>
                <th>View as Thumbnail</th>
                <th width="130">Option</th>
                <th class="d-none"></th>
            </tr>
            </thead>
            <?php $no = 1 ?>
            <tbody>
            <?php foreach ($gallery as $datas) : ?>
                <tr data-id-product-gallery="<?php echo $datas->id ?>" style="cursor: move">
                    <td><?php echo $no ?></td>
                    <td><?php echo $datas->title ?></td>
                    <td>
                        <img src="<?php echo $this->main->image_preview_url($datas->thumbnail) ?>" alt="Icon"
                             width="<?php echo $this->main->image_size_preview() ?>">
                    </td>
                    <td><?php echo $datas->use_view ?></td>
                    <td><?php echo $datas->use_thumbnail ?></td>
                    <td>
                        <a href="#"
                           class="btn btn-success btn-elevate btn-elevate-air btn-edit">Edit</a>
                        <a href="#"
                           data-action="<?php echo base_url() ?>proweb/product_gallery/delete/<?php echo $datas->id ?>"
                           class="btn btn-danger btn-elevate btn-elevate-air btn-delete">Delete</a>
                    </td>
                    <td class="d-none data-row">
                        <textarea><?php echo json_encode($datas) ?></textarea>
                    </td>
                </tr>
                <?php $no++ ?>
            <?php endforeach; ?>
            </tbody>
        </table>
        <!--end: Datatable -->
    </div>
</div>
<!--begin::Modal-->

<form method="post" action="<?php echo base_url() . 'proweb/product_gallery/createprocess/' . $id_product; ?>"
      enctype="multipart/form-data" class="form-send">
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Slider</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleSelect1">Title</label>
                        <input type="text" class="form-control" placeholder="Title" name="title">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Use View</label>
                        <select class="form-control" name="use_view">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Use as Thumbnail</label>
                        <select class="form-control" name="use_thumbnail">
                            <option value="yes">Yes</option>
                            <option value="no" selected>No</option>
                        </select>
                        <span class="form-text text-muted">Jika ini digunakan, maka status di foto lain akan disable</span>
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Thumbnail</label>
                        <br/>
                        <img src="" class="img-thumbnail" width="200">
                        <br/><br/>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input browse-preview-img" accept="image/*"
                                   name="thumbnail" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <span class="form-text text-muted"><?php echo $this->main->file_info() ?></span>
                    </div>
                    <div class="form-group">
                        <label>Thumbnail Alt</label>
                        <textarea class="form-control" id="exampleTextarea" rows="3" name="thumbnail_alt"></textarea>
                        <span class="form-text text-muted"><?php echo $this->main->help_thumbnail_alt() ?></span>
                    </div>
                    <div class="form-group">
                        <label>Youtube Video URL</label>
                        <textarea class="form-control" id="exampleTextarea" rows="3" name="youtube_url"></textarea>
                    </div>
                    <?php if ($product_option_count > 0) { ?>
                        <hr/>
                        <h5><i class="fa fa-info-circle"></i> Hubungkan Gambar dengan Opsi Produk</h5>
                        <div class="alert alert-success fade show" role="alert">
                            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                            <div class="alert-text">
                                <ul>
                                    <li>
                                        Pilihan Form dibawah digunakan disaat user memilih Opsi Produk, Galeri Foto
                                        Produk akan langsung berpindah foto sesuai dengan Opsi yang dipilih
                                    </li>
                                    <li>Untuk mengaktifkan Fitur ini, diharapkan memilih semua opsi dibawah</li>
                                </ul>
                            </div>
                        </div>
                        <?php foreach ($product_option as $key => $row) { ?>
                            <div class="form-group">
                                <label><?php echo ++$key . '. ' . $row->title ?></label>
                                <select class="form-control" name="id_product_option_value[]">
                                    <option value="">Pilih Opsi Produk</option>
                                    <?php foreach ($row->product_option_value as $row_2) { ?>
                                        <option value="<?php echo $row_2->id ?>"><?php echo $row_2->title ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <hr/>
                        <h5><i class="fa fa-info-circle"></i> Belum ada opsi produk</h5>
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            </div>
        </div>
    </div>
</form>

<form method="post" action="<?php echo base_url() . 'proweb/product_gallery/update/' . $id_product; ?>"
      enctype="multipart/form-data" class="form-send">
    <div class="modal" id="modal-edit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit gallery</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" name="id">
                        <label for="exampleSelect1">Title</label>
                        <input type="text" class="form-control" placeholder="Title" name="title">
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Use View</label>
                        <select class="form-control" name="use_view">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Use as Thumbnail</label>
                        <select class="form-control" name="use_thumbnail">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                        <span class="form-text text-muted">Jika ini digunakan, maka status di foto lain akan disable</span>
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1">Thumbnail</label>
                        <br/>
                        <img src="" class="img-thumbnail" width="200">
                        <br/><br/>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input browse-preview-img" accept="image/*"
                                   name="thumbnail" id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <span class="form-text text-muted"><?php echo $this->main->file_info() ?></span>
                    </div>
                    <div class="form-group">
                        <label>Thumbnail Alt</label>
                        <textarea class="form-control" id="exampleTextarea" rows="3" name="thumbnail_alt"></textarea>
                        <span class="form-text text-muted"><?php echo $this->main->help_thumbnail_alt() ?></span>
                    </div>
                    <div class="form-group">
                        <label>Youtube Video URL</label>
                        <textarea class="form-control" id="exampleTextarea" rows="3" name="youtube_url"></textarea>
                    </div>
                    <?php if ($product_option_count > 0) { ?>
                        <hr/>
                        <h5><i class="fa fa-info-circle"></i> Hubungkan Gambar dengan Opsi Produk</h5>
                        <div class="alert alert-success fade show" role="alert">
                            <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                            <div class="alert-text">
                                <ul>
                                    <li>
                                        Pilihan Form dibawah digunakan disaat user memilih Opsi Produk, Galeri Foto
                                        Produk akan langsung berpindah foto sesuai dengan Opsi yang dipilih
                                    </li>
                                    <li>Untuk mengaktifkan Fitur ini, diharapkan memilih semua opsi dibawah</li>
                                </ul>
                            </div>
                        </div>
                        <?php foreach ($product_option as $key => $row) { ?>
                            <div class="form-group">
                                <label><?php echo ++$key . '. ' . $row->title ?></label>
                                <select class="form-control" name="id_product_option_value[]">
                                    <option value="">Pilih Opsi Produk</option>
                                    <?php foreach ($row->product_option_value as $row_2) { ?>
                                        <option value="<?php echo $row_2->id ?>"><?php echo $row_2->title ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <hr/>
                        <h5><i class="fa fa-info-circle"></i> Belum ada opsi produk</h5>
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="submit" value="upload">
                </div>
            </div>
        </div>
    </div>
</form>

<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".table-sortable tbody").sortable({
            update: function (event, ui) {
                var id_product_gallery_order = [];
                $(".table-sortable tbody tr").each(function (i, self) {
                    var id_product_gallery = $(this).data('id-product-gallery');
                    id_product_gallery_order[id_product_gallery] = i;
                })

                $.ajax({
                    url: '<?php echo base_url('proweb/product_gallery/order_update') ?>',
                    type: 'post',
                    data: {
                        order_index: id_product_gallery_order
                    },
                    success: function () {

                    }
                });
            }
        });
    });
</script>


