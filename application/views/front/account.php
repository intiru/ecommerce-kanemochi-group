<main class="bg_gray">

    <div class="container margin_30">
        <div class="page_header">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                    <li>Dashboard</li>
                </ul>
            </div>
            <h1>Dashboard</h1>
        </div>

        <div class="row justify-content-center">

            <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                <?php echo $this->load->view('front/account_sidebar', ['side_menu_active' => $side_menu_active], TRUE); ?>
            </div>

            <div class="col-xl-9 col-lg-9 col-md-9 col-12">
                <div class="box_account">
                    <div class="form_container">
                        <div class="table-responsive">
                            <table class="table table-sm table-striped">
                                <tbody>
                                <tr>
                                    <td><strong>Transaksi Terakhir</strong></td>
                                    <td><?php echo $this->main->format_date($transaction_last->cart_date) ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Transaksi Pertama</strong></td>
                                    <td><?php echo $this->main->format_date($transaction_first->cart_date) ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Login Terakhir</strong></td>
                                    <td><?php echo $this->main->format_datetime_view($member->last_login) ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>