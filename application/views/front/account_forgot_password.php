<main class="bg_gray">
    <div class="container margin_30">
        <div class="page_header">

            <h1 align="center">Lupa Password</h1>
        </div>

        <div class="row">
            <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2 col-md-8 offset-md-2">
                <div class="box_account">
                    <form action="<?php echo site_url('forgot-password-send') ?>" method="post" class="form_container form-send"
                          data-alert-modal="true">
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Masukkan Email *">
                        </div>
                        <div class="text-center"><input type="submit" value="Kirim Permintaan Reset Password" class="btn_1 full-width"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
