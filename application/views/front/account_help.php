<main class="bg_gray">

    <div class="container margin_30">
        <div class="page_header">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                    <li>Halaman Bantuan</li>
                </ul>
            </div>
            <h1>Halaman Bantuan</h1>
        </div>

        <div class="row justify-content-center">

            <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                <?php echo $this->load->view('front/account_sidebar', ['side_menu_active' => $side_menu_active], TRUE); ?>
            </div>

            <div class="col-xl-9 col-lg-9 col-md-9 col-12">
                <div class="box_account">
                    <div class="form_container">
                        <h5><?php echo $page->title ?></h5>
                        <?php echo $page->title_sub ?>
                        <br />
                        <?php echo $page->description ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>