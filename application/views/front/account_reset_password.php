<main class="bg_gray">
    <div class="container margin_30">
        <div class="page_header">

            <h1 align="center">Reset Password</h1>
        </div>

        <div class="row">
            <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2 col-md-8 offset-md-2">
                <div class="box_account">
                    <form action="<?php echo site_url('reset-password-send') ?>" method="post" class="form_container form-send"
                          data-alert-modal="true">
                        <div class="form-group">
                            Email : <?php echo $member->email ?>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Masukkan Password *">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password_conf" placeholder="Ulangi Memasukkan Password *">
                        </div>
                        <div class="text-center"><input type="submit" value="Proses Reset Password" class="btn_1 full-width"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
