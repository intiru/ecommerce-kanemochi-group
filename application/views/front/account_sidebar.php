<div class="box_account member-account-side">
    <div class="form_container no-padding">
        <ul>
            <li<?php echo $side_menu_active == 'dashboard' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url('account') ?>">
                    <i class="ti-dashboard"></i> Dashboard
                </a>
            </li>
            <li<?php echo $side_menu_active == 'order' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url('account/order') ?>">
                    <i class="ti-package"></i> Daftar Pemesanan
                </a>
            </li>
            <li<?php echo $side_menu_active == 'profile' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url('account/profile') ?>">
                    <i class="ti-user"></i> Profil Saya
                </a>
            </li>
            <li<?php echo $side_menu_active == 'address' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url('account/address') ?>">
                    <i class="ti-home"></i> Daftar Alamat Pengiriman
                </a>
            </li>
            <li<?php echo $side_menu_active == 'help' ? ' class="active"':'' ?>>
                <a href="<?php echo site_url('account/help') ?>">
                    <i class="ti-help-alt"></i> Halaman Bantuan
                </a>
            </li>
            <li>
                <a href="<?php echo site_url('account/logout') ?>">
                    <i class="ti-zoom-out"></i> Logout
                </a>
            </li>
        </ul>
    </div>
</div>