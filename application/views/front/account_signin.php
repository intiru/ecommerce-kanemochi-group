<main class="bg_gray">
    <div class="container margin_30">
        <div class="page_header">

            <h1 align="center">Login Member</h1>
        </div>

        <div class="row">
            <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2 col-md-8 offset-md-2">
                <div class="box_account">
                    <form action="<?php echo site_url('login/kirim') ?>" method="post" class="form_container form-send"
                          data-alert-modal="false">
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="Email *">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password"
                                   placeholder="Password *">
                        </div>
                        <div class="form-group text-right">
                            <a href="<?php echo site_url('forgot-password') ?>">Anda Lupa Password ? Klik disini.</a>
                        </div>
                        <div class="text-center"><input type="submit" value="Log In" class="btn_1 full-width"></div>
                        <div id="forgot_pw">
                            <div class="form-group">
                                <input type="text" class="form-control" name="email_forgot" id="email_forgot"
                                       placeholder="Type your email">
                            </div>
                            <p>A new password will be sent shortly.</p>
                            <div class="text-center"><input type="submit" value="Reset Password" class="btn_1"></div>
                        </div>
                        <div class="hide">
                            <div class="divider"><span>atau</span></div>
                            <div class="row no-gutters">
                                <div class="col-lg-6 pr-lg-1">
                                    <a href="#" class="social_bt facebook">Login with Facebook</a>
                                </div>
                                <div class="col-lg-12 pl-lg-1">
                                    <a href="<?php echo $login_google ?>" class="social_bt google">Login with Google</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
