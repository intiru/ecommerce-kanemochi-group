<main class="bg_gray">

    <div class="container margin_30">
        <div class="page_header">
            <h1 align="center">Daftar Member Baru</h1>
        </div>

        <div class="row">
            <div class="col-xl-8 offset-xl-2 col-lg-8 offset-lg-2 col-md-8 offset-md-2">
                <div class="box_account">
                    <h3 class="new_client"></h3> <small class="float-right pt-2">* Perlu Diisi</small>
                    <form action="<?php echo site_url('daftar/kirim') ?>" method="post" class="form-horizontal form-send"
                          data-alert-modal="true">
                        <div class="form_container">
                            <h4 class="pb-3">Informasi Member</h4>
                            <div class="private box">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Nama Depan *</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="first_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Nama Belakang *</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="last_name">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Jenis Kelamin *</label>
                                    <div class="col-sm-9">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="gender" id="inlineRadio1"
                                                   value="male" checked>
                                            <label class="form-check-label" for="inlineRadio1">Laki - Laki</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="gender" id="inlineRadio2"
                                                   value="female">
                                            <label class="form-check-label" for="inlineRadio2">Perempuan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Lahir *</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control datepicker" name="birthday">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Nomer Telepon *</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="phone">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-3 control-label">Provinsi *</label>
                                    <div class="col-sm-9">
                                        <select class="form-control select2" name="id_province">
                                            <option value="">Pilih Provinsi</option>
                                            <?php foreach ($province as $row) { ?>
                                                <option value="<?php echo $row->id_province ?>"><?php echo $row->province_name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-3 control-label">Kota *</label>
                                    <div class="col-sm-9">
                                        <select class="form-control select2" name="id_city">
                                            <option value="">Pilih Kota</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-3 control-label">Kecamatan *</label>
                                    <div class="col-sm-9">
                                        <select class="form-control select2" name="id_subdistrict">
                                            <option value="">Pilih Kecamatan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Nama Jalan *</label>
                                    <div class="col-sm-9">
                                    <textarea class="form-control"
                                              name="address"></textarea>
                                    </div>
                                </div>
                                <br/>
                                <hr>
                                <br/>
                                <h4 class="pb-3">Data Akun</h4>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Email *</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Password *</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Konfirmasi Password *</label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" name="password_conf">
                                    </div>
                                </div>

                            </div>

                            <br/>
                            <br/>
                            <div class="text-center"><input type="submit" value="Register" class="btn_1 full-width">
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    </div>

</main>