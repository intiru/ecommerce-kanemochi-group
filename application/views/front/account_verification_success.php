<main class="bg_gray">

    <div class="bg_white">
        <div class="container margin_60_35 text-center">
            <i class="ti-check-box payment-success-icon"></i>
            <h1 class="pb-3 text-center">Verifikasi Member Berhasil</h1>
            <div class="row">
                <div class="col-lg-12 col-md-12 add_bottom_25">
                    <p>Terima Kasih sudah melakukan registrasi member pada Website Kanemochi Group</p>
                    <br />
                    <a href="<?php echo site_url('login') ?>" class="btn_1">Silahkan Melakukan Login</a>
                </div>
            </div>
        </div>
    </div>
</main>