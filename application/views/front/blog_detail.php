<main class="bg_gray">
    <div class="container margin_30">
        <div class="page_header">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                    <li><a href="<?php echo site_url('blog') ?>">Blog</a></li>
                    <li>
                        <a href="<?php echo $this->main->permalink(array('blog', $page->category_title)) ?>"><?php echo $page->category_title ?></a>
                    </li>
                    <li><?php echo $page->title ?></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <div class="singlepost">
                    <figure><img alt="<?php echo $page->thumbnail_alt ?>" class="img-fluid"
                                 src="<?php echo $this->main->image_preview_url($page->thumbnail) ?>"></figure>
                    <h1><?php echo $page->title ?></h1>
                    <div class="postmeta">
                        <ul>
                            <li><a href="<?php echo $this->main->permalink(array('blog', $page->category_title)) ?>"><i
                                            class="ti-folder"></i> <?php echo $page->category_title ?></a></li>
                            <li><i class="ti-calendar"></i> <?php echo $this->main->date_view($page->created_at) ?></li>
                            <li><i class="ti-user"></i> Admin</li>
                        </ul>
                    </div>

                    <div class="post-content">
                        <?php echo $page->description ?>
                    </div>
                </div>
            </div>

            <aside class="col-lg-3">
                <div class="widget search_blog">
                    <div class="form-group">
                        <input type="text" name="search" id="search" class="form-control" placeholder="Search..">
                        <button type="submit"><i class="ti-search"></i><span class="sr-only">Search</span></button>
                    </div>
                </div>

                <div class="widget">
                    <div class="widget-title">
                        <h4>Blog Terbaru</h4>
                    </div>
                    <ul class="comments-list">
                        <?php foreach ($latest_blog as $row) {
                            $link = $this->main->permalink(array('blog', $row->category_title, $row->title)); ?>
                            <li>
                                <div class="alignleft">
                                    <a href="<?php echo $link ?>"><img
                                                src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                                alt="<?php echo $row->thumbnail_alt ?>"
                                                title="<?php echo $row->title ?>"></a>
                                </div>
                                <small><?php echo $row->category_title ?>
                                    - <?php echo $this->main->date_view($row->created_at) ?></small>
                                <h3><a href="<?php echo $link ?>" title=""><?php echo $row->title ?></a></h3>
                            </li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="widget">
                    <div class="widget-title">
                        <h4>Kategori Blog</h4>
                    </div>
                    <ul class="cats">
                        <?php foreach ($category as $row) { ?>
                            <li>
                                <a href="<?php echo $this->main->permalink(array('blog', $row->title)); ?>"><?php echo $row->title ?>
                                    <span>(<?php echo $row->total ?>)</span></a></li>
                        <?php } ?>
                    </ul>
                </div>

            </aside>
        </div>
    </div>
</main>