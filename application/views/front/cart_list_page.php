<main class="bg_gray">
    <div class="container margin_30">
        <div class="page_header">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                    <li>Keranjang Belanja</li>
                </ul>
            </div>
            <h1>Keranjang Belanja</h1>
        </div>

        <form action="<?php echo site_url('cart/update') ?>" method="post">
            <table class="table table-bordered table-hover table-cart-list cart-list">
                <thead>
                <tr>
                    <th>

                    </th>
                    <th>
                        Produk
                    </th>
                    <th>
                        Harga
                    </th>
                    <th>
                        Qty
                    </th>
                    <th>
                        Subtotal
                    </th>
                    <th style="">

                    </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($cart_list as $row) {
                    $product_title = $this->main->set_special_char($row['name']);
                    $out_of_stock = $this->db->select('out_of_stock')->where('id', $row['id'])->get('product')->row()->out_of_stock;
                    if($out_of_stock == 'yes') {
                        $tr_class = 'row-product-out-of-stock';
                    } else {
                        $tr_class = '';
                    } ?>
                    <input type="hidden" name="id_product[<?php echo $row['rowid'] ?>]"
                           value="<?php echo $row['id'] ?>">
                    <tr
                            data-rowid="<?php echo $row['rowid'] ?>"
                            data-price="<?php echo $row['price'] ?>"
                            data-subtotal="<?php echo $row['subtotal'] ?>"
                            class="<?php echo $tr_class ?>">

                        <td>
                            <?php if($out_of_stock == 'no') { ?>
                                <input type="checkbox" name="check-item" data-rowid="<?php echo $row['rowid'] ?>" <?php echo $row['options']['checked_status'] == "yes" ? 'checked':'' ?>>
                            <?php } ?>
                        </td>
                        <td>
                            <a href="<?php echo $this->main->permalink(array('produk', $row['options']['category_title'], $product_title)) ?>"
                               target="_blank">
                                <div class="thumb_cart">
                                    <img src="<?php echo $row['options']['thumbnail'] ?>"
                                         data-src="<?php echo $row['options']['thumbnail'] ?>" class="lazy" alt="Image">
                                </div>
                                <span class="item_cart"><?php echo $product_title ?></span>
                                <?php foreach($row['options']['product_option'] as $row_option) { ?>
                                    <span class="item_cart_option"><?php echo $row_option['option'].' : '.$row_option['value'] ?></span>
                                <?php } ?>
                            </a>
                        </td>
                        <td class="font-weight-bolder"><?php echo $this->main->format_currency($row['price']) ?></td>
                        <td>
                            <?php if($out_of_stock == 'no') { ?>
                            <div class="numbers-row">
                                <input type="text" value="<?php echo $row['qty'] ?>" id="quantity_1"
                                       class="qty2 cart-list-qty-input" name="qty[<?php echo $row['rowid'] ?>]" min="1">
                                <div class="inc button_inc cart-list-qty-plus">+</div>
                                <div class="dec button_inc cart-list-qty-min">-</div>
                            </div>
                            <?php } else { ?>
                                <div class="text-center"><?php echo $row['qty']; ?></div>
                            <?php } ?>
                        </td>
                        <td class="cart-list-item-subtotal font-weight-bolder">
                            <?php echo $this->main->format_currency($row['subtotal']) ?>
                        </td>
                        <td class="options">
                            <a href="javascript:" class="cart-item-remove-on-page"
                               data-rowid="<?php echo $row['rowid'] ?>"
                               data-id="<?php echo $row['id'] ?>">
                                <i class="ti-trash"></i>
                            </a>

                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td colspan="3" align="center" class="font-weight-bolder">TOTAL</td>
                    <td align="center" class="cart-item-total font-weight-bolder"><?php echo $cart_total_item; ?></td>
                    <td class="cart-price-total font-weight-bolder"><?php echo $this->main->format_currency($cart_total_price) ?></td>
                    <td></td>
                </tr>
            </table>

            <div class="row add_top_30 cart_actions">
                <div class="col-sm-5">
                    <a href="<?php echo site_url('produk') ?>" class="btn_1 outline"><i class="ti-shopping-cart"></i>
                        Lanjut Belanja</a>
                </div>
                <div class="col-sm-7 text-right">
                    <button type="submit" class="btn_1 gray hide"><i class="ti-pencil"></i> Perbarui Keranjang Belanja
                    </button>
                    <button type="button"
                            class="btn_1 cart-checkout <?php echo !$product_checked_status ? 'btn-disabled' : '' ?>"
                        <?php echo !$product_checked_status ? 'disabled' : '' ?>>
                        <i class="ti-check"></i> Checkout
                    </button>
                </div>
            </div>
        </form>

    </div>
</main>