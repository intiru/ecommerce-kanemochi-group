<main class="bg_gray">

    <div class="container margin_60">
        <div class="main_title">
            <h2><?php echo $page->title ?></h2>
            <p><?php echo $page->title_sub ?></p>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="box_contacts">
                    <i class="ti-support"></i>
                    <h2>Kontak Bantuan</h2>
                    <a href="<?php echo $whatsapp_link ?>" target="_blank">Phone/WA : <?php echo $whatsapp ?></a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="box_contacts">
                    <i class="ti-map-alt"></i>
                    <h2>Alamat Kantor</h2>
                    <a href="<?php echo $address_link ?>"><?php echo $address ?></a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="box_contacts">
                    <i class="ti-package"></i>
                    <h2>Alamat Email</h2>
                    <a href="<?php echo $email_link ?>"><?php echo $email ?></a>
                </div>
            </div>
        </div>
    </div>

    <div class="bg_white">
        <div class="container margin_60_35">
            <h4 class="pb-3">Form Kontak Kami</h4>
            <div class="row">
                <div class="col-lg-4 col-md-6 add_bottom_25">
                    <form action="<?php echo site_url('kontak/send') ?>" method="post" class="form-send">
                        <div class="form-group">
                            <input class="form-control" type="text" name="name" placeholder="Nama">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="email" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" name="phone" placeholder="Telepon">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="4" name="message" placeholder="Pesan"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="help-block">
                                <strong>Kode Keamanan</strong><br/>
                                Untuk terhindar dari spam, ketik kode dibawah ini.
                            </div>
                            <?php echo $captcha ?>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" name="captcha" placeholder="Kode Keamanan">
                        </div>
                        <div class="form-group">
                            <input class="btn_1 full-width" type="submit" value="Submit">
                        </div>
                    </form>
                </div>
                <div class="col-lg-8 col-md-6 add_bottom_25">
                    <iframe class="map_contact"
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.444064689006!2d107.27462407688802!3d-6.33648039365321!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e699df7e4dde819%3A0x7a1169d2f5494bf!2sKanemochi%20Omiyage%20Souvenir%20Jepang!5e0!3m2!1sen!2sid!4v1690424282808!5m2!1sen!2sid"
                            style="border: 0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</main>