<main>
    <div id="carousel-home">
        <div class="owl-carousel owl-theme">
            <?php foreach ($slider as $row) {
                ?>
                <div class="owl-slide cover"
                     style="background-image: url(<?php echo $this->main->image_preview_url($row->thumbnail) ?>);">
                    <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.5)">
                        <div class="container">
                            <div class="row justify-content-center justify-content-md-start">
                                <div class="col-lg-12 static">
                                    <div class="slide-text text-<?php echo $row->align ?> white">
                                        <h2 class="owl-slide-animated owl-slide-title"><?php echo $row->title ?>
                                        </h2>
                                        <p class="owl-slide-animated owl-slide-subtitle">
                                            <?php echo $row->description ?>
                                        </p>
                                        <div class="owl-slide-animated owl-slide-cta">
                                            <a class="btn_1"
                                               href="<?php echo $row->url ?>"
                                               role="button">Lihat Daftar Produk</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div id="icon_drag_mobile"></div>
    </div>

    <div class="container margin_60_35">

        <div class="main_title">
            <h2>Kategori Produk Kami</h2>
            <span>Best Produk</span>
            <p>Berikut adalah daftar Kategori Produk terbaik dari Kami:</p>
        </div>

        <ul id="banners_grid" class="clearfix">
            <?php foreach ($category_menu as $row) { ?>
                <li>
                    <a href="<?php echo $this->main->permalink(array('produk', $row->title, '?category=' . $this->main->str_encrypt($row->id))) ?>"
                       class="img_container">
                        <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                             data-src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                             alt="<?php echo $row->thumbnail_alt ?>" class="lazy">
                        <div class="short_info opacity-mask" data-opacity-mask="rgba(0, 0, 0, 0.5)">
                            <h3><?php echo $row->title ?></h3>
                            <div><span class="btn_1">Lihat Produk</span></div>
                        </div>
                    </a>
                </li>
            <?php } ?>
        </ul>

        <div class="text-center margin_30_5">
            <a class="btn_1" href="<?php echo $this->main->permalink(array('produk')) ?>" role="button"><i
                        class="ti-shopping-cart"></i> Lihat Semua Produk</a>
        </div>
    </div>

    <div class="container margin_60_35">
        <div class="main_title">
            <h2>Produk Populer</h2>
            <span>Sepatu</span>
            <p>Berikut adalah daftar Produk Populer berdasarkan kalkulasi pembeli:</p>
        </div>
        <div class="row small-gutters">
            <?php foreach ($product_list as $row) {
                $product_item_view = $this->main->product_item_view($row); ?>
                <div class="col-12 col-md-4 col-xl-3">
                    <?php echo $product_item_view ?>
                </div>
            <?php } ?>
        </div>

        <div class="text-center margin_30">
            <a class="btn_1" href="<?php echo $this->main->permalink(array('produk')) ?>" role="button"><i
                        class="ti-shopping-cart"></i> Lihat Semua Produk</a>
        </div>
    </div>

    <div class="featured lazy"
         data-bg="url(<?php echo $this->main->image_preview_url($home_product_row->pages_thumbnail) ?>)">
        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.5)">
            <div class="container margin_60">
                <div class="row justify-content-center justify-content-md-start">
                    <div class="col-lg-6 wow" data-wow-offset="150">
                        <h3><?php echo $home_product_row->title ?></h3>
                        <p><?php echo $home_product_row->title_sub ?></p>
                        <div class="feat_text_block">
                            <div class="price_box">
                                <span class="new_price"><?php echo $this->main->format_currency($home_product_row->price) ?></span>
                                <?php if ($home_product_row->price_old) { ?>
                                    <span class="old_price"><?php echo $this->main->format_currency($home_product_row->price_old) ?></span>
                                <?php } ?>
                            </div>
                            <a class="btn_1"
                               href="<?php echo $this->main->permalink(array($home_product_row->category_title, $home_product_row->title)) ?>"
                               role="button">Shop Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (count($blog_home) > 0) { ?>
        <div class="container margin_60_35">
            <div class="main_title">
                <h2>Blog Terakhir</h2>
                <span>Blog</span>
                <p>Berikut daftar Blog Terakhir yang Diterbitkan:</p>
            </div>
            <div class="row">
                <?php foreach ($blog_home as $row) { ?>
                    <div class="col-12 col-lg-6">
                        <a class="box_news"
                           href="<?php echo $this->main->permalink(array('blog', $row->category_title, $row->title)) ?>">
                            <figure>
                                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                     data-src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" alt=""
                                     width="400"
                                     height="266" class="lazy">
                                <figcaption>
                                    <strong><?php echo date('d', strtotime($row->created_at)) ?></strong><?php echo date('M', strtotime($row->created_at)) ?>
                                </figcaption>
                            </figure>
                            <ul>
                                <li><i class="ti-comment-alt"></i> <?php echo $row->category_title ?></li>
                                <li><?php echo $this->main->date_view($row->created_at); ?></li>
                            </ul>
                            <h4><?php echo $row->title ?></h4>
                            <p align="justify"><?php echo $this->main->short_desc($row->description) ?></p>
                        </a>
                    </div>
                <?php } ?>
            </div>

            <div class="text-center margin_30">
                <a class="btn_1" href="<?php echo $this->main->permalink(array('blog')) ?>" role="button"><i
                            class="ti-folder"></i> Lihat Semua Blog</a>
            </div>
        </div>
    <?php } ?>
</main>