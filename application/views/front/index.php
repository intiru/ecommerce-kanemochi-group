<!doctype html>
<html lang="<?php echo $lang_active->code ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $page->meta_title ?></title>
    <meta name="keywords" content="<?php echo $page->meta_keywords ?>"/>
    <meta name="description" content="<?php echo $page->meta_description ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="revisit-after" content="2 days"/>
    <meta name="robots" content="index, follow"/>
    <meta name="rating" content="General"/>
    <meta http-equiv="charset" content="ISO-8859-1"/>
    <meta http-equiv="content-language" content="<?php echo $lang_active->code ?>"/>
    <meta name="MSSmartTagsPreventParsing" content="true"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo base_url() ?>assets/template_front/img/favicon-japan-omiyage.png" rel="shortcut icon" type="image/x-icon"/>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/template_front/css/bootstrap.custom.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/style.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/template_front/css/home_1.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/template_front/plugin/bootstrap-datepicker/css/bootstrap-datepicker.min.css"
          rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/listing.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/error_track.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/product_page.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/blog.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/contact.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/plugin/lightbox/css/lightbox.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/plugin/select2/css/select2.min.css" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/template_front/css/account.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/cart.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/faq.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/checkout.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/template_front/css/loading.css" rel="stylesheet">

    <script src="<?php echo base_url() ?>assets/template_front/js/common_scripts.js"></script>
</head>

<body
        data-site-url="<?php echo site_url() ?>"
        data-current-url="<?php echo current_url() ?>"
        data-current-url-enc="<?php echo $this->main->str_encrypt(current_url()) ?>"
        data-route-cart-add="<?php echo site_url('cart/add') ?>"
        data-route-cart-remove="<?php echo site_url('cart/remove') ?>"
        data-route-cart-checkout-process="<?php echo site_url('cart/checkout/process') ?>"
        data-route-cart-checkout-page="<?php echo site_url('cart/checkout') ?>"
        data-route-cart-update-item-qty="<?php echo site_url('cart/update_item_qty') ?>"
        data-route-cart-checked-update="<?php echo site_url('cart/update_item_checked') ?>"
        data-route-login="<?php echo site_url('login') ?>"
        data-route-product="<?php echo site_url('produk') ?>"
        data-route-wishlist-process="<?php echo site_url('wishlist/process') ?>"
        data-route-city-get="<?php echo site_url('account/city_get') ?>"
        data-route-subdistrict-get="<?php echo site_url('account/subdistrict_get') ?>"
        data-route-subdistrict-postal-code-get="<?php echo site_url('account/subdistrict_postal_code_get') ?>"
        data-route-member-address-primary="<?php echo site_url('account/member_address_primary') ?>"
        data-route-member-address-delete="<?php echo site_url('account/member_address_delete') ?>"
        data-route-member-address-edit="<?php echo site_url('account/member_address_edit') ?>"
        data-back-status="<?php echo $login_status ?>"
        data-cart-price-total="<?php echo $cart_price_total ?>"
        data-currency="<?php echo $currency ?>"
        data-route-checkout-success="<?php echo site_url('cart/checkout_success') ?>"
        data-route-checkout-pending="<?php echo site_url('cart/checkout_pending') ?>"
        data-route-checkout-error="<?php echo site_url('cart/checkout_error') ?>"
        data-route-payment-success="<?php echo site_url('payment/success') ?>"
        data-route-payment-pending="<?php echo site_url('payment/pending') ?>"
        data-route-payment-error="<?php echo site_url('payment/error') ?>"
        data-route-courier-change-price="<?php echo site_url('cart/courier_change_price') ?>"
        data-route-member-cart-order-tracking="<?php echo site_url('account/order_tracking') ?>"
        data-route-member-cart-order-accepted="<?php echo site_url('account/order_accepted') ?>">

<div id="page">
    <header class="version_1">
        <div class="layer" id="layer-all"></div>
        <div class="main_header">
            <div class="container">
                <div class="row small-gutters">
                    <div class="col-xl-3 col-lg-3 d-lg-flex align-items-center">
                        <div id="logo">
                            <a href="<?php echo site_url() ?>">
                                <img src="<?php echo base_url() ?>assets/template_front/img/logo-japan-omiyage.png"
                                     alt="" width="200">
                            </a>

                        </div>
                    </div>
                    <nav class="col-xl-9 col-lg-9">
                        <a class="open_close d-none" href="javascript:void(0);">
                            <div class="hamburger hamburger--spin">
                                <div class="hamburger-box">
                                    <div class="hamburger-inner"></div>
                                </div>
                            </div>
                        </a>

                        <div class="main-menu">
                            <div id="header_menu">
                                <a href="<?php echo site_url() ?>">
                                    <img src="<?php echo base_url() ?>assets/template_front/img/logo-japan-omiyage.png"
                                         alt="" height="25" width="200">
                                </a>
                                <a href="javascript:;" class="open_close" id="close_in"><i class="ti-close"></i></a>
                            </div>
                            <ul>
                                <li>
                                    <a href="<?php echo site_url() ?>" <?php echo $page->type == 'home' ? 'class="active"' : '' ?>>Beranda</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('produk') ?>" <?php echo $page->type == 'category' ? 'class="active"' : '' ?>>Daftar
                                        Produk</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('cara-belanja') ?>" <?php echo $page->type == 'how_shop' ? 'class="active"' : '' ?>>Cara
                                        Belanja</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('galeri-foto') ?>" <?php echo $page->type == 'gallery_photo' ? 'class="active"' : '' ?>>Galeri
                                        Foto</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('blog') ?>" <?php echo $page->type == 'blog' ? 'class="active"' : '' ?>>Blog</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('tentang-kami') ?>" <?php echo $page->type == 'about_us' ? 'class="active"' : '' ?>>Tentang
                                        Kami</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('kontak-kami') ?>" <?php echo $page->type == 'contact_us' ? 'class="active"' : '' ?>>Kontak
                                        Kami</a>
                                </li>
                                <li class="d-xl-none">
                                    <a href="<?php echo site_url('daftar?p=' . $url_uniqid) ?>">
                                        Daftar</a>
                                </li>
                                <li class="d-xl-none">
                                    <a href="<?php echo site_url('login?p=' . $url_uniqid) ?>">
                                        Login</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>

        <div class="main_nav Sticky header-shadow">
            <div class="container">
                <div class="row small-gutters">
                    <div class="col-xl-3 col-lg-3 col-md-3">
                        <nav class="categories">
                            <ul class="clearfix">
                                <li><span>
										<a href="#">
											<span class="hamburger hamburger--spin">
												<span class="hamburger-box">
													<span class="hamburger-inner"></span>
												</span>
											</span>
											Kategori Produk
										</a>
									</span>
                                    <div id="menu">
                                        <ul>
                                            <?php foreach ($category_menu as $row_0) { ?>
                                                <li>
                                                    <span><a href="<?php echo $this->main->permalink(array('produk', $row_0->title)) ?>"><?php echo ucwords(strtolower($row_0->title)) ?></a></span>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="col-xl-6 col-lg-7 col-md-5 d-none d-md-block">
                        <div class="custom-search-input">
                            <form action="<?php echo site_url('produk') ?>" method="get">
                                <input type="text" placeholder="Ketik pencarian disini ..." name="keyword" value="<?php echo $keyword ?>">
                                <button type="submit"><i class="header-icon_search_custom"></i></button>
                            </form>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-2 col-md-4">
                        <ul class="top_tools">
                            <li class="d-xl-none">
                                <a class="open_close" href="javascript:void(0);">
                                    <div class="hamburger hamburger--spin">
                                        <div class="hamburger-box">
                                            <div class="hamburger-inner mobile-menu"></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="cart-list-review">
                                <?php echo $cart_list_review ?>
                            </li>
                            <?php if ($login_status) { ?>
                                <li>
                                    <a href="<?php echo site_url('wishlist') ?>" class="wishlist"><span>Wishlist</span></a>
                                </li>
                                <li>
                                    <div class="dropdown dropdown-access">
                                        <a href="<?php echo site_url('account') ?>"
                                           class="access_link"><span>Account</span></a>
                                        <div class="dropdown-menu">
                                            <ul>
                                                <li>
                                                    <a href="<?php echo site_url('account') ?>"><i
                                                                class="ti-dashboard"></i>Dashboard</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo site_url('account/order') ?>"><i
                                                                class="ti-package"></i>Daftar Pemesanan</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo site_url('account/profile') ?>"><i
                                                                class="ti-user"></i>Profil
                                                        Saya</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo site_url('account/address') ?>">
                                                        <i class="ti-home"></i>Daftar Alamat Pengiriman
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo site_url('account/help') ?>"><i
                                                                class="ti-help-alt"></i>Halaman Bantuan</a>
                                                </li>
                                                <a href="<?php echo site_url('account/logout?p='.$url_uniqid); ?>"
                                                   class="btn_1 btn-logout">
                                                    Logout
                                                </a>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            <?php } ?>
                            <li class="d-block d-sm-none">
                                <a href="javascript:void(0);" class="btn_search_mob"><span>Search</span></a>
                            </li>
                            <?php if (!$login_status) { ?>
                                <li>
                                    <a href="<?php echo site_url('login?p=' . $url_uniqid) ?>">Login</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('daftar?p=' . $url_uniqid) ?>">Daftar</a>
                                </li>
                            <?php } ?>
                            <li class="d-block d-sm-none">
                                <a href="#menu" class="btn_cat_mob">
                                    <div class="hamburger hamburger--spin" id="hamburger">
                                        <div class="hamburger-box">
                                            <div class="hamburger-inner"></div>
                                        </div>
                                    </div>
                                    Kategori
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="search_mob_wp">
                <input type="text" class="form-control" placeholder="Search over 10.000 products">
                <input type="submit" class="btn_1 full-width" value="Search">
            </div>
        </div>
    </header>

    <?php echo $content ?>

    <footer class="revealed">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <h3 data-target="#collapse_1">Quick Links</h3>
                    <div class="collapse dont-collapse-sm links" id="collapse_1">
                        <ul>
                            <li><a href="<?php echo site_url('cara-belanja') ?>">Cara Belanja</a></li>
                            <li><a href="<?php echo site_url('galeri-foto') ?>">Galeri Foto</a></li>
                            <li><a href="<?php echo site_url('blog') ?>">Blog</a></li>
                            <li><a href="<?php echo site_url('tentang-kami') ?>">Tentang Kami</a></li>
                            <li><a href="<?php echo site_url('frequently-ask-question') ?>">FAQ</a></li>
                            <li><a href="<?php echo site_url('kontak-kami') ?>">Kontak Kami</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h3 data-target="#collapse_2">Kategori Produk</h3>
                    <div class="collapse dont-collapse-sm links" id="collapse_2">
                        <ul>
                            <?php foreach ($category_menu as $row) { ?>
                                <li>
                                    <a href="<?php echo $this->main->permalink(array('produk', $row->title)) ?>"><?php echo $row->title ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h3 data-target="#collapse_3">Kontak Kami</h3>
                    <div class="collapse dont-collapse-sm contacts" id="collapse_3">
                        <ul>
                            <li><i class="ti-home"></i><?php echo $address ?></li>
                            <li><i class="ti-headphone-alt"></i><a href="<?php echo $whatsapp_link ?>"
                                                                   target="_blank"><?php echo $whatsapp ?></a></li>
                            <li><i class="ti-email"></i><a href="<?php echo $email_link ?>"><?php echo $email ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h3 data-target="#collapse_4">Sosial Media Kita</h3>
                    <div class="collapse dont-collapse-sm" id="collapse_4">
                        <div class="follow_us">
                            <ul>
                                <li>
                                    <a href="#0">
                                        <img
                                                src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                data-src="<?php echo base_url() ?>assets/template_front/img/twitter_icon.svg"
                                                alt="" class="lazy">
                                    </a>
                                </li>
                                <li>
                                    <a href="#0">
                                        <img
                                                src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                data-src="<?php echo base_url() ?>assets/template_front/img/facebook_icon.svg"
                                                alt="" class="lazy">
                                    </a>
                                </li>
                                <li>
                                    <a href="#0">
                                        <img
                                                src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                data-src="<?php echo base_url() ?>assets/template_front/img/instagram_icon.svg"
                                                alt="" class="lazy">
                                    </a>
                                </li>
                                <li>
                                    <a href="#0">
                                        <img
                                                src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                                                data-src="<?php echo base_url() ?>assets/template_front/img/youtube_icon.svg"
                                                alt="" class="lazy">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row add_bottom_25">
                <div class="col-lg-6">
                </div>
                <div class="col-lg-6">
                    <ul class="additional_links">
                        <li><a href="<?php echo site_url('kebijakan-dan-privasi') ?>">Kebijakan & Privasi</a></li>
                        <li><span>© <?php echo date('Y') ?> duitte.com</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div>


<div id="toTop"></div>

<div class="top_panel cart-add-wrapper">
    <div class="container header_panel">
        <a href="javascript:;" class="btn_close_top_panel"><i class="ti-close"></i></a>
        <label><span class="cart-add-count">0</span> produk dimasukkan ke keranjang belanja</label>
    </div>

    <div class="item">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="item_panel">
                        <figure class="cart-add-item-thumbnail">

                        </figure>
                        <span class="cart-add-item-title"></span>
                        <div class="price_panel">
                            <span class="new_price cart-add-item-price"></span>
                            <span class="old_price cart-add-item-price-old"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 btn_panel">
                    <a href="<?php echo site_url('cart/list') ?>" class="btn_1 outline">Keranjang Belanja</a>
                </div>
            </div>
        </div>
    </div>

    <div class="container related">
        <div class="cart-add-item-bought-title"></div>
        <div class="cart-add-item-bought-list"></div>
    </div>
</div>


<div class='container-loading hide'>
    <div class='loader'>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--text'></div>
        <div class='loader--desc'></div>
    </div>
</div>


<script src="<?php echo base_url() ?>assets/template_front/js/carousel-home.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/js/sticky_sidebar.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/js/specific_listing.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/js/carousel_with_thumbs.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/plugin/lightbox/js/lightbox.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/plugin/masonry/masonry.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/js/sweetalert.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/plugin/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/plugin/select2/js/select2.min.js"></script>

<script src="<?php echo base_url() ?>assets/template_front/js/main.js"></script>
<script src="<?php echo base_url() ?>assets/template_front/js/custom.js"></script>


</body>

</html>