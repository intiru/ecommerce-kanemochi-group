<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/><!--<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="x-apple-disable-message-reformatting">
    <title>Email Template</title>
    <style type="text/css">


        @import url(https://fonts.googleapis.com/css?family=Nunito:200,300,400,600,700,800,900|Raleway:200,300,400,500,600,700,800,900);

        html {
            width: 100%
        }

        body {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
            margin: 0;
            padding: 0;
            font-family: 'Open Sans', Arial, Sans-serif !important
        }

        table {
            border-spacing: 0;
            table-layout: auto;
            margin: 0 auto
        }

        img {
            display: block !important;
            overflow: hidden !important
        }

        a {
            text-decoration: none;
            color: unset
        }

        .ReadMsgBody {
            width: 100%;
            background-color: #fff
        }

        .ExternalClass {
            width: 100%;
            background-color: #fff
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%
        }

        .yshortcuts a {
            border-bottom: none !important
        }

        .full {
            width: 100%
        }

        .pad {
            width: 92%
        }

        .show-mobile {
            display: none !important;
        }

        .hide-mobile {
            display: block !important;
        }

        @media only screen and (max-width: 650px) {
            .res-pad {
                width: 92%;
                max-width: 92%
            }

            .res-full {
                width: 100%;
                max-width: 100%
            }

            .res-left {
                text-align: left !important
            }

            .res-right {
                text-align: right !important
            }

            .res-center {
                text-align: center !important
            }

            .show-mobile {
                display: block !important;
            }

            .hide-mobile {
                display: none !important;
            }
        }

        @media only screen and (max-width: 750px) {
            .margin-full {
                width: 100%;
                max-width: 100%
            }

            .margin-pad {
                width: 92%;
                max-width: 92%;
                max-width: 600px
            }

            .show-mobile {
                display: block !important;
            }

            .hide-mobile {
                display: none !important;
            }
        }
    </style>
</head>
<body>


<table bgcolor="#F5F5F5" class="full" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td style="color: #F5F5F5">
            Terima Kasih sudah berbelanja di <?php echo $this->main->web_name() ?>.
            <?php if ($member_cart->status == 'payment_accepted') {
                echo 'Pembayaran Anda Sudah Kami Terima. Mohon Menunggu untuk pengiriman produk dari Kami.';
            } else if ($member_cart->status == 'pending') {
                echo 'Kami Menunggu pembayaran untuk dapat melanjutkan pengiriman produk.';
            } else if ($member_cart->status == 'error') {
                echo 'Mohon Maaf terjadi kesalahan pada Sistem Kami. Segera hubungi Kami pada menu Kontak Kami.';
            } ?>
        </td>
    </tr>
</table>

<table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table bgcolor="#F5F5F5" width="750" align="center" class="margin-full" border="0" cellpadding="0"
                   cellspacing="0">
                <tr>
                    <td>
                        <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="20" style="font-size:0px">&nbsp;</td>
                            </tr>
                            <!-- paragraph -->
                            <tr>
                                <td class="res-center"
                                    style="text-align: right; color: #909090; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word;">
                                    Tidak bisa melihat email ini ?
                                    <a href="<?php echo site_url() ?>"
                                       style="color: #304050; font-family: 'Nunito', Arial, Sans-serif; letter-spacing: 0.7px; text-decoration: none; word-break: break-word; padding-left: 4px; font-weight: 600;">
                                        Klik disini untuk melihat di website.
                                    </a>
                                </td>
                            </tr>
                            <!-- paragraph end -->
                            <tr>
                                <td height="20" style="font-size:0px">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- ====== Module : Intro ====== -->
<table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table bgcolor="#DB94BE" align="center" width="750" class="margin-full"
                   style="background-size: cover; background-position: center; border-radius: 6px 6px 0 0;" border="0"
                   cellpadding="0" cellspacing="0"
                   background="<?php echo base_url() ?>assets/template_front/img/mail/module02-bg01.png">
                <tr>
                    <td>
                        <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="35" style="font-size:0px">&nbsp;</td>
                            </tr>
                            <!-- column x2 -->
                            <tr>
                                <td>
                                    <table class="full" align="center" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td valign="top">
                                                <!-- left column -->
                                                <table width="100%" align="left" class="res-full" border="0"
                                                       cellpadding="0" cellspacing="0">
                                                    <!-- image -->
                                                    <tr>
                                                        <td>
                                                            <table width="100%" align="left" class="res-full" border="0"
                                                                   cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <table align="center" border="0" cellpadding="0"
                                                                               cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <img width="250"
                                                                                         style="max-width: 250px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;"
                                                                                         src="<?php echo base_url() ?>assets/template_front/img/logo-japan-omiyage.png">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!-- image end -->
                                                </table>
                                                <!-- left column end -->
                                                <!--[if (gte mso 9)|(IE)]></td>
                                                <td><![endif]-->
                                                <table width="1" align="left" class="res-full" border="0"
                                                       cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td height="20" style="font-size:0px">&nbsp;</td>
                                                    </tr>
                                                </table>
                                                <!--[if (gte mso 9)|(IE)]></td>
                                                <td valign="top"><![endif]-->
                                                <!-- right column -->
                                                <table width="100%" class="res-full" border="0"
                                                       cellpadding="0" cellspacing="0">
                                                    <!-- nested column -->
                                                    <tr>
                                                        <td>
                                                            <table class="res-full" border="0"
                                                                   cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <table align="center" border="0" cellpadding="0"
                                                                               cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                                    <table border="0" cellpadding="0"
                                                                                           cellspacing="0">
                                                                                        <tr><![endif]-->
                                                                                    <!--[if !((gte mso 9)|(IE))]-->
                                                                                    <!-- column -->
                                                                                    <table align="left" border="0"
                                                                                           cellpadding="0"
                                                                                           cellspacing="0">
                                                                                        <tr>
                                                                                            <!--[endif]-->
                                                                                            <!-- link -->
                                                                                            <td class="res-center"
                                                                                                style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;">
                                                                                                <a href="<?php echo site_url() ?>"
                                                                                                   style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                                                    BERANDA
                                                                                                </a>
                                                                                            </td>
                                                                                            <!-- link end -->
                                                                                            <!--[if !((gte mso 9)|(IE))]-->
                                                                                        </tr>
                                                                                    </table>
                                                                                    <!-- column end -->
                                                                                    <!-- column -->
                                                                                    <table align="left" border="0"
                                                                                           cellpadding="0"
                                                                                           cellspacing="0">
                                                                                        <tr>
                                                                                            <!--[endif]-->
                                                                                            <!-- link -->
                                                                                            <td class="res-center"
                                                                                                style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;">
                                                                                                <a href="<?php echo site_url('produk') ?>"
                                                                                                   style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                                                    DAFTAR PRODUK
                                                                                                </a>
                                                                                            </td>
                                                                                            <!-- link end -->
                                                                                            <!--[if !((gte mso 9)|(IE))]-->
                                                                                        </tr>
                                                                                    </table>
                                                                                    <!-- column end -->
                                                                                    <!-- column -->
                                                                                    <table align="left" border="0"
                                                                                           cellpadding="0"
                                                                                           cellspacing="0">
                                                                                        <tr>
                                                                                            <!--[endif]-->
                                                                                            <!-- link -->
                                                                                            <td class="res-center"
                                                                                                style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;">
                                                                                                <a href="<?php echo site_url('galeri-foto') ?>"
                                                                                                   style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                                                    GALERI
                                                                                                </a>
                                                                                            </td>
                                                                                            <!-- link end -->
                                                                                            <!--[if !((gte mso 9)|(IE))]-->
                                                                                        </tr>
                                                                                    </table>
                                                                                    <!-- column end -->
                                                                                    <!-- column -->
                                                                                    <table align="left" border="0"
                                                                                           cellpadding="0"
                                                                                           cellspacing="0">
                                                                                        <tr>
                                                                                            <!--[endif]-->
                                                                                            <!-- link -->
                                                                                            <td class="res-center"
                                                                                                style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;">
                                                                                                <a href="<?php echo site_url('blog') ?>"
                                                                                                   style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                                                    BLOG
                                                                                                </a>
                                                                                            </td>
                                                                                            <!-- link end -->
                                                                                            <!--[if !((gte mso 9)|(IE))]-->
                                                                                        </tr>
                                                                                    </table>
                                                                                    <!-- column end -->
                                                                                    <!-- column -->
                                                                                    <table align="left" border="0"
                                                                                           cellpadding="0"
                                                                                           cellspacing="0">
                                                                                        <tr>
                                                                                            <!--[endif]-->
                                                                                            <!-- link -->
                                                                                            <td class="res-center"
                                                                                                style="text-align: center; color: white; font-family: 'Raleway', Arial, Sans-serif; font-size: 15px; letter-spacing: 1.8px; line-height: 23px; word-break: break-word; padding: 0 7px;">
                                                                                                <a href="<?php echo site_url('kontak-kami') ?>"
                                                                                                   style="color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 15px; letter-spacing: 0.7px; text-decoration: none; word-break: break-word;">
                                                                                                    KONTAK
                                                                                                </a>
                                                                                            </td>
                                                                                            <!-- link end -->
                                                                                            <!--[if !((gte mso 9)|(IE))]-->
                                                                                        </tr>
                                                                                    </table>
                                                                                    <!-- column end -->
                                                                                    <!--[endif]-->
                                                                                    <!--[if (gte mso 9)|(IE)]></tr></table>
                                                                                    <![endif]-->
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!-- nested column end -->
                                                </table>
                                                <!-- right column end -->
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="40" style="font-size:0px">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table bgcolor="white" width="750" align="center" class="margin-full" border="0" cellpadding="0"
                   cellspacing="0">
                <tr>
                    <td>
                        <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="70" style="font-size:0px">&nbsp;</td>
                            </tr>
                            <!-- title -->
                            <tr>
                                <td class="res-center"
                                    style="text-align: center; color: #707070; font-family: 'Raleway', Arial, Sans-serif; font-size: 20px; letter-spacing: 1px; word-break: break-word;">
                                    No Invoice : #<?php echo $member_cart->invoice ?>
                                </td>
                            </tr>
                            <!-- title end -->
                            <tr>
                                <td height="13" style="font-size:0px">&nbsp;</td>
                            </tr>
                            <!-- dash -->
                            <tr>
                                <td>
                                    <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table bgcolor="#006D89" align="center" style="border-radius: 10px;"
                                                       border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="55" height="3" style="font-size:0px">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- dash end -->
                            <tr>
                                <td height="30" style="font-size:0px">&nbsp;</td>
                            </tr>
                            <!-- column x3 -->
                            <tr>
                                <td>
                                    <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0"
                                           cellspacing="0">
                                        <tr>
                                            <td>
                                                <table bgcolor="white" width="750" align="center" class="margin-full"
                                                       border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <table width="600" align="center" class="margin-pad"
                                                                   border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td height="35" style="font-size:0px">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 10px;">
                                                                        <strong>Alamat Pengiriman : </strong><br/>
                                                                        <?php echo $member_address->owner_name ?><br/>
                                                                        <?php echo $member_address->phone ?><br/>
                                                                        <?php echo $member_address->address ?>
                                                                        <?php echo $member_address->city_name ?>
                                                                        - <?php echo $member_address->subdistrict_name ?>
                                                                        <?php echo $member_address->province_name ?>
                                                                        <?php echo $member_address->postal_code ?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="600" align="center" class="margin-pad"
                                                                   border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td height="35" style="font-size:0px">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 10px;">
                                                                        <strong>Metode Pengiriman : </strong><br/>
                                                                        <?php echo $member_cart->courier_code ?>
                                                                        - <?php echo $member_cart->courier_code_name ?>
                                                                        <br/>
                                                                        <?php echo $member_cart->courier_service_code ?>
                                                                        - <?php echo $member_cart->courier_service_code_name ?>
                                                                        <br/>
                                                                        <?php echo $member_cart->courier_service_day_estimate ?>
                                                                        hari
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="600" align="center" class="margin-pad"
                                                                   border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td height="35" style="font-size:0px">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 10px;">
                                                                        <strong>Metode Pembayaran : </strong><br/>
                                                                        Tipe Pembayaran
                                                                        : <?php echo $this->main->midtrans_payment_type_label($member_cart_midtrans->payment_type) ?>
                                                                        <br/>

                                                                        <?php if ($member_cart_midtrans->payment_type == 'bank_transfer') { ?>
                                                                            Nama Bank : <?php echo strtoupper($member_cart_midtrans->va_bank) ?>
                                                                            <br/>
                                                                            Nomer Virtual Account : <?php echo $member_cart_midtrans->va_number ?>
                                                                            <br/>
                                                                        <?php } ?>
                                                                        Status
                                                                        : <?php echo $this->main->midtrans_payment_status_label($member_cart_midtrans->payment_type, $member_cart_midtrans->transaction_status) ?>
                                                                        <?php if ($member_cart_midtrans->pdf_url) { ?>
                                                                            <br/>
                                                                            Lampiran Pembayaran : <a
                                                                                    href="<?php echo $member_cart_midtrans->pdf_url ?>"
                                                                                    style="color: blue; text-decoration: underline">Klik
                                                                                Disini</a>
                                                                        <?php } ?>
                                                                        <?php if ($member_cart_midtrans->transaction_status != 'settlement') { ?>
                                                                            <br />
                                                                            Batas Waktu Pembayaran : <?php echo $this->main->format_datetime_view($member_cart_midtrans->payment_deadline) ?>
                                                                            <br/>
                                                                        <?php } ?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0"
                                           cellspacing="0">
                                        <tr>
                                            <td>
                                                <table bgcolor="white" width="750" align="center" class="margin-full"
                                                       border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <table width="600" align="center" class="margin-pad"
                                                                   border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td height="35" style="font-size:0px">&nbsp;</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table class="full" align="center" border="0"
                                                                               cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <table width="600" align="left"
                                                                                           class="res-full" border="0"
                                                                                           cellpadding="0"
                                                                                           cellspacing="0">
                                                                                        <tr>
                                                                                            <td width="150"
                                                                                                style="width: 150px; text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                <table class="full"
                                                                                                       align="center"
                                                                                                       border="0"
                                                                                                       cellpadding="0"
                                                                                                       cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td style="text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                            <strong>Gambar
                                                                                                                : </strong>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td width="250"
                                                                                                style="width: 250px; text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                <table class="full"
                                                                                                       align="center"
                                                                                                       border="0"
                                                                                                       cellpadding="0"
                                                                                                       cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <strong>Produk
                                                                                                                : </strong>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td width="50"
                                                                                                style="width: 50px; text-align: right; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                <table class="full"
                                                                                                       align="right"
                                                                                                       border="0"
                                                                                                       cellpadding="0"
                                                                                                       cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <strong>Qty
                                                                                                                : </strong>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td width="100"
                                                                                                style="width: 100px; text-align: right; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                <table class="full"
                                                                                                       align="right"
                                                                                                       border="0"
                                                                                                       cellpadding="0"
                                                                                                       cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <strong>Subtotal
                                                                                                                : </strong>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <?php foreach ($member_cart_detail as $row) {
                                                                                            $cart_price_total += $row->qty * $row->price; ?>

                                                                                            <tr>
                                                                                                <td width="150"
                                                                                                    style="width: 150px; text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                    <table class="full"
                                                                                                           align="center"
                                                                                                           border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td style="text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                                <img width="100"
                                                                                                                     style="max-width: 100px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;"
                                                                                                                     src="<?php echo $this->main->image_preview_url($row->product_thumbnail) ?>">
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td width="250"
                                                                                                    style="width: 250px; text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                    <table class="full"
                                                                                                           align="center"
                                                                                                           border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td style="padding-right: 15px">
                                                                                                                <?php echo $row->product_title ?>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td width="50"
                                                                                                    style="width: 50px; text-align: right; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                    <table class="full"
                                                                                                           align="right"
                                                                                                           border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <?php echo $row->qty ?>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <td width="150"
                                                                                                    style="width: 150px; text-align: right; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                    <table class="full"
                                                                                                           align="right"
                                                                                                           border="0"
                                                                                                           cellpadding="0"
                                                                                                           cellspacing="0">
                                                                                                        <tr>
                                                                                                            <td>
                                                                                                                <?php echo $this->main->currency($row->qty * $row->product_price_cart) ?>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        <?php } ?>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table bgcolor="white" width="750" align="center" class="margin-full"
                                                       border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <table width="600" align="center" class="margin-pad"
                                                                   border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td height="30" style="font-size:0px">
                                                                        <hr/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table class="full" align="center" border="0"
                                                                               cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <table width="600" align="left"
                                                                                           class="res-full" border="0"
                                                                                           cellpadding="0"
                                                                                           cellspacing="0">
                                                                                        <tr>
                                                                                            <td width="450"
                                                                                                style="width: 450px; text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                <table class="full"
                                                                                                       align="center"
                                                                                                       border="0"
                                                                                                       cellpadding="0"
                                                                                                       cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td style="text-align: justify; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 10px 0">
                                                                                                            <strong>Ongkos
                                                                                                                Kirim
                                                                                                                : </strong>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                            <td width="150"
                                                                                                style="width: 150px; text-align: right; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0px 0">
                                                                                                <table class="full"
                                                                                                       align="right"
                                                                                                       border="0"
                                                                                                       cellpadding="0"
                                                                                                       cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <strong><?php echo $this->main->currency($member_cart->courier_service_price) ?></strong>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="font-size:0px">
                                                                        <hr/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table class="full" align="center" border="0"
                                                                               cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td valign="top">
                                                                                    <table width="600" align="left"
                                                                                           class="res-full" border="0"
                                                                                           cellpadding="0"
                                                                                           cellspacing="0">
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table style="width: 100%"
                                                                                                       align="center"
                                                                                                       border="0"
                                                                                                       cellpadding="0"
                                                                                                       cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td width="450">

                                                                                                            <table class="full"
                                                                                                                   align="left"
                                                                                                                   border="0"
                                                                                                                   cellpadding="0"
                                                                                                                   cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td class="res-center"
                                                                                                                        style="text-align: left; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 10px;">
                                                                                                                        <strong>Total</strong>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                        <td width="50">

                                                                                                            <table class="full"
                                                                                                                   align="right"
                                                                                                                   border="0"
                                                                                                                   cellpadding="0"
                                                                                                                   cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td class="res-center"
                                                                                                                        style="text-align: right; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 10px;">
                                                                                                                        <strong><?php echo $this->main->format_number($member_cart_product_count) ?></strong>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                        <td width="150">
                                                                                                            <table class="full"
                                                                                                                   align="right"
                                                                                                                   border="0"
                                                                                                                   cellpadding="0"
                                                                                                                   cellspacing="0">
                                                                                                                <tr>
                                                                                                                    <td class="res-center"
                                                                                                                        style="text-align: right; font-family: 'Raleway', Arial, Sans-serif; font-size: 14px; word-break: break-word; padding: 0 0px;">
                                                                                                                        <?php echo $this->main->currency($cart_price_total + $member_cart->courier_service_price) ?>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height="8"
                                                                                                style="font-size:0px">
                                                                                                &nbsp;
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!-- column x2 end -->
                                                                <tr>
                                                                    <td height="37" style="font-size:0px">&nbsp;</td>
                                                                </tr>
                                                                <!-- paragraph -->
                                                                <!--																<tr >-->
                                                                <!--																	<td class="res-center" style="text-align: center; color: #707070; font-family: 'Nunito', Arial, Sans-serif; font-size: 16.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word" >-->
                                                                <!--																		Lorem ipsum dolor sit amet, consectetur adipiscing elit-->
                                                                <!--																		sed do eiusmod tempor incididunt ut labore et-->
                                                                <!--																		dolore magna aliqua-->
                                                                <!--																	</td>-->
                                                                <!--																</tr>-->
                                                                <!-- paragraph end -->
                                                                <!--																<tr><td height="70" style="font-size:0px" >&nbsp;</td></tr>-->
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- ====== Module : Footer ====== -->
<table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table bgcolor="#DB94BE" align="center" width="750" class="margin-full"
                   style="background-size: cover; background-position: center; border-radius: 0 0 6px 6px;" border="0"
                   cellpadding="0" cellspacing="0"
                   background="<?php echo base_url() ?>assets/template_front/img/mail/module20-bg01.png">
                <tr>
                    <td>
                        <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="70" style="font-size:0px">&nbsp;</td>
                            </tr>
                            <!-- image -->
                            <tr>
                                <td>
                                    <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <img width="200"
                                                                 style="max-width: 200px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;"
                                                                 src="<?php echo base_url() ?>assets/template_front/img/logo-japan-omiyage.png">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- image end -->
                            <tr>
                                <td height="20" style="font-size:0px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                        <!-- image -->
                                        <tr>
                                            <td>
                                                <table align="center" class="res-full" border="0" cellpadding="0"
                                                       cellspacing="0">
                                                    <tr>
                                                        <td style="padding: 0 4px;">
                                                            <table style="border-radius: 50%; padding: 10px; border: 1px solid white;"
                                                                   align="center" border="0" cellpadding="0"
                                                                   cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <img width="15"
                                                                             style="max-width: 15px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;"
                                                                             src="<?php echo base_url() ?>assets/template_front/img/mail/module20-img02.png">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="padding: 0 4px;">
                                                            <table style="border-radius: 50%; padding: 10px; border: 1px solid white;"
                                                                   align="center" border="0" cellpadding="0"
                                                                   cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <img width="15"
                                                                             style="max-width: 15px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;"
                                                                             src="<?php echo base_url() ?>assets/template_front/img/mail/module20-img03.png">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="padding: 0 4px;">
                                                            <table style="border-radius: 50%; padding: 10px; border: 1px solid white;"
                                                                   align="center" border="0" cellpadding="0"
                                                                   cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <img width="15"
                                                                             style="max-width: 15px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;"
                                                                             src="<?php echo base_url() ?>assets/template_front/img/mail/module20-img04.png">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="padding: 0 4px;">
                                                            <table style="border-radius: 50%; padding: 10px; border: 1px solid white;"
                                                                   align="center" border="0" cellpadding="0"
                                                                   cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <img width="15"
                                                                             style="max-width: 15px; width: 100%; display: block; line-height: 0px; font-size: 0px; border: 0px;"
                                                                             src="<?php echo base_url() ?>assets/template_front/img/mail/module20-img05.png">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- image end -->
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="30" style="font-size:0px">&nbsp;</td>
                            </tr>
                            <!-- dash -->
                            <tr>
                                <td>
                                    <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table bgcolor="#84b1ff" align="center" style="border-radius: 10px;"
                                                       border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- dash end -->
                            <tr>
                                <td height="20" style="font-size:0px">&nbsp;</td>
                            </tr>
                            <!-- paragraph -->
                            <tr>
                                <td class="res-center"
                                    style="text-align: center; color: white; font-family: 'Nunito', Arial, Sans-serif; font-size: 16.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; opacity: .7;">
                                    Japan Omiyage © All rights reserved
                                </td>
                            </tr>
                            <!-- paragraph end -->
                            <tr>
                                <td height="20" style="font-size:0px">&nbsp;</td>
                            </tr>
                            <!-- dash -->
                            <tr>
                                <td>
                                    <table align="center" class="res-full" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table bgcolor="#84b1ff" align="center" style="border-radius: 10px;"
                                                       border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="600" height="1" style="font-size:0px">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <!-- dash end -->
                            <tr>
                                <td height="70" style="font-size:0px">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- ====== Module : Unsubscribe ====== -->
<table bgcolor="#F5F5F5" align="center" class="full" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table bgcolor="#F5F5F5" width="750" align="center" class="margin-full" border="0" cellpadding="0"
                   cellspacing="0">
                <tr>
                    <td>
                        <table width="600" align="center" class="margin-pad" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="50" style="font-size:0px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table class="full" align="center" border="0" cellpadding="0"
                                                       cellspacing="0">
                                                    <!-- paragraph -->
                                                    <tr>
                                                        <td class="res-center"
                                                            style="text-align: center; color: #909090; font-family: 'Nunito', Arial, Sans-serif; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; font-size: 15px;">
                                                            We land on a wrong place?
                                                        </td>
                                                    </tr>
                                                    <!-- paragraph end -->
                                                </table>
                                            </td>
                                            <td>
                                                <table class="full" align="center" border="0" cellpadding="0"
                                                       cellspacing="0">
                                                    <!-- paragraph -->
                                                    <tr>
                                                        <td class="res-center"
                                                            style="text-align: center; color: #304050; font-family: 'Nunito', Arial, Sans-serif; font-size: 16.5px; letter-spacing: 0.4px; line-height: 23px; word-break: break-word; padding-left: 7px; font-weight: 600;">
                                                            <unsubscribe>Unsubscribe</unsubscribe>
                                                        </td>
                                                    </tr>
                                                    <!-- paragraph end -->
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="50" style="font-size:0px">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>