<form class="form-horizontal1">
    <div class="row">
        <label class="col-sm-4 control-label">
            Nomer Invoice
        </label>
        <div class="col-sm-8">
            <?php echo $member_cart->order_id ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Metode Pembayaran
        </label>
        <div class="col-sm-8">
            <?php echo $member_cart->payment_type ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Waktu Transaksi
        </label>
        <div class="col-sm-8">
            <?php echo $this->main->format_datetime_view($member_cart->transaction_time) ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Batas Waktu Pembayaran
        </label>
        <div class="col-sm-8">
            <?php echo $this->main->format_datetime_view($member_cart->payment_deadline) ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Total Pembayaran
        </label>
        <div class="col-sm-8">
            <?php echo $this->main->currency($member_cart->gross_amount) ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Status Pembayaran
        </label>
        <div class="col-sm-8">
            <?php echo $this->main->midtrans_payment_status_label($member_cart->payment_type, $member_cart->transaction_status, $member_cart->order_id) ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Nama Bank
        </label>
        <div class="col-sm-8">
            <?php echo $member_cart->va_bank ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Nomer Virtual Account
        </label>
        <div class="col-sm-8">
            <?php echo $member_cart->va_number ?>
        </div>
    </div>
</form>