<main class="bg_gray">

    <div class="container margin_30">
        <div class="page_header">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                    <li><a href="<?php echo site_url('account/order') ?>">Daftar Pemesanan</a></li>
                    <li>Invoice : <?php echo $member_cart->invoice ?></li>
                </ul>
            </div>
            <h1>No Invoice : <?php echo $member_cart->invoice ?></h1>
        </div>

        <div class="row justify-content-center">

            <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                <?php echo $this->load->view('front/account_sidebar', ['side_menu_active' => $side_menu_active], TRUE); ?>
            </div>

            <div class="col-xl-9 col-lg-9 col-md-9 col-12">
                <div class="box_account">
                    <div class="form_container">
                        <div class="table-responsive">
                            <h4>Data Invoice</h4>
                            <table class="table table-sm table-striped">
                                <tbody>
                                <tr>
                                    <td width="300"><strong>No Invoice</strong></td>
                                    <td><?php echo $member_cart->invoice ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Status Pemesanan</strong></td>
                                    <td><?php echo $this->main->member_cart_status($member_cart->status) ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Waktu Transaksi</strong></td>
                                    <td><?php echo $this->main->format_datetime_view($member_cart->transaction_time) ?></td>
                                </tr>
                                <?php if ($member_cart->status == 'pending') { ?>
                                    <tr>
                                        <td><strong>Batas Waktu Pembayaran</strong></td>
                                        <td><?php echo $this->main->format_datetime_view($member_cart->payment_deadline) ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                            <h4>Data Penerima</h4>
                            <table class="table table-sm table-striped">
                                <tbody>
                                <tr>
                                    <td width="300"><strong>Nama Kurir</strong></td>
                                    <td><?php echo $member_cart->courier_code_name ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Nama Paket Pengiriman</strong></td>
                                    <td><?php echo $member_cart->courier_service_code_name ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Estimasi Waktu Pengiriman</strong></td>
                                    <td><?php echo $member_cart->courier_service_day_estimate ?> hari</td>
                                </tr>
                                <tr>
                                    <td><strong>Alamat Tujuan Pengiriman</strong></td>
                                    <td>
                                        <?php echo $member_address->owner_name ?><br/>
                                        <?php echo $member_address->phone ?><br/>
                                        <?php echo $member_address->address ?>
                                        <?php echo $member_address->city_name ?>
                                        - <?php echo $member_address->subdistrict_name ?>
                                        <?php echo $member_address->province_name ?>
                                        <?php echo $member_address->postal_code ?>
                                    </td>
                                </tr>
                                <?php if ($member_cart->resi) { ?>
                                    <tr>
                                        <td><strong>Lacak Pemesanan</strong></td>
                                        <td>
                                            <button type="button"
                                                    class="btn_1 btn-sm btn-order-tracking"
                                                    data-action="<?php echo base_url('account/order_tracking/' . $member_cart->id_member_cart) ?>">
                                                <i class="ti-location-pin"></i> Lacak Sekarang
                                            </button>
                                        </td>
                                    </tr>
                                    <?php if ($member_cart->status != 'received') { ?>
                                        <tr>
                                            <td><strong>Pesanan Diterima ?</strong></td>
                                            <td>
                                                <button type="button"
                                                        class="btn_1 btn-sm btn-order-accepted"
                                                        data-id-member-cart="<?php echo $member_cart->id_member_cart ?>">
                                                    <i class="ti-check"></i> Ya, Saya Sudah Terima
                                                </button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>

                            <h4>Data Pembayaran</h4>
                            <table class="table table-sm table-striped">
                                <tbody>
                                <tr>
                                    <td width="300"><strong>Status Pemesanan</strong></td>
                                    <td><?php echo $this->main->member_cart_status($member_cart->status) ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Metode Pembayaran</strong></td>
                                    <td><?php echo $member_cart->payment_type ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Batas Waktu Pembayaran</strong></td>
                                    <td><?php echo $this->main->format_datetime_view($member_cart_midtrans->payment_deadline) ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Total Harga Produk</strong></td>
                                    <td><?php echo $this->main->currency($member_cart->cart_total) ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Harga Pengiriman</strong></td>
                                    <td><?php echo $this->main->currency($member_cart->courier_service_price) ?></td>
                                </tr>
                                <tr>
                                    <td><strong>Total Pembayaran</strong></td>
                                    <td><?php echo $this->main->currency($member_cart->cart_total + $member_cart->courier_service_price) ?></td>
                                </tr>
                                <?php if ($member_cart->status == 'pending') { ?>
                                    <?php if ($member_cart->payment_type == 'bank_transfer') { ?>
                                        <tr>
                                            <td><strong>Detail Pembayaran</strong></td>
                                            <td>
                                                <button type="button"
                                                        class="btn_1 btn-sm btn-detail-pembayaran"
                                                        data-action="<?php echo site_url('account/detail_pembayaran/' . $member_cart->id_member_cart) ?>">
                                                    <i class="ti-check"></i> Detail Pembayaran
                                                </button>
                                            </td>
                                        </tr>
                                    <?php } elseif ($member_cart->payment_type == 'qris') { ?>
                                        <tr>
                                            <td><strong>Lakukan Pembayaran Kembali</strong></td>
                                            <td>
                                                <button type="button"
                                                        class="btn_1 btn-sm btn-bayar-kembali"
                                                        data-action="<?php echo site_url('account/bayar_kembali/' . $member_cart->id_member_cart) ?>">
                                                    <i class="ti-check"></i> Bayar Sekarang
                                                </button>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="box_account">
                    <div class="form_container">
                        <h5>Daftar Pemesanan Produk</h5>
                        <hr/>
                        <table class="table table-striped cart-list">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>
                                    Produk
                                </th>
                                <th>
                                    Harga
                                </th>
                                <th>
                                    Qty
                                </th>
                                <th>
                                    Subtotal
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($member_cart_detail as $key => $row) {
                                $product_option = [];
                                $id_product_option_json = $row->id_product_option_json;
                                $id_product_option_arr = json_decode($id_product_option_json, TRUE);
                                foreach ($id_product_option_arr as $id_product_option => $id_product_option_value) {
                                    $product_option[$id_product_option]['option'] = $this->db->where('id', $id_product_option)->get('product_option')->row()->title;
                                    $product_option[$id_product_option]['value'] = $this->db->where('id', $id_product_option_value)->get('product_option_value')->row()->title;
                                }

                                $total_qty += $row->qty; ?>
                                <tr>
                                    <td><?php echo ++$key ?>.</td>
                                    <td>
                                        <a href="<?php echo $this->main->permalink(array('produk', $row->product_category, $row->product_title)) ?>"
                                           target="_blank">
                                            <div class="thumb_cart">
                                                <img src="<?php echo $this->main->image_preview_url($row->product_thumbnail) ?>"
                                                     data-src="<?php echo $this->main->image_preview_url($row->product_thumbnail) ?>"
                                                     class="lazy" alt="Image">
                                            </div>
                                            <span class="item_cart"><?php echo $row->product_title ?></span>
                                            <?php foreach ($product_option as $row_option) { ?>
                                                <span class="item_cart_option"><?php echo $row_option['option'] . ' : ' . $row_option['value'] ?></span>
                                            <?php } ?>
                                        </a>
                                    </td>
                                    <td class="font-weight-bolder"><?php echo $this->main->format_currency($row->product_price_cart) ?></td>
                                    <td class="font-weight-bolder"><?php echo $this->main->format_number($row->qty) ?></td>
                                    <td class="cart-list-item-subtotal font-weight-bolder">
                                        <?php echo $this->main->format_currency($row->product_price_cart * $row->qty) ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="4" align="right">SUB TOTAL</td>
                                <td class="cart-price-total font-weight-bolder"><?php echo $this->main->format_currency($member_cart->cart_total) ?></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right">Ongkos Kirim</td>
                                <td class="cart-price-total font-weight-bolder"><?php echo $this->main->format_currency($member_cart->courier_service_price) ?></td>
                            </tr>
                            <tr>
                                <td colspan="4" align="right" class="font-weight-bolder">TOTAL</td>
                                <td class="cart-price-total font-weight-bolder"><?php echo $this->main->format_currency($member_cart->cart_total + $member_cart->courier_service_price) ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</main>

<div class="modal fade" id="modal-order-tracking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row" style="width: 100%">
                    <div class="col-10">
                        <h4 class="modal-title" id="myModalLabel"><i class="ti-location-pin"></i> Order Tracking</h4>
                    </div>
                    <div class="col-2">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                </div>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-detail-pembayaran" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row" style="width: 100%">
                    <div class="col-10">
                        <h4 class="modal-title" id="myModalLabel"><i class="ti-money"></i> Detail Informasi Pembayaran
                        </h4>
                    </div>
                    <div class="col-2">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                </div>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo $midtrans_js_link ?>" data-client-key="<?php echo $client_key ?>"></script>