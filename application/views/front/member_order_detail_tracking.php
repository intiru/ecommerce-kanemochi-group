<form class="form-horizontal1">
    <div class="row">
        <label class="col-sm-4 control-label">
            Status Pengiriman
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['delivered'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Nomer Resi
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['summary']['waybill_number'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Nama Kurir
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['summary']['courier_name'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Nama Paket Pengiriman
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['summary']['service_code'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Waktu Pengiriman
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['details']['waybill_date'].' '.$track['result']['details']['waybill_time'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Berat
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['details']['weight'] ?> Kg
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Alamat Tujuan
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['details']['destination'] ?>
        </div>
    </div>
    <hr />
    <div class="row">
        <label class="col-sm-4 control-label">
            Nama Pengirim
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['details']['shipper_name'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Kota Pengirim
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['details']['shipper_city'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Nama Penerima
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['details']['receiver_name'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Alamat Penerima
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['details']['receiver_address2'].'  '. $track['result']['details']['receiver_address3'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Metode Penerimaan
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['details']['receiver_address1'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Kota Penerima
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['details']['receiver_city'] ?>
        </div>
    </div>
    <hr />

    <div class="row">
        <label class="col-sm-4 control-label">
            Status Penerimaan
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['delivery_status']['status'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Tempat Penerimaan
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['delivery_status']['pod_receiver'] ?>
        </div>
    </div>
    <div class="row">
        <label class="col-sm-4 control-label">
            Waktu Penerimaan
        </label>
        <div class="col-sm-8">
            <?php echo $track['result']['delivery_status']['pod_date'].' '. $track['result']['delivery_status']['pod_time']  ?>
        </div>
    </div>
</form>
<br />

<table class="table table-striped">
    <thead>
    <tr>
        <th>Keterangan</th>
        <th>Waktu</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $manifest = array_reverse($track['result']['manifest'], TRUE);
    foreach($manifest as $row){ ?>
    <tr>
        <td><?php echo $row['manifest_description'] ?></td>
        <td><?php echo $row['manifest_date'].' '.$row['manifest_time'] ?></td>
    </tr>
    <?php  } ?>
    </tbody>
</table>