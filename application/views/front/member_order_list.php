<main class="bg_gray">

    <div class="container margin_30">
        <div class="page_header">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                    <li>Daftar Pemesanan</li>
                </ul>
            </div>
            <h1>Daftar Pemesanan</h1>
        </div>

        <div class="row justify-content-center">

            <div class="col-xl-3 col-lg-3 col-md-3 col-12">
                <?php echo $this->load->view('front/account_sidebar', ['side_menu_active' => $side_menu_active], TRUE); ?>
            </div>


            <div class="col-xl-9 col-lg-9 col-md-9 col-12">
                <div class="box_account">
                    <div class="form_container">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>
                                    No Invoice
                                </th>
                                <th>
                                    Tanggal
                                </th>
                                <th>
                                    Total Barang
                                </th>
                                <th>
                                    Total Harga
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Menu
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($member_order as $key => $row) {
                                $total_barang = $this->db->select_sum('qty')->where('id_member_cart', $row->id)->get('member_cart_detail')->row()->qty; ?>
                                <tr>
                                    <td><?php echo $key + 1 ?>.</td>
                                    <td>
                                        <?php echo $row->invoice ?>
                                    </td>
                                    <td>
                                        <?php echo $this->main->format_datetime_view($row->cart_date) ?>
                                    </td>
                                    <td>
                                        <?php echo $total_barang ?>
                                    </td>
                                    <td>
                                        <strong><?php echo $this->main->currency($row->cart_total + $row->courier_service_price) ?></strong>
                                    </td>
                                    <td>
                                        <strong><?php echo $this->main->member_cart_status($row->status) ?></strong>
                                    </td>
                                    <td>
                                        <a href="<?php echo site_url('account/order_detail/' . $row->id) ?>"
                                           class="btn btn-success">Detail</a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>