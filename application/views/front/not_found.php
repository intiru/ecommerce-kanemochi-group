<main class="bg_gray">
    <div id="error_page">
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-xl-7 col-lg-9">
                    <img src="<?php echo base_url() ?>assets/template_front/img/404.svg" alt="" class="img-fluid" width="400" height="212">
                    <p>Maaf, Halaman Website Tidak Ditemukan</p>
                    <form>
                        <div class="search_bar">
                            <input type="text" class="form-control" placeholder="Ketik apa yang Anda cari ...">
                            <input type="submit" value="Search">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>