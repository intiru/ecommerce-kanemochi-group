<main class="bg_gray">

    <div class="bg_white">
        <div class="container margin_60_35 text-center">
            <i class="ti-check-box payment-success-icon"></i>
            <h1 class="pb-3 text-center">Pemesanan Berhasil</h1>
            <div class="row">
                <div class="col-lg-12 col-md-12 add_bottom_25">
                    <p>Terima Kasih sudah melakukan pemesanan, silahkan melakukan pembayaran untuk dapat Kami kirimkan barangnya.</p>
                    <br />
                    <a href="<?php echo site_url('produk') ?>" class="btn_1">Silahkan Melihat Produk Kami yang lainnya</a>
                </div>
            </div>
        </div>
    </div>
</main>