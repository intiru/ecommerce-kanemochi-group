<main>
    <br/>
    <br/>

    <div class="container margin_30 wrapper-product-detail">
        <div class="row">
            <div class="col-md-6">
                <div class="all">

                    <div class="slider">
                        <div class="owl-carousel owl-theme main magnific-gallery product-item">
                            <?php foreach ($product_gallery as $row) {
                                $id_product_option_value_arr = json_decode($row->id_product_option_value_json, TRUE);
                                $id_product_option_value_implode = implode('_', $id_product_option_value_arr);
                                if ($row->youtube_url) {
                                    $class = 'video';
                                    $popup_link = $row->youtube_url;
                                } else {
                                    $class = 'image';
                                    $popup_link = $this->main->image_preview_url($row->thumbnail);
                                } ?>
                                <div class="item-box <?php echo $class ?>">
                                    <a href="<?php echo $popup_link ?>"
                                       title="<?php echo $row->thumbnail_alt ?>" class="<?php echo $class ?>"
                                       data-effect="mfp-zoom-in">
                                        <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                             data-id-product-option-value-implode="<?php echo $id_product_option_value_implode ?>"
                                             data-id-product-gallery="<?php echo $row->id ?>">
                                        <?php

                                        if ($row->youtube_url) { ?>
                                            <span class="product-video-play">
                                                <i class="ti-control-play"></i>
                                            </span>
                                        <?php } ?>

                                        <?php if ($page->out_of_stock == 'yes') { ?>
                                            <div class="overlay overlay_2">
                                                <div class="label-out-stock">Stok Habis</div>
                                            </div>
                                        <?php } ?>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="left nonl"><i class="ti-angle-left"></i></div>
                        <div class="right"><i class="ti-angle-right"></i></div>
                    </div>
                    <div class="slider-two">
                        <div class="owl-carousel owl-theme thumbs">
                            <?php foreach ($product_gallery as $key => $row) {
                                if ($row->youtube_url) {
                                    $class = 'video';
                                } else {
                                    $class = 'image';
                                }
                                ?>
                                <div class="item <?php echo $class ?>">
                                    <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                         class="<?php echo $class ?>" alt="<?php echo $row->thumbnail_alt ?>">
                                    <?php if ($row->youtube_url) { ?>
                                        <span class="product-video-play">
                                            <i class="ti-control-play"></i>
                                        </span>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="left-t nonl-t"></div>
                        <div class="right-t"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                        <li><a href="<?php echo site_url('produk') ?>">Produk</a></li>
                        <?php echo $this->main->user_category_breadcrumb_hirarcy($page->id_category); ?>
                    </ul>
                </div>

                <div class="prod_info">
                    <h1><?php echo $page->title ?></h1>
                    <div class="rating">
                        <?php echo $this->main->format_star($page->star) ?>
                    </div>
                    <div class="product-detail-description">
                        <?php echo $page->description ?>
                    </div>

                    <div class="prod_options">
                        <div class="prod_options_wrapper">
                            <?php foreach ($product_option as $row_1) { ?>
                                <div class="row">
                                    <label class="col-xl-5 col-lg-5 col-md-5 col-5"><strong><?php echo $row_1->title ?></strong></a>
                                    </label>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-7">
                                        <div class="custom-select-form">
                                            <select class="wide product-option-select"
                                                    name="id_product_option_value[<?php echo $row_1->id ?>]"
                                                    data-id-product-option="<?php echo $row_1->id ?>">
                                                <option value="">Pilih <?php echo $row_1->title ?> ...</option>
                                                <?php foreach ($row_1->product_option_value as $row_2) { ?>
                                                    <option value="<?php echo $row_1->id . '|' . $row_2->id ?>"
                                                            data-id="<?php echo $row_2->id ?>"><?php echo $row_2->title ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="row">
                            <label class="col-xl-5 col-lg-5 col-md-5 col-5"><strong>Quantity</strong></label>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-7">
                                <div class="numbers-row">
                                    <input type="text" value="1" id="quantity_1" class="qty2" name="product_qty">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-md-6 col-12">
                            <div class="price_main">
                                <span class="new_price"><?php echo $this->main->format_currency($page->price) ?></span>
                                <?php if ($page->price_old) { ?>
                                    <span class="old_price"><?php echo $this->main->format_currency($page->price_old) ?></span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-8 col-12">
                            <div class="btn_add_to_cart">
                                <a href="javascript:;"
                                   class="btn_1 <?php echo $page->out_of_stock == 'yes' ? 'btn-disabled':'' ?>"
                                   data-id="<?php echo $page->id ?>"
                                   data-title="<?php echo $this->main->trim_special_char($page->title) ?>"
                                   data-category-title="<?php echo $page->category_title ?>"
                                   data-image="<?php echo $this->main->image_preview_url($page->product_thumbnail) ?>"
                                   data-price="<?php echo $page->price ?>"
                                   data-price-label="<?php echo $this->main->format_currency($page->price) ?>"
                                    <?php if ($page->price_old) { ?>
                                        data-price-old="<?php echo $page->price_old ?>"
                                        data-price-old-label="<?php echo $this->main->format_currency($page->price_old) ?>"
                                    <?php } ?>>

                                    <?php if($page->out_of_stock == 'yes') { ?>
                                        <i class="ti-info"></i> Stok Habis
                                    <?php } else { ?>
                                        <i class="ti-shopping-cart"></i> Masukkan Keranjang
                                    <?php } ?>
                                </a>
                            </div>

                            <div class="product_actions">
                                <ul>
                                    <li>
                                        <a href="javascript:;"><i class="ti-heart"></i><span>Add to Wishlist</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ($related_count > 0) { ?>

        <div class="container margin_60_35">
            <div class="main_title">
                <h2>Produk Berkaitan</h2>
                <span>Daftar</span>
                <p>Berikut adalah daftar produk berkaitan dengan produk <?php echo $page->title ?></p>
            </div>
            <div class="owl-carousel owl-theme products_carousel">

                <?php foreach ($related as $row) {
                    $product_item_view = $this->main->product_item_view($row); ?>
                    <div class="item">
                        <?php echo $product_item_view ?>
                    </div>
                <?php } ?>
            </div>
        </div>

    <?php } ?>

    <div class="feat">
        <div class="container">
            <ul>
                <li>
                    <div class="box">
                        <i class="ti-gift"></i>
                        <div class="justify-content-center">
                            <h3>Free Shipping</h3>
                            <p>For all oders over $99</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="box">
                        <i class="ti-wallet"></i>
                        <div class="justify-content-center">
                            <h3>Secure Payment</h3>
                            <p>100% secure payment</p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="box">
                        <i class="ti-headphone-alt"></i>
                        <div class="justify-content-center">
                            <h3>24/7 Support</h3>
                            <p>Online top support</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="also-bought-list hide"><?php echo $also_bought ?></div>
</main>

