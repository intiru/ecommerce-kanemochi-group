<main>
    <div class="top_banner">
        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
            <div class="container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="<?php echo site_url() ?>">Beranda</a></li>
                        <li>Daftar Semua Produk</li>
                    </ul>
                </div>
                <h1>Daftar Semua Produk</h1>
                <p align="justify">Berikut adalah daftar semua produk yang ada di toko <?php echo $web_name ?></p>
            </div>
        </div>
        <img src="<?php echo $this->main->image_preview_url($page->thumbnail) ?>" class="img-fluid" alt="">
    </div>
    <div id="stick_here"></div>

    <div class="container margin_30">

        <div class="row">
            <aside class="col-lg-3" id="sidebar_fixed">
                <div class="filter_col">
                    <div class="inner_bt"><a href="javascript:;" class="open_filters"><i class="ti-close"></i></a></div>
                    <div class="filter_type version_2">
                        <h4><a href="javascript:;" data-toggle="collapse" class="opened">Kategori Produk</a></h4>
                        <div class="collapse show" id="filter_1">
                            <ul class="tree">
                                <li>
                                    <a href="<?php echo $this->main->permalink(array('produk')) ?>">
                                        Semua Produk
                                    </a>
                                </li>
                                <?php foreach ($category_menu as $row_0) {
                                    $category_1 = $this->main->user_category_sub($row_0->id);
                                    $permalink = $this->main->permalink(array('produk', $row_0->title, '?category=' . $this->main->str_encrypt($row_0->id)));
                                    $class = in_array($row_0->id, $id_category_hirarcy) ? 'open' : '';
                                    $class .= $row_0->id == $id_category_active ? ' active':''; ?>
                                    <li class="<?php echo $class ?>"
                                        data-id="<?php echo $row_0->id ?>">
                                        <a href="<?php echo $permalink ?>" >
                                            <?php echo $this->main->kategori_judul($row_0->title).'' ?>
                                        </a>
                                        <?php if (count($category_1) > 0) { ?>
                                            <ul>
                                                <?php foreach ($category_1 as $row_1) {
                                                    $category_2 = $this->main->user_category_sub($row_1->id);
                                                    $permalink = $this->main->permalink(array('produk', $row_1->title, '?category=' . $this->main->str_encrypt($row_1->id)));
                                                    $class = in_array($row_1->id, $id_category_hirarcy) ? 'open' : '';
                                                    $class .= $row_1->id == $id_category_active ? ' active':'';
                                                    ?>
                                                    <li class="<?php echo $class ?>" data-id="<?php echo $row_1->id ?>">
                                                        <a href="<?php echo $permalink ?>">
                                                            <?php echo $this->main->kategori_judul($row_1->title) ?>
                                                        </a>
                                                        <?php if (count($category_2) > 0) { ?>
                                                            <ul>
                                                                <?php foreach ($category_2 as $row_2) {
                                                                    $category_3 = $this->main->user_category_sub($row_2->id);
                                                                    $permalink = $this->main->permalink(array('produk', $row_2->title, '?category=' . $this->main->str_encrypt($row_2->id)));
                                                                    $class = in_array($row_2->id, $id_category_hirarcy) ? 'open' : '';
                                                                    $class .= $row_2->id == $id_category_active ? ' active':''; ?>
                                                                    <li class="<?php echo $class ?>"
                                                                        data-id="<?php echo $row_2->id ?>">
                                                                        <a href="<?php echo $permalink ?>"
                                                                           >
                                                                            <?php echo $this->main->kategori_judul($row_2->title) ?>
                                                                        </a>
                                                                        <?php if (count($category_3) > 0) { ?>
                                                                            <ul>
                                                                                <?php foreach ($category_3 as $row_3) {
                                                                                    $category_4 = $this->main->user_category_sub($row_3->id);
                                                                                    $permalink = $this->main->permalink(array('produk', $row_3->title, '?category=' . $this->main->str_encrypt($row_3->id)));
                                                                                    $class = in_array($row_3->id, $id_category_hirarcy) ? 'open' : '';
                                                                                    $class .= $row_3->id == $id_category_active ? ' active':''; ?>
                                                                                    <li class="<?php echo $class ?>"
                                                                                        data-id="<?php echo $row_3->id ?>">
                                                                                        <a href="<?php echo $permalink ?>"
                                                                                           >
                                                                                            <?php echo $this->main->kategori_judul($row_3->title) ?>
                                                                                        </a>
                                                                                        <?php if (count($category_4) > 0) { ?>
                                                                                            <ul>
                                                                                                <?php foreach ($category_4 as $row_4) {
                                                                                                    $category_5 = $this->main->user_category_sub($row_4->id);
                                                                                                    $permalink = $this->main->permalink(array('produk', $row_4->title, '?category=' . $this->main->str_encrypt($row_4->id)));
                                                                                                    $class = in_array($row_4->id, $id_category_hirarcy) ? 'open' : '';
                                                                                                    $class .= $row_4->id == $id_category_active ? ' active':''; ?>
                                                                                                    <li class="<?php echo $class ?>"
                                                                                                        data-id="<?php echo $row_4->id ?>">
                                                                                                        <a href="<?php echo $permalink ?>"
                                                                                                           >
                                                                                                            <?php echo $this->main->kategori_judul($row_4->title) ?>
                                                                                                        </a>
                                                                                                        <?php if (count($category_5) > 0) { ?>
                                                                                                            <ul>
                                                                                                                <?php foreach ($category_5 as $row_5) {
                                                                                                                    $category_6 = $this->main->user_category_sub($row_5->id);
                                                                                                                    $permalink = $this->main->permalink(array('produk', $row_5->title, '?category=' . $this->main->str_encrypt($row_5->id)));
                                                                                                                    $class = in_array($row_5->id, $id_category_hirarcy) ? 'open' : '';
                                                                                                                    $class .= $row_5->id == $id_category_active ? ' active':''; ?>
                                                                                                                    <li class="<?php echo $class ?>"
                                                                                                                        data-id="<?php echo $row_5->id ?>">
                                                                                                                        <a href="<?php echo $permalink ?>"
                                                                                                                           >
                                                                                                                            <?php echo $this->main->kategori_judul($row_5->title) ?>
                                                                                                                        </a>
                                                                                                                    </li>
                                                                                                                <?php } ?>
                                                                                                            </ul>
                                                                                                        <?php } ?>
                                                                                                    </li>
                                                                                                <?php } ?>
                                                                                            </ul>
                                                                                        <?php } ?>
                                                                                    </li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                        <?php } ?>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                <?php } ?>
                            </ul>

                        </div>

                    </div>
                </div>

            </aside>

            <div class="col-lg-9">
                <div class="row small-gutters">


                    <div class="container" style="margin-top:30px;">
                        <div class="row">
                            <div class="col-md-4">

                            </div>
                        </div>
                    </div>

                    <?php foreach ($product_list as $row) {
                        $product_item_view = $this->main->product_item_view($row); ?>
                        <div class="col-6 col-xs-12 col-md-4">
                            <?php echo $product_item_view; ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="pagination__wrapper">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
    </div>
</main>